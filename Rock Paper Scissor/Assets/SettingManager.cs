using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingManager : MonoBehaviour
{
    public static SettingManager instance;
    public GameObject settingPanel;
    public Button onVibBtn;
    public Button offVibBtn;

    public Button onSoundBtn;
    public Button offSoundBtn;

    public Text settigs_username;

    // Start is called before the first frame update
    void Start()
    {

        instance = this;
    }

    // Update is called once per frame

    private void OnEnable()
    {
        settigs_username.text = UI.instance.displayName;

        InterectableCheck();
    }



    public void GameVibration(bool vibrateCheck)
    {
        if (vibrateCheck)
        {
            PlayerPrefs.SetInt("Vibration", 1);
          
        }
        else
        {
            PlayerPrefs.SetInt("Vibration", 0);
            



        }
        InterectableCheck();

    }



    public void InterectableCheck()
    {
        if (PlayerPrefs.GetInt("Vibration",1) == 1)
        {
            onVibBtn.interactable = false;
            offVibBtn.interactable = true;

        }
        else
        {

            onVibBtn.interactable = true;
            offVibBtn.interactable = false;
        }
        if (PlayerPrefs.GetInt("Sound",1) == 1)
        {
            onSoundBtn.interactable = false;
            offSoundBtn.interactable = true;

        }
        else
        {

            onSoundBtn.interactable = true;
            offSoundBtn.interactable = false;
        }



    }

    public void GameSound(bool soundCheck)
    {
        if (soundCheck)
        {
            PlayerPrefs.SetInt("Sound", 1);
            FindObjectOfType<GameMusiic>().gameObject.GetComponent<AudioSource>().enabled = true;
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 0);
            FindObjectOfType<GameMusiic>().gameObject.GetComponent<AudioSource>().enabled = false;




        }
        InterectableCheck();

    }

    public void OnSettingPanel()
    {
        SoundManager.PlaySound("btnSound");
      //  settingPanel.SetActive(true);

    }

   


    public void PrivacyPolicy()
    {

        //Application.OpenURL("https://www.blogger.com/blog/post/edit/2759750459410668934/551076691043274583");

    }


    public void TermsOfService()
    {

        //Application.OpenURL("https://www.blogger.com/blog/post/edit/2759750459410668934/5332044955879654140");

    }


    public void AvatarSound()
    {
        SoundManager.PlaySound("btnSound");

    }
}
