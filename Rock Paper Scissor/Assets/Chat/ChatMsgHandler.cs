using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class ChatMsgHandler : MonoBehaviourPunCallbacks
{
    public List<string> chatEvents;

    public string messageRecieved;
    public InputField msgs;
    public InputField writeMsg;
    public GameObject parent;
    

    enum EventCodes
    { 
    chatmessage = 0,
        playAudio = 1



    }

    public void Start()
    {
        chatEvents = new List<string>();
     
    }
    
    private void OnEnable()
    {

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;

    }

    private void OnDisable()
    {


        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;    

    }

    public void MySendMsg()
    {

        photonView.RPC("InstantiaiteIt", RpcTarget.All);
    
    }

    float increment = 5f;

//[PunRPC]
    public void InstantiaiteIt()
    {
        //Vector3 pos = new Vector3(0, 0, 0);
        //Vector3 spawnPoint += Vector3(0, 0, 0);
        //parent = GameObject.FindGameObjectWithTag("Canvas");
        GameObject myText = PhotonNetwork.Instantiate("myText", new Vector3(0,0,0), Quaternion.identity);
        //PhotonNetwork.Instantiate
        myText.transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
        //myText.transform.parent = parent.transform;


    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        object content = photonEvent.CustomData;
        EventCodes code = (EventCodes)eventCode;

        if (code == EventCodes.chatmessage)
        {

            object[] datas = content as object[];
            messageRecieved = (string)datas[0];
            Debug.Log("Message Recived " + (string)datas[0]);
        }

    }

    public void SetText()
    {
        //messsage = msgs.text;
        //SendMsg(msgs.text);

        chatEvents.Add(writeMsg.text);
        
        SendMsg(writeMsg.text);
    
    }

    public void SendMsg(string message)
    {
        object[] datas = new object[] { message  };
        RaiseEventOptions options = new RaiseEventOptions
        {
            CachingOption = EventCaching.DoNotCache,
            Receivers = ReceiverGroup.All

        };
        SendOptions sendOptions = new SendOptions();
        sendOptions.Reliability = true;

        PhotonNetwork.RaiseEvent((byte)EventCodes.chatmessage , datas , options, sendOptions);


    }


}
