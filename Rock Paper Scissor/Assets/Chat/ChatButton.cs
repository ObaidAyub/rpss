using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChatButton : MonoBehaviour
{
    public GameObject chatMessages;
    public GameObject chatCard;

    public void OnClick()
    {

        SoundManager.PlaySound("btnSound");

        if (chatCard.activeSelf)
        {

          //  chatMessages.SetActive(false);
            chatCard.SetActive(false);
        }

        else
        {
            //chatMessages.SetActive(true);
            chatCard.SetActive(true);
        }
    
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
