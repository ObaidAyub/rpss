using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using Photon.Realtime;

public class ChatScript : MonoBehaviour
{

    public GameObject chatSystem;
    // Start is called before the first frame update
    void Start()
    {
        PhotonView photonView = GetComponent<PhotonView>();
        if (photonView.IsMine)
        {
            chatSystem.SetActive(true);
        
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
