using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class RPS : MonoBehaviour
{
    public static RPS instance;
    public Image activePawnImg;
    public GameManager gm;

    public Button rock_Btn;
    public Button paper_Btn;
    public Button scissor_Btn;

    public Sprite rockImage;
    public Sprite paperImage;
    public Sprite scissorImage;

    public Sprite defaultImage;
    public int turn = 1;
    public GameObject tiePanel;
    public bool enableRPSS;
    //public GameObject jailPanel;

    PhotonView PV;

    private void Start()
    {
        instance = this;
        PV = GetComponent<PhotonView>();
        enableRPSS = false;
    }

    private void OnEnable()
    {
        activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = defaultImage;
        rock_Btn.interactable = true;
        paper_Btn.interactable = true;
        scissor_Btn.interactable = true;
        enableRPSS = true;
    }
    private void SelectFirstOwnerForRPSS(string teamColor)
    {
        switch (teamColor)
        {
            case "Red":
                gm.firstTurn = gm.red4Pawn.GetComponent<Stone>();
                break;
            case "Purple":
                gm.firstTurn = gm.purple4Pawn.GetComponent<Stone>();
                break;
            case "Green":
                gm.firstTurn = gm.green4Pawn.GetComponent<Stone>();
                break;
            case "Blue":
                gm.firstTurn = gm.blue4Pawn.GetComponent<Stone>();
                break;
        }
    }
    private void SeletSecondOwnerForRPSS(string teamColor)
    {
        switch (teamColor)
        {
            case "Red":
                gm.secondTurn = gm.red4Pawn.GetComponent<Stone>();
                break;
            case "Purple":
                gm.secondTurn = gm.purple4Pawn.GetComponent<Stone>();
                break;
            case "Green":
                gm.secondTurn = gm.green4Pawn.GetComponent<Stone>();
                break;
            case "Blue":
                gm.secondTurn = gm.blue4Pawn.GetComponent<Stone>();
                break;
        }
    }
    public void Rock()
    {
        if (gm.firstTurn == null)
        {
            Debug.Log("first turn is null");
        }
        if (gm.secondTurn == null)
        {
            Debug.Log("second turn is null");
        }

        if (turn == 1)
        {
            SelectFirstOwnerForRPSS(gm.TeamColor);
            gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Rock;
            gm.FPSelectedRPS = "Rock";
        }
        else
        {
            SeletSecondOwnerForRPSS(gm.TeamColor);
            gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Rock;
            gm.SPSelectedRPS = "Rock";
        }

        activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = rockImage;
        rock_Btn.interactable = false;
        paper_Btn.interactable = false;
        scissor_Btn.interactable = false;
        turn++;
        PV.RPC("SelectRPSS", RpcTarget.AllBuffered, "Rock", gm.TeamColor);
    }

   

    public void Paper()
    {
        if (gm.firstTurn == null)
        {
            Debug.Log("first turn is null");
        }
        if (gm.secondTurn == null)
        {
            Debug.Log("second turn is null");
        }

        if (turn == 1)
        {
            SelectFirstOwnerForRPSS(gm.TeamColor);
            gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Paper;
            gm.FPSelectedRPS = "Paper";
        }
        else
        {
            SeletSecondOwnerForRPSS(gm.TeamColor);
            gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Paper;
            gm.SPSelectedRPS = "Paper";
        }

        activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = paperImage;
        rock_Btn.interactable = false;
        paper_Btn.interactable = false;
        scissor_Btn.interactable = false;
        turn++;
        PV.RPC("SelectRPSS", RpcTarget.AllBuffered, "Paper", gm.TeamColor);
    }
    public void Scissor()
    {
        if (gm.firstTurn == null)
        {
            Debug.Log("first turn is null");
        }
        if (gm.secondTurn == null)
        {
            Debug.Log("second turn is null");
        }

        if (turn == 1)
        {
            SelectFirstOwnerForRPSS(gm.TeamColor);
            gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Scissor;
            gm.FPSelectedRPS = "Scissor";
        }
        else
        {
            SeletSecondOwnerForRPSS(gm.TeamColor);
            gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Scissor;
            gm.SPSelectedRPS = "Scissor";
        }

        activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = scissorImage;
        rock_Btn.interactable = false;
        paper_Btn.interactable = false;
        scissor_Btn.interactable = false;
        turn++;

        PV.RPC("SelectRPSS", RpcTarget.AllBuffered, "Scissor", gm.TeamColor);
    }
   

    [PunRPC]
    public void SelectRPSS(string RPSS_Name, string TeamColor)
    {
        Debug.Log("RPss  " + RPSS_Name + "  team color " + TeamColor + " turn " + turn);
        if (gm.TeamColor == TeamColor)
        {

            if (gm.firstTurn == null)
            {
                Debug.Log("Return first turn");
                return;
            }
            if (gm.secondTurn == null)
            {
                Debug.Log("Return second turn ");
                return;
            }
            else
            {
                Debug.Log("Return with comparison");
                RpssConditions(gm.firstTurn, gm.secondTurn);
                return;
            }
        }
        switch (RPSS_Name)
        {
            case "Rock":
                Debug.Log(RPSS_Name);

                if (turn == 1)
                {
                    
                    SelectFirstOwnerForRPSS(TeamColor);
                    gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Rock;
                    gm.FPSelectedRPS = "Rock";
                }
                else
                {
                    SeletSecondOwnerForRPSS(TeamColor);
                    gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Rock;
                    gm.SPSelectedRPS = "Rock";
                }
                turn++;
                break;
            case "Paper":
                Debug.Log(RPSS_Name);

                if (turn == 1)
                {
                    SelectFirstOwnerForRPSS(TeamColor);
                    gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Paper;
                    gm.FPSelectedRPS = "Paper";
                }
                else
                {
                    SeletSecondOwnerForRPSS(TeamColor);
                    gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Paper;
                    gm.SPSelectedRPS = "Paper";
                }
                turn++;
                break;
            case "Scissor":
                Debug.Log(RPSS_Name);

                if (turn == 1)
                {
                    SelectFirstOwnerForRPSS(TeamColor);
                    gm.firstTurn.selectedRPS = Stone.ChoosenRPS.Scissor;
                    gm.FPSelectedRPS = "Scissor";
                }
                else
                {
                    SeletSecondOwnerForRPSS(TeamColor);
                    gm.secondTurn.selectedRPS = Stone.ChoosenRPS.Scissor;
                    gm.SPSelectedRPS = "Scissor";
                }
                turn++;
                break;
            
        }
        if (gm.firstTurn == null)
        {
            Debug.Log("Return first turn");
            return;
        }
        if (gm.secondTurn == null)
        {
            Debug.Log("Return second turn ");
            return;
        }
        else
        {
            Debug.Log("Return with comparison");
            RpssConditions(gm.firstTurn, gm.secondTurn);
            return;
        }
    }
    private void RpssConditions(Stone first, Stone second)
    {
        // tie conditions
        if ((first.selectedRPS == Stone.ChoosenRPS.Rock) && (second.selectedRPS == Stone.ChoosenRPS.Rock))
        {
            RpssSetToDefault(first, second);
            StartCoroutine(TieWorking(first.gameObject, second.gameObject));
        }
        else if ((first.selectedRPS == Stone.ChoosenRPS.Paper) && (second.selectedRPS == Stone.ChoosenRPS.Paper))
        {
            RpssSetToDefault(first, second);
            StartCoroutine(TieWorking(first.gameObject, second.gameObject));
        }
        else if ((first.selectedRPS == Stone.ChoosenRPS.Scissor) && (second.selectedRPS == Stone.ChoosenRPS.Scissor))
        {
            RpssSetToDefault(first, second);
            StartCoroutine(TieWorking(first.gameObject, second.gameObject));
        }
       
        // PLAYER ONE Rock AND PLAYER TWO SCISSOR
        else if ((first.selectedRPS == Stone.ChoosenRPS.Rock) && (second.selectedRPS == Stone.ChoosenRPS.Scissor))
        {
            Debug.Log("Here " + first.name + " " + second.name);
            PlayerGotoJail(first, second, false);// player first wins
        }
        // PLAYER ONE Scissor AND PLAYER TWO Rock
        else if ((first.selectedRPS == Stone.ChoosenRPS.Scissor) && (second.selectedRPS == Stone.ChoosenRPS.Rock))
        {
            Debug.Log("Here " + first.name + " " + second.name);
            PlayerGotoJail(first, second, true);
        }


        // PLAYER ONE Paper AND PLAYER TWO Rock
        else if ((first.selectedRPS == Stone.ChoosenRPS.Paper) && (second.selectedRPS == Stone.ChoosenRPS.Rock))
        {
            Debug.Log("Here " + first.name + " " + second.name);
            PlayerGotoJail(first, second, false);// player first wins
        }


        // PLAYER ONE Rock AND PLAYER TWO Paper
        else if ((first.selectedRPS == Stone.ChoosenRPS.Rock) && (second.selectedRPS == Stone.ChoosenRPS.Paper))
        {
            Debug.Log("Here " + first.name + " " + second.name);
            PlayerGotoJail(first, second, true);
        }


        // PLAYER ONE Scissor AND PLAYER TWO Paper
        else if ((first.selectedRPS == Stone.ChoosenRPS.Scissor) && (second.selectedRPS == Stone.ChoosenRPS.Paper))
        {
            Debug.Log("Here");
            PlayerGotoJail(first, second, false);// player first wins
        }
        
        // PLAYER ONE Paper AND PLAYER TWO SCISSOR
        else if ((first.selectedRPS == Stone.ChoosenRPS.Paper) && (second.selectedRPS == Stone.ChoosenRPS.Scissor))
        {
            Debug.Log("Here");
            PlayerGotoJail(first, second, true);
        }


        


    }
    
    private void RpssSetToDefault(Stone first, Stone second)
    {
        first.selectedRPS = Stone.ChoosenRPS.None;
        second.selectedRPS = Stone.ChoosenRPS.None;
        turn = 1;
        activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = defaultImage;
        rock_Btn.interactable = true;
        paper_Btn.interactable = true;
        scissor_Btn.interactable = true;
        
    }

    public bool RPSSResult;
    public void PlayerGotoJail(Stone first, Stone second, bool whoWon)
    {
        RpssSetToDefault(first, second);

        RPSSResult = true;
        if (whoWon) // on true first pawn jailed
        {
            //AddPawnToJail(first);
            PV.RPC("ComparisonRPC", RpcTarget.AllBuffered, first.gameObject.name,second.AnimName);
        }
        else// on true second pawn jailed
        {
            //AddPawnToJail(second);
            PV.RPC("ComparisonRPC", RpcTarget.AllBuffered, second.gameObject.name, first.AnimName);

        }

    }


    [PunRPC]
    public void ComparisonRPC(string pawnName,string otherPawnColor)
    {
        if (RPSSResult)
        {
            RPSSResult = false;
        }
        else
        {
            return;
        }
        
        switch (pawnName)
        {
            case "RedPawn":
                if (gm.TeamColor == "Red")
                {
                    pawnLose(gm.red4Pawn.GetComponent<Stone>());
                    return;
                }
                else if (otherPawnColor == gm.TeamColor)
                {
                    pawnWin(gm.red4Pawn.GetComponent<Stone>());
                    
                    return;
                }
                else
                {
                    pawnLose(gm.red4Pawn.GetComponent<Stone>());
                }

                break;
            case "PurplePawn":
                if (gm.TeamColor == "Purple")
                {
                    pawnLose(gm.purple4Pawn.GetComponent<Stone>());
                    return;
                }
               else if (otherPawnColor == gm.TeamColor)
                {

                    pawnWin(gm.purple4Pawn.GetComponent<Stone>());

                    return;
                }
                else
                {
                    pawnLose(gm.purple4Pawn.GetComponent<Stone>());
                }
                break;
            case "GreenPawn":
                if (gm.TeamColor == "Green")
                {
                    pawnLose(gm.green4Pawn.GetComponent<Stone>());
                    return;
                }
               else if (otherPawnColor == gm.TeamColor)
                {

                    pawnWin(gm.green4Pawn.GetComponent<Stone>());
                    return;
                }
                else
                {
                    pawnLose(gm.green4Pawn.GetComponent<Stone>());
                }
                break;
            case "BluePawn":
                if (gm.TeamColor == "Blue")
                {
                    pawnLose(gm.blue4Pawn.GetComponent<Stone>());
                    return;
                }
               else if (otherPawnColor == gm.TeamColor)
                {

                    pawnWin(gm.blue4Pawn.GetComponent<Stone>());
                    return;
                }
                else
                {
                    pawnLose(gm.blue4Pawn.GetComponent<Stone>());
                }

                break;
        }
    }

    private void AddPawnToJail(Stone pawnObj)
    {
        
        if (pawnObj.currentRoot.GetComponent<Route>().childNodesList.Count > 8)
        {
            for (int i = 0; i < 8; i++)
            {
                pawnObj.currentRoot.GetComponent<Route>().childNodesList.RemoveAt(8);
            }
        }
        
        pawnObj.routePosition = 0;
        pawnObj.pathSeelcted = false;
        pawnObj.jailed = true;
        pawnObj.jailedImage.enabled = true;
        pawnObj.gameObject.transform.position = pawnObj.currentRoot.childNodesList[0].position;
        pawnObj.gameObject.SetActive(false);
        gm.rpsPanel.transform.DOScale(0, 0.1f);

        
        // add jail animation in the game. 


    }
    public void pawnWin(Stone pawnObj)
    {
        Debug.Log("Win");
        AddPawnToJail(pawnObj);
        GameManager.instance.openRewardPanel(25, "You won the RPS Match and got ");
        GameManager.instance.firstTurn = null;
        GameManager.instance.secondTurn = null;
        GameManager.instance.FPSelectedRPS = "";
        GameManager.instance.SPSelectedRPS = "";

    }
    public void pawnLose(Stone pawnObj)
    {
        Debug.Log("Lose");
        AddPawnToJail(pawnObj);
        StartCoroutine(PlayJailAnim(pawnObj));
        GameManager.instance.firstTurn = null;
        GameManager.instance.secondTurn = null;
        GameManager.instance.FPSelectedRPS = "";
        GameManager.instance.SPSelectedRPS = "";
    }

    IEnumerator PlayJailAnim(Stone pawnObj)
    {

        gm.jailPanel.SetActive(true);
        gm.jailPanel.GetComponent<Jail>().jailedPawn.sprite = pawnObj.pawnImage;

        gm.JailAnim.SetBool(pawnObj.AnimName, true);
        yield return new WaitForSeconds(pawnObj.AnimClip.length + 2f);
        gm.JailAnim.SetBool(pawnObj.AnimName, false);
        gm.jailPanel.SetActive(false);
        enableRPSS = true;


    }

    IEnumerator TieWorking(GameObject fst, GameObject scnd)
    {
        tiePanel.SetActive(true);
        yield return new WaitForSecondsRealtime(2f);
        tiePanel.SetActive(false);
        gm.RPS_PanelActivation(fst, scnd);

        GameManager.instance.firstTurn = null;
        GameManager.instance.secondTurn = null;
        GameManager.instance.FPSelectedRPS = "";
        GameManager.instance.SPSelectedRPS = "";
    }
}