using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class PlayerListing : MonoBehaviourPunCallbacks
{
    public bool ready = false;

    public bool teamSelected = false;


    public Player Player { get; private set; }
    [SerializeField]
    private Text _text;

    public RoomInfo RoomInfo { get; private set; }

    public void SetPlayerInfo(Player player)
    {

        Player = player;
        SetPlayerText(player);

    }

    public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
    {
        base.OnPlayerPropertiesUpdate(target, changedProps);
        if (target != null && target == Player)
        {
            if (changedProps.ContainsKey("RandomNumber"))
            {
                SetPlayerText(target);
            
            }
        
        }
        
    }

    public void SetPlayerText(Player player )
    {
        int result = -1;
        if (player.CustomProperties.ContainsKey("RandomNumber"))
        {
            result = (int)player.CustomProperties["RandomNumber"];
        }
        //_text.text = result.ToString() + " , " + Player.NickName; // Setting Value of individual Player
        _text.text = Player.NickName; 

    }



}
