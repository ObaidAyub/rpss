using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
public class PlayerListingMenu : MonoBehaviourPunCallbacks
{

    private const string TEAM = "team";

    [SerializeField]
    private Transform _content;
    [SerializeField]
    private PlayerListing _playerListing;
    [SerializeField]
    private Text _readyUpText;
    [SerializeField]
    private UI uiManager;

    public List<PlayerListing> _listings = new List<PlayerListing>();
    private RommsCanvas _rommCanvases;
    private bool _ready = false;
    private bool _teamSelected = false;


    private GameController gameController;

    public RawImage ready;


    public void SetDependencies(GameController gameController)
    {
        this.gameController = gameController;
    }

    public void FirstInitialize(RommsCanvas canvases)
    {

        _rommCanvases = canvases;
    }


    public override void OnEnable()
    {
        base.OnEnable();

        if (PhotonNetwork.IsMasterClient)
        {
            SetReadyUp(true);

        }
        else
        {
            SetReadyUp(false);

        }

        GetCurrentRoomPlayers();//Added
    }
    
    private void SetReadyUp(bool state)
    {
        _ready = state;
        if (_ready)
        {

            _readyUpText.text = "R"; // This is ready
            ready.color = new Color(ready.color.r, ready.color.g, ready.color.b, 1f);
        }
        else
        {
            _readyUpText.text = "N"; // this is not ready

            ready.color = new Color(ready.color.r, ready.color.g, ready.color.b, 0.5f);
        }



    }

    public override void OnDisable() // Addedd
    {
        base.OnDisable();
        for (int i = 0; i < _listings.Count; i++)
        {
            Destroy(_listings[i].gameObject);
        }
        _listings.Clear();
    }

    //public override void OnLeftRoom() /Added
    //{
    //    base.OnLeftRoom();
    //    _content.DestroyChildren();
    //}


    private void GetCurrentRoomPlayers()
    {
        if(!PhotonNetwork.IsConnected)
        {
            return;
        }
        if(PhotonNetwork.CurrentRoom == null || PhotonNetwork.CurrentRoom.Players == null )
        {
            return;
        }

        foreach (KeyValuePair<int, Player> playerInfo in PhotonNetwork.CurrentRoom.Players)
        {
            AddPlayerListing(playerInfo.Value);
        
        }

    }



    private void AddPlayerListing(Player player)
    {
        int index = _listings.FindIndex(x => x.Player == player);//Added
        if (index != -1)//Addedd
        {
            _listings[index].SetPlayerInfo(player);
            Debug.Log("local Player added  " + player.NickName + " " + index);
        }
        else//Addedd
        {
            PlayerListing listing = Instantiate(_playerListing, _content);
            if (listing != null)
            {
                listing.SetPlayerInfo(player);
                _listings.Add(listing);
            }

        }
    }

    public override void OnMasterClientSwitched(Player newMasterClient) // e.g : if the ping is high so it can switch the master client
    {
        _rommCanvases.currentRoomCanvas.LeaveRoomMenu.OnClick_LeaveRoom();
      
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerListing(newPlayer);
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)//Added khudse
        {
            var player = PhotonNetwork.CurrentRoom.GetPlayer(1);
            if (player.CustomProperties.ContainsKey(TEAM))
            {
                var occupiedTeam = player.CustomProperties[TEAM];
                uiManager.RestrictTeamChoice((TeamColor)occupiedTeam);
            }
        }

    }
    public void SetPlayerTeam(int teamInt)//Added khudse
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
        {
            var player = PhotonNetwork.CurrentRoom.GetPlayer(1);
            if (player.CustomProperties.ContainsKey(TEAM))
            {
                var occupiedTeam = player.CustomProperties[TEAM];
                teamInt = (int)occupiedTeam == 0 ? 1 : 0;
            }
        }
        PhotonNetwork.LocalPlayer.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() { { TEAM, teamInt } });
        //gameInitializer.InitializeMultiplayerController();
        gameController.SetupCamera((TeamColor)teamInt);
        //gameController.SetLocalPlayer((TeamColor)teamInt);
        //chessGameController.StartNewGame();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);//added

        int index = _listings.FindIndex(x => x.Player==otherPlayer);
        if (index != -1)
        {
            Destroy(_listings[index].gameObject);
            _listings.RemoveAt(index);
        }
    }

    public void OnClick_StartGame()
    {
        SoundManager.PlaySound("btnSound");
        if (PhotonNetwork.IsMasterClient) // Only MasterClient can start the game.
        {
            //GameMusiic.PlaySound("BgSound"); // Turn Off Music
            Debug.Log(_listings.Count);
            for (int i = 0; i < _listings.Count; i++)
            {


                if (_listings[i].Player != PhotonNetwork.LocalPlayer)
                {
                    if (!_listings[i].ready || !_listings[i].teamSelected)
                    {
                        Debug.Log("All Player Are not Ready");
                        return;

                    }

                }

                if (_listings[i].Player == PhotonNetwork.MasterClient)
                {
                    if ( !_listings[i].teamSelected)
                    {
                        return;

                    }

                }
            }

            PhotonNetwork.CurrentRoom.IsOpen = false; // this preventing other players for joing the room
            PhotonNetwork.CurrentRoom.IsVisible = false; // No Longer listed inside the list of other player
           
            PhotonNetwork.LoadLevel(1);

        }

    }

    public void OnClick_ReadyUp() // This code will run if the player is not master
    {
        SoundManager.PlaySound("pawnSound");

        if (!PhotonNetwork.IsMasterClient)
        {
            SetReadyUp(!_ready);
            base.photonView.RPC("RPC_ChangeReadyState", RpcTarget.MasterClient,PhotonNetwork.LocalPlayer, _ready);
            // PHOTON VIEW IS ON PLAYE LISTING MENU
        }

        

    }
    [PunRPC]
    private void RPC_ChangeReadyState( Player player ,  bool ready)
    {
        int index = _listings.FindIndex(x => x.Player == player);
        if (index != -1)
        {
            _listings[index].ready = ready;

            //_listings.Count
            Debug.Log("local Player added  " + player.NickName + " " + index + " " + _listings.Count + " = " + MultiplayerGameController.instance.playerNum);
            if (MultiplayerGameController.instance.playerNum == _listings.Count)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    Debug.Log("Enable Start btn on master ");
                    MultiplayerGameController.instance.startBtn.interactable = true;
                    MultiplayerGameController.instance.startBtn.gameObject.SetActive(true);
                }
                else
                {
                    MultiplayerGameController.instance.startBtn.interactable = false;
                    MultiplayerGameController.instance.startBtn.gameObject.SetActive(false);
                }
            }
            
        }
    }


    public void OnClick_TeamSelected() 
    {
        SoundManager.PlaySound("pawnSound");

        if (!PhotonNetwork.IsMasterClient)
        {
           // SetReadyUp(!_teamSelected);
            base.photonView.RPC("RPC_TeamSelected", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer, true);
        }
        if (PhotonNetwork.IsMasterClient)
        {
            // SetReadyUp(!_teamSelected);
            base.photonView.RPC("RPC_TeamSelected", RpcTarget.MasterClient, PhotonNetwork.MasterClient, true);
        }

    }
    [PunRPC]
    private void RPC_TeamSelected(Player player , bool teamSelected)
    {
        int index = _listings.FindIndex(x => x.Player == player);
        if (index != -1)
        {
            _listings[index].teamSelected = teamSelected;
            Debug.Log("local Player added  " + player.NickName + " " + index + " " + _listings.Count);
            if (MultiplayerGameController.instance.playerNum == _listings.Count)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    Debug.Log("Enable Start btn on master ");
                    MultiplayerGameController.instance.startBtn.interactable = true;
                    MultiplayerGameController.instance.startBtn.gameObject.SetActive(true);
                }
                else
                {
                    MultiplayerGameController.instance.startBtn.interactable = false;
                    MultiplayerGameController.instance.startBtn.gameObject.SetActive(false);
                }

            }

        }

    }

}
