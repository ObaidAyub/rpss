using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.DynamicLinks;
using System;

public class FirebaseUniversalLink : MonoBehaviour
{
    // Start is called before the first frame update
    //   https://cleancommunity.page.link

    public static FirebaseUniversalLink Instance;

    public string url;
    public bool FromLinkCheck;
    void Start()
    {
        Instance = this;
        DynamicLinks.DynamicLinkReceived += OnDynamicLink;

        StartCoroutine(createDeeplink());
    }
    void OnDynamicLink(object sender, EventArgs args)
    {
        var dynamicLinkEventArgs = args as ReceivedDynamicLinkEventArgs;
        Debug.LogFormat("Received dynamic link {0}",
                        dynamicLinkEventArgs.ReceivedDynamicLink.Url.OriginalString);
    
    FromLinkCheck = true;

    }


    public IEnumerator createDeeplink()
    {
        yield return new WaitForSeconds(5);

        createFBuniversalLink();
    }


    public void createFBuniversalLink()
    {
        var components = new DynamicLinkComponents(new System.Uri("https://cleancommunity.page.link/tobR"),"https://cleancommunity.page.link")
        {
            IOSParameters = new IOSParameters(Application.identifier),
            AndroidParameters = new AndroidParameters(Application.identifier),
        };

        var options = new Firebase.DynamicLinks.DynamicLinkOptions
        {
            PathLength = DynamicLinkPathLength.Unguessable
        };

        Firebase.DynamicLinks.DynamicLinks.GetShortLinkAsync(components, options).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("GetShortLinkAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("GetShortLinkAsync encountered an error: " + task.Exception);
                return;
            }

            // Short Link has been created.
            Firebase.DynamicLinks.ShortDynamicLink link = task.Result;
            Debug.LogFormat("Generated short link {0}", link.Url);
            url = link.Url.ToString();
            var warnings = new System.Collections.Generic.List<string>(link.Warnings);
            if (warnings.Count > 0)
            {
                // Debug logging for warnings generating the short link.
            }
        });
    }
}
