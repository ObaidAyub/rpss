using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class LeaveRoomMenu : MonoBehaviour
{
    public static LeaveRoomMenu instance;

    private RommsCanvas _rommCanvas;

    private void Awake()
    {
        instance = this;
    }
    public void OnClick_LeaveRoom()
    {
        SoundManager.PlaySound("btnSound");
        PhotonNetwork.LeaveRoom(true);

        _rommCanvas.currentRoomCanvas.hide();
        Debug.Log("Sucesssfully leave room ");
        CreateRoomMenu.instance.privateShareBtn.gameObject.SetActive(false);

        RoomListingMenu.instance.DestroyRoomOnLeave();
        RommsCanvas.instance.RoomType = "";
    }

    public void FirstInitialize(RommsCanvas canvases)
    {
        _rommCanvas = canvases;
    }
}
