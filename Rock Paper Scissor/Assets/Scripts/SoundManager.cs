using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

	//public GameObject gameMusic;
	static AudioSource audioSource;
	public static AudioClip btnSound;
	public static AudioClip pawnSound;
	public static AudioClip diceRollSound;


	private void Awake()
	{
		//  DontDestroyOnLoad(gameMusic);
		//DontDestroyOnLoad(this.gameObject);
	}
	// Start is called before the first frame update
	void Start()
	{

		audioSource = GetComponent<AudioSource>();
		btnSound = Resources.Load<AudioClip>("btnSound");
		pawnSound = Resources.Load<AudioClip>("pawnSound");
		diceRollSound = Resources.Load<AudioClip>("diceRollSound");
	}

	public static void PlaySound(string clip)
	{
		if (SceneManager.GetActiveScene().buildIndex == 0)
		{
			if (PlayerPrefs.GetInt("Sound", 1) == 1)
				switch (clip)
				{
					case "btnSound":
						audioSource.PlayOneShot(btnSound);
						break;
					case "pawnSound":
						audioSource.PlayOneShot(pawnSound);
						break;
					case "diceRollSound":
						audioSource.PlayOneShot(diceRollSound);
						break;
				}
		}
	}
}
