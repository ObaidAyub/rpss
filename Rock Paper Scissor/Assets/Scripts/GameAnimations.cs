using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameAnimations : MonoBehaviour
{
	public static GameAnimations instance;

	public RectTransform signUp, logIn, forgetPassword, leaderboard, setting, resetDisplayName, avatarPanel, dailyrewardPanel, inAppScreen, userDetailScreen;
	Vector3 loginScale;
	Vector3 forgetPasswordScale;
	Vector3 signUpScale;
	Vector3 resetDisplayNameScale;

	// Start is called before the first frame update
	void Start()
	{
		instance = this;

		

	}
	public void onStart()
    {
		logIn.gameObject.SetActive(true);
		logIn.transform.DOScale(1, 1f);
	}
	public void LoginAnimation()
	{

		logIn.transform.DOScale(1, 1f);
		resetDisplayNameScale = new Vector3(0, 0, 0);
		resetDisplayName.transform.DOScale(resetDisplayNameScale, 1f);



		signUpScale = new Vector3(0, 0, 0);
		signUp.transform.DOScale(signUpScale, 1f);

		forgetPasswordScale = new Vector3(0, 0, 0);
		forgetPassword.transform.DOScale(forgetPasswordScale, 1f);


	}

	public void ForgotPassAnimation()
	{
		logIn.transform.DOScale(0, 1f);

		forgetPassword.transform.DOScale(1, 1f);

	}

	public void SignUpAnimation()
	{

		resetDisplayName.transform.DOScale(0, 1f);

		signUp.transform.DOScale(1, 1f);

		logIn.transform.DOScale(0, 1f);
	}
	
	IEnumerator ResetScreen(GameObject Gobj)
	{
		yield return new WaitForSeconds(1f);
		Gobj.gameObject.SetActive(false);
	}
	
	public void LeaderBoardAnimation()
	{

		signUp.transform.DOScale(0, 0.5f);

		if (leaderboard.gameObject.activeSelf == false)
		{
			leaderboard.gameObject.SetActive(true);
			leaderboard.DOMoveY(0f, 1f);

			resetDisplayName.transform.DOScale(0, 1f);

			logIn.transform.DOScale(0, 1f);

		}
		else
		{
			signUp.transform.DOScale(1, 0.5f);

			leaderboard.DOMoveY(120f, 1f);
			StartCoroutine(ResetScreen(leaderboard.gameObject));

		}


	}
	public void InAppScrAnimation()
	{

		signUp.transform.DOScale(0, 0.5f);

		if ( inAppScreen.gameObject.activeSelf == false)
		{
			inAppScreen.gameObject.SetActive(true);
			inAppScreen.DOMoveY(0f, 1f);

			resetDisplayName.transform.DOScale(0, 1f);

			logIn.transform.DOScale(0, 1f);

		}
		else
		{
			signUp.transform.DOScale(1, 0.5f);

			inAppScreen.DOMoveY(120f, 1f);
			StartCoroutine(ResetScreen(inAppScreen.gameObject));

		}


	}
	public void UserDetailScrAnimation()
	{
		signUp.transform.DOScale(0, 0.5f);

		if (userDetailScreen.gameObject.activeSelf == false)
		{
			userDetailScreen.gameObject.SetActive(true);
			userDetailScreen.DOMoveY(0f, 1f);

			resetDisplayName.transform.DOScale(0, 1f);

			logIn.transform.DOScale(0, 1f);
			inAppScreen.DOMoveY(120f, 1f);
		}
		else
		{
			signUp.transform.DOScale(1, 0.5f);
			userDetailScreen.DOMoveY(120f, 1f);
			StartCoroutine(ResetScreen(userDetailScreen.gameObject));
			

		}


	}
	public void DailyRewardAnimation(bool check, GameObject popup)
	{


		if (!check)
		{
			dailyrewardPanel.gameObject.SetActive(true);
			dailyrewardPanel.DOMoveY(120f, 2f);

			WaitToOff(popup);

		}
		else
		{
			dailyrewardPanel.DOMoveY(0f, 1f);

		}


	}
	IEnumerator WaitToOff(GameObject popup)
	{
		Debug.LogError("WaitToOff");
		yield return new WaitForSeconds(2f);
		popup.SetActive(false);
	}
	IEnumerator WaitSettings()
	{
		yield return new WaitForSeconds(1f);
		setting.gameObject.SetActive(false);
	}

	public void SettingAnimation()
	{
		signUp.transform.DOScale(0, 1f);



		if (setting.gameObject.activeSelf == false)
		{
			setting.gameObject.SetActive(true);
			setting.DOMoveY(0f, 1f);
		}

	}

	public void OffSettingAnimation()
	{
		signUp.transform.DOScale(1, 1f);


		if (setting.gameObject.activeSelf == true)
		{
			setting.DOMoveY(100f, 1f);
			StartCoroutine(WaitSettings());


		}

	}


	public void ResetDisplayNameAnimation()
	{
		signUp.transform.DOScale(0, 1f);
		logIn.transform.DOScale(0, 1f);
		resetDisplayName.transform.DOScale(1, 1f);
	}

	public void CloseResetDisplayNameAnimation()
	{
		signUp.transform.DOScale(1, 1f);
		resetDisplayName.transform.DOScale(0, 1f);
	}

	IEnumerator WaitAvatarPanel()
	{
		yield return new WaitForSeconds(1f);
		avatarPanel.gameObject.SetActive(false);
	}
	public void AvatarAnimation()
	{

		signUp.transform.DOScale(0, 0.5f);
		if (avatarPanel.gameObject.activeSelf == false)
		{
			avatarPanel.gameObject.SetActive(true);
			avatarPanel.DOMoveY(-110f, 1f);
		}

	}

	public void OffAvatarPanel()
	{


		if (avatarPanel.gameObject.activeSelf == true)
		{


			avatarPanel.DOMoveY(100f, 1f);
			StartCoroutine(WaitAvatarPanel());

		}

	}

}
