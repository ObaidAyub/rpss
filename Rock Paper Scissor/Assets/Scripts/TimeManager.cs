using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    /* 
          necessary variables to hold all the things we need.
        php url
        timedata, the data we get back
        current time
        current date
    */

    public static TimeManager sharedInstance;
    string _url = "http://leatonm.net/wp-content/uploads/2017/candlepin/getdate.php";
    public string _timeData;
    public string _currentTime;
    public string _currentDate;
    public string _currentDay;//
    public string _currentMonth;//
    public string _currentYear;//


    //make sure there is only one instance of this always.
    void Awake()
    {
        if (sharedInstance == null)
        {
            sharedInstance = this;
        }
        else if (sharedInstance != this)
        {
            Destroy(this);
        }
        //DontDestroyOnLoad(this);
    }
    

    //time fether coroutine
    public IEnumerator getTime()
    {
        //Debug.Log("==> step 1. Getting info from internet now!");
        WWW www = new WWW(_url);
        yield return www;
        _timeData = www.text;
        //Debug.Log("==> step 2. Got the info from internet!" + _timeData);
        if (string.IsNullOrEmpty(_timeData))
        {
            yield break;
        }

        string[] words = _timeData.Split('/');
        //timerTestLabel.text = www.text;
        //Debug.Log("The date is : " + words[0]);
        //Debug.Log("The time is : " + words[1]);

        //setting current time
        _currentDate = words[0];
        _currentTime = words[1];

        //Splitting date
        string[] day = _currentDate.Split('-');
        _currentDay = day[0];
        _currentMonth = day[1];
        _currentYear = day[2];
        //Debug.LogError("day : " + day[0]);
        //Debug.LogError("month : " + day[1]);
        //Debug.LogError("year : " + day[2]);
    }


    //get the current time at startup
    void Start()
    {
        //Debug.Log("==> TimeManager script is Ready.");
        //StartCoroutine("getTime");
    }

    //get the current date
    public string getCurrentDateNow()
    {
        //Debug.LogError("_currentDate " + _currentDate);
        return _currentDate;
    }
    public string getDayNow()
    {
        return _currentDay;
    }
    public string getMonthNow()
    {
        return _currentMonth;
    }
    public string getYearNow()
    {
        return _currentYear;
    }


    //get the current Time
    public string getCurrentTimeNow()
    {
        Debug.Log("_currentTime " + _currentTime);
        return _currentTime;
    }


}