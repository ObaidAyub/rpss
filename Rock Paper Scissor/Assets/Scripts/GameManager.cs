using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using PlayFab;
using PlayFab.ClientModels;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Newtonsoft.Json;

public class GameManager : MonoBehaviourPunCallbacks
{
	public static GameManager instance;

	public string TeamColor;

	[Header("User Data")]
	public Text userName;
	public Text points;
	public Text SafeCards;

	[Header("Main Camera")]
	public GameObject mainCamera;

	[Header("Inapp Packages")]
	public GameObject InAppObj;
	public Button InappButton;

	public Transform green2Pawn;
	public Transform red2Pawn;
	public Transform red4Pawn;
	public Transform green4Pawn;
	public Transform blue4Pawn;
	public Transform purple4Pawn;
	public Text centerText;
	public Transform mainLight;
	//public SpriteRenderer[] arrowSprites;
	public Transform activePlayer;



	public enum Mode
	{
		TwoPlayers,
		FourPlayers
	};
	public static Mode modeType;

	[Header("RPS Content")]
	public GameObject rpsPanel;
	public GameObject jailPanel;
	public Animator JailAnim;
	public Stone firstTurn;
	public string FPSelectedRPS;
	public Stone secondTurn;
	public string SPSelectedRPS;

	[Header("Score Elements")]
	public Text myScore;
	public bool winScore;

	public Authentication authenticationManager;
	public Player player { get; private set; }
	private ExitGames.Client.Photon.Hashtable _myCustomProperties = /*new ExitGames.Client.Photon.Hashtable();*/new ExitGames.Client.Photon.Hashtable();
	[Header("Camera Positions for Teams")]
	public Transform redCamPos;
	public Transform blueCamPos;
	public Transform greenCamPos;
	public Transform purpleCamPos;
	[Header("Light Positions for Teams")]
	public Transform blueLightPos;
	public Transform greenLightPos;

	public GameObject PathSelectionScreen;
	public Button RedArrow, PurpleArrow, GreenArrow, BlueArrow;

	public GameObject rewardScreen;


	private PhotonView PV;
	private void OnEnable()
	{
		if ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Red")
        {
            SetCameraPos(redCamPos);
        }
        else if ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Purple")
		{
			SetCameraPos(purpleCamPos);
		}
		else if ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Blue")
		{
			SetCameraPos(blueCamPos);
			mainLight.position = blueLightPos.position;
			mainLight.rotation = blueLightPos.rotation;

		}
		else if ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Green")
		{
			SetCameraPos(greenCamPos);
			mainLight.position = greenLightPos.position;
			mainLight.rotation = greenLightPos.rotation;
		}

	}

    private void SetCameraPos(Transform camPos)
    {
        mainCamera.transform.position = camPos.position;
        mainCamera.transform.rotation = camPos.rotation;
        TeamColor = (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
    }

    private void Awake()
	{
		PV = GetComponent<PhotonView>();
		instance = this;
	}
	void Start()
	{
        if (string.IsNullOrEmpty( UI.instance.userNameText))
        {

		userName.text = UI.instance.displayName.ToString();
        }
        else
        {
			userName.text = UI.instance.userNameText;
		}
		points.text = UI.instance.Score.ToString();
		SafeCards.text = UI.instance.SafeCard.ToString();
		switch (PlayFabManager.instance.playerNum)
		{
			case 2:
                {
                    modeType = Mode.TwoPlayers;
                    PlayerSelection(true,false);
                }
                break;
			case 4:
				{
					modeType = Mode.FourPlayers;
					PlayerSelection(true, true);
				}
				break;
		}
		SetAvatarDataOnGamePlay(TeamColor);

		//InappButton.onClick.RemoveAllListeners();
		//InappButton.onClick.AddListener(OpenInAppScreen);


	}

    private void PlayerSelection(bool TwoPlayer, bool FourPlayer)
    {
        AssignTurns.instance.Onstart();


        red4Pawn.gameObject.GetComponent<Stone>().jailedImage.gameObject.SetActive(TwoPlayer);
        green4Pawn.gameObject.GetComponent<Stone>().jailedImage.gameObject.SetActive(TwoPlayer);

        blue4Pawn.gameObject.GetComponent<Stone>().jailedImage.gameObject.SetActive(FourPlayer);
        purple4Pawn.gameObject.GetComponent<Stone>().jailedImage.gameObject.SetActive(FourPlayer);

        red4Pawn.gameObject.SetActive(TwoPlayer);
        green4Pawn.gameObject.SetActive(TwoPlayer);
        blue4Pawn.gameObject.SetActive(FourPlayer);
        purple4Pawn.gameObject.SetActive(FourPlayer);

        for (int i = 0; i < 3; i++)
        {
            red4Pawn.GetComponent<Stone>().safePawn[i].SetActive(TwoPlayer);
            green4Pawn.GetComponent<Stone>().safePawn[i].SetActive(TwoPlayer);
            blue4Pawn.GetComponent<Stone>().safePawn[i].SetActive(FourPlayer);
            purple4Pawn.GetComponent<Stone>().safePawn[i].SetActive(FourPlayer);

        }
        redScoreCard.SetActive(TwoPlayer);
        greenScoreCard.SetActive(TwoPlayer);
        blueScoreCard.SetActive(FourPlayer);
        purpleScoreCard.SetActive(FourPlayer);
    }


    public void SetAvatarDataOnGamePlay(string PlayerColor)
	{

		PlayFabManager.instance.PlayerName = UI.instance.userName;

		avatarSystem.SetAvatar(PlayFabManager.instance.avatardata, PlayerColor, PlayFabManager.instance.PlayerName);

		PV.RPC("GetAvatarData", RpcTarget.AllBuffered, PlayerColor, PlayFabManager.instance.avatardata, PlayFabManager.instance.PlayerName);
	}
	[PunRPC]
	public void GetAvatarData(string teamColor, string avatarData, string playerName)
	{
		Debug.Log("GetAvatarData  = " + avatarData);
		Debug.Log("Color  = " + teamColor);
		if (TeamColor == teamColor)
		{
			Debug.Log("avatar data same return");
			return;
		}
		else
		{
			Debug.Log("Data  = " + avatarData + " data recive of this color " + teamColor);
			avatarSystem.SetAvatar(avatarData, teamColor, playerName);
		}


	}

	public void RPS_PanelActivation(GameObject Player, GameObject Opponent)
	{
		RedPawn(Player, Opponent);

		BluePawn(Player, Opponent);

		GreenPawn(Player, Opponent);

		PurplePawn(Player, Opponent);
	}

	private void PurplePawn(GameObject Player, GameObject Opponent)
	{
		if ((Player.CompareTag("Purple") || Opponent.CompareTag("Purple")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Purple"))
		{
			rpsPanel.SetActive(true);
			rpsPanel.transform.DOScale(1, 0.5f);

			if ((Player.CompareTag("Purple") && (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Purple"))
			{

				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Player.GetComponent<Stone>().pawnImage;

			}
			else
			{
				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Opponent.GetComponent<Stone>().pawnImage;

			}

		}

	}

	private void GreenPawn(GameObject Player, GameObject Opponent)
	{
		if ((Player.CompareTag("Green") || Opponent.CompareTag("Green")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Green"))
		{
			rpsPanel.SetActive(true);
			rpsPanel.transform.DOScale(1, 0.5f);

			if ((Player.CompareTag("Green") && (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Green"))
			{

				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Player.GetComponent<Stone>().pawnImage;

			}
			else
			{
				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Opponent.GetComponent<Stone>().pawnImage;

			}

		}
	}

	private void BluePawn(GameObject Player, GameObject Opponent)
	{
		if ((Player.CompareTag("Blue") || Opponent.CompareTag("Blue")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Blue"))
		{
			rpsPanel.SetActive(true);
			rpsPanel.transform.DOScale(1, 0.5f);

			if ((Player.CompareTag("Blue") && (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Blue"))
			{

				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Player.GetComponent<Stone>().pawnImage;

			}
			else
			{
				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Opponent.GetComponent<Stone>().pawnImage;

			}

		}
	}

	private void RedPawn(GameObject Player, GameObject Opponent)
	{
		if ((Player.CompareTag("Red") || Opponent.CompareTag("Red")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Red"))
		{
			Debug.Log("RPS_PanelActivation for Red and Player : " + Player.name + "Opponent : " + Opponent.name);
			rpsPanel.SetActive(true);
			rpsPanel.transform.DOScale(1, 0.5f);
			if ((Player.CompareTag("Red") && (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Red"))
			{

				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Player.GetComponent<Stone>().pawnImage;

			}
			else
			{
				rpsPanel.GetComponent<RPS>().activePawnImg.sprite = Opponent.GetComponent<Stone>().pawnImage;

			}

		}
	}

	[PunRPC]
	public void Activation(GameObject collidingPawn, GameObject collidedPawn)
	{
		if ((collidingPawn.CompareTag("Red") || collidedPawn.CompareTag("Red")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Red"))
		{

			rpsPanel.SetActive(true);
			RPS.instance.activePawnImg.sprite = collidingPawn.GetComponent<Stone>().pawnImage;

			firstTurn.selectedRPS = Stone.ChoosenRPS.None;
			secondTurn.selectedRPS = Stone.ChoosenRPS.None;
			RPS.instance.turn = 1;
			RPS.instance.activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = RPS.instance.defaultImage;
		}
		if ((collidingPawn.CompareTag("Blue") || collidedPawn.CompareTag("Blue")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Blue"))
		{
			rpsPanel.SetActive(true);
			rpsPanel.GetComponent<RPS>().activePawnImg.sprite = collidingPawn.GetComponent<Stone>().pawnImage;

			firstTurn.selectedRPS = Stone.ChoosenRPS.None;
			secondTurn.selectedRPS = Stone.ChoosenRPS.None;
			RPS.instance.turn = 1;
			RPS.instance.activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = RPS.instance.defaultImage;
		}
		if ((collidingPawn.CompareTag("Green") || collidedPawn.CompareTag("Green")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Green"))
		{

			rpsPanel.SetActive(true);
			rpsPanel.GetComponent<RPS>().activePawnImg.sprite = collidingPawn.GetComponent<Stone>().pawnImage;

			firstTurn.selectedRPS = Stone.ChoosenRPS.None;
			secondTurn.selectedRPS = Stone.ChoosenRPS.None;
			RPS.instance.turn = 1;
			RPS.instance.activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = RPS.instance.defaultImage;
		}
		if ((collidingPawn.CompareTag("Purple") || collidedPawn.CompareTag("Purple")) && ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == "Purple"))
		{

			rpsPanel.SetActive(true);
			rpsPanel.GetComponent<RPS>().activePawnImg.sprite = collidingPawn.GetComponent<Stone>().pawnImage;

			firstTurn.selectedRPS = Stone.ChoosenRPS.None;
			secondTurn.selectedRPS = Stone.ChoosenRPS.None;
			RPS.instance.turn = 1;
			RPS.instance.activePawnImg.gameObject.transform.GetChild(0).GetComponent<Image>().sprite = RPS.instance.defaultImage;
		}
	}
	
	public void SelectRedPath()
	{

		activePlayer.GetComponent<Stone>().PathMoveIndication(0);

	}
	public void SelectPurplePath()
	{

		activePlayer.GetComponent<Stone>().PathMoveIndication(1);

	}

	public void SelectGreenPath()
	{
		activePlayer.GetComponent<Stone>().PathMoveIndication(2);

	}
	public void SelectBluePath()
	{

		activePlayer.GetComponent<Stone>().PathMoveIndication(3);

	}
	public GameObject JailScreen;
	public Image red, purple, green, blue;

	public GameObject popup;
	public Text popupText;
	//public Button useSafeCardBtn, buySafeCardBtn;
	public void onJailClick()
	{
		switch (TeamColor)
		{
            case "Red":
                if (!red4Pawn.gameObject.GetComponent<Stone>().jailed)
                {
                    return;
                }
                EnableJailScreen(true,false, false, false);
                break;
            case "Purple":
				if (!purple4Pawn.gameObject.GetComponent<Stone>().jailed)
				{
					return;
				}

				EnableJailScreen(false, true, false, false);
				break;
			case "Green":
				if (!green4Pawn.gameObject.GetComponent<Stone>().jailed)
				{
					return;
				}
				EnableJailScreen(false, false, true, false);
				break;
			case "Blue":
				if (!blue4Pawn.gameObject.GetComponent<Stone>().jailed)
				{
					return;
				}
				EnableJailScreen(false, false, false, true);
				break;
		}
		JailScreen.SetActive(true);

	}

    private void EnableJailScreen(bool redAct, bool purpleAct, bool greenAct, bool blueAct)
    {
        red.gameObject.SetActive(redAct);
        purple.gameObject.SetActive(purpleAct);
        green.gameObject.SetActive(greenAct);
        blue.gameObject.SetActive(blueAct);
    }

    public void useSafeCard()
	{
		Debug.Log("use safe cards");
		int val;
		val = UI.instance.Getint("SafeCard");

		if (val <= 0)
		{
			Debug.Log("You have 0 Safe Cards");
			BuySafeCard();
			return;
		}
		switch (TeamColor)
		{
			case "Red":
				UnlockFromJail(red4Pawn);
				PV.RPC("UnJailPawn", RpcTarget.AllBuffered, "Red");
				break;
			case "Purple":
				UnlockFromJail(purple4Pawn);
				PV.RPC("UnJailPawn", RpcTarget.AllBuffered, "Purple");
				break;
			case "Green":
				UnlockFromJail(green4Pawn);
				PV.RPC("UnJailPawn", RpcTarget.AllBuffered, "Green");
				break;
			case "Blue":
				UnlockFromJail(blue4Pawn);
				PV.RPC("UnJailPawn", RpcTarget.AllBuffered, "Blue");
				break;
		}

		int useSafeCard = 1;
		val -= useSafeCard;
		UI.instance.SetInt("SafeCard", val);
		UI.instance.SafeCard = val;
		SafeCards.text = UI.instance.Getint("SafeCard").ToString();
		popup.SetActive(true);
		popupText.text = "Pawn successfully out from Jail...";

		Authentication.Instance.SaveAppearence("Score", UI.instance.Getint("Score"),
			"SafeCard", UI.instance.Getint("SafeCard"),
			"Win", UI.instance.Getint("Win"),
			"Lose", UI.instance.Getint("Lose"),
			"_date", UI.instance.Getint("_date"),
			"Hours", UI.instance.Getint("hours"),
			"Minutes", UI.instance.Getint("minutes"),
			"Seconds", UI.instance.Getint("seconds"));

		UI.instance.UpdateScore();

	}
	void UnlockFromJail(Transform Pawn)
	{
		Debug.Log("use safe cards" + Pawn.name);
		Pawn.gameObject.SetActive(true);
		Pawn.gameObject.GetComponent<Stone>().jailed = false;
		Pawn.gameObject.GetComponent<Stone>().onImaginary = false;
		Pawn.gameObject.GetComponent<Stone>().jailedImage.enabled = false;
		Pawn.gameObject.GetComponent<Stone>().routePositionImaginary = 0;
		Pawn.gameObject.GetComponent<Stone>().routePosition = 0;
		//Pawn.gameObject.GetComponent<Stone>().doubleTurn = true;

	}

	[PunRPC]
	public void UnJailPawn(string PawnName)
	{
		Debug.Log("UnJailPawn");
		if (TeamColor == PawnName)
		{
			return;
		}
		switch (PawnName)
		{
			case "Red":
				UnlockFromJail(red4Pawn);
				break;
			case "Purple":
				UnlockFromJail(purple4Pawn);
				break;
			case "Green":
				UnlockFromJail(green4Pawn);
				break;
			case "Blue":
				UnlockFromJail(blue4Pawn);
				break;
		}
		popup.SetActive(true);
		popupText.text = PawnName + " Pawn is out from Jail using SafeCard...";
		Debug.Log("Pawn is out from Jail using SafeCard..");
	}
	public void BuySafeCard()
	{
		OpenInAppScreen();
	}

	[Header("Final Screen")]
	public GameObject FinalScree;
	public Text congrats;
	public GameObject redScoreCard, blueScoreCard, greenScoreCard, purpleScoreCard;

	[Header("Red final text")]
	public Text redCongrats;
	public Text redSafecard;
	public Text redScore;
	public Text redWon;
	public Text redLose;

	[Header("Purple final text")]
	public Text PurpleCongrats;
	public Text PurpleSafecard;
	public Text PurpleScore;
	public Text PurpleWon;
	public Text PurpleLose;

	[Header("Green final text")]
	public Text GreenCongrats;
	public Text GreenSafecard;
	public Text GreenScore;
	public Text GreenWon;
	public Text GreenLose;

	[Header("Blue final text")]
	public Text BlueCongrats;
	public Text BlueSafecard;
	public Text BlueScore;
	public Text BlueWon;
	public Text BlueLose;

	[Header("Player Profiles")]
	public Character redPlayer;
	public Character purplePlayer;
	public Character bluePlayer;
	public Character greenPlayer;
	public Text redPlayerName, purplePlayerName, bluePlayerName, greenPlayerName;

	public AvatarSystem avatarSystem;


    #region for plaayer price amount distributions

    float totalPrice = 500, firstPosPrice = 26, secondPosPrice = 16, thirdPosPrice = 10, forthPosPrice = 6.8f;

	public float PriceValue(float total, float price)
	{
		var amount = (total / 100) * price;

		return amount;
	}
    #endregion
    public void ShowFinalGameScreen(GameObject WinPlayer, bool isPlyTeamWin)
	{
		Debug.Log("FIal screen Show");
		FinalScree.SetActive(true);
		FinalScree.transform.GetChild(0).DOScale(1, 1);

		var player = WinPlayer.GetComponent<Stone>();

        //switch (player.name)
        //{
        //	case "RedPawn":
        //		Debug.Log("Red Won");
        //              if (player.name == TeamColor)
        //              {
        //			congrats.text = "Congratulation You Win the Game";
        //			UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);

        //		}
        //              else
        //              {
        //			congrats.text = "You lose the Game";
        //		}

        //		redCongrats.text = "WINNER";
        //		redSafecard.text = "Safe Card: 1";
        //		redScore.text = "Points: 500";
        //		redWon.text = "Win: 1";
        //		redLose.text = "Lose: 0";

        //		PurpleCongrats.text = "LOSER";
        //		PurpleSafecard.text = "Safe Card: 0";
        //		PurpleScore.text = "Points: 100";
        //		PurpleWon.text = "Win: 0";
        //		PurpleLose.text = "Lose: 1";

        //		GreenCongrats.text = "LOSER";
        //		GreenSafecard.text = "Safe Card: 0";
        //		GreenScore.text = "Points: 100";
        //		GreenWon.text = "Win: 0";
        //		GreenLose.text = "Lose: 1";

        //		BlueCongrats.text = "LOSER";
        //		BlueSafecard.text = "Safe Card: 0";
        //		BlueScore.text = "Points: 100";
        //		BlueWon.text = "Win: 0";
        //		BlueLose.text = "Lose: 1";





        //		break;
        //	case "PurplePawn":
        //		Debug.Log("purple Won");
        //		if (player.name == TeamColor)
        //		{
        //			congrats.text = "Congratulation You Win the Game";
        //			UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);

        //		}
        //		else
        //		{
        //			congrats.text = "You lose the Game";
        //			UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
        //		}

        //		redCongrats.text = "LOSER";
        //		redSafecard.text = "Safe Card: 0";
        //		redScore.text = "Points: 100";
        //		redWon.text = "Win: 0";
        //		redLose.text = "Lose: 1";

        //		PurpleCongrats.text = "WINNER";
        //		PurpleSafecard.text = "Safe Card: 1";
        //		PurpleScore.text = "Points: 500";
        //		PurpleWon.text = "Won: 1";
        //		PurpleLose.text = "Lose: 0";

        //		GreenCongrats.text = "LOSER";
        //		GreenSafecard.text = "Safe Card: 0";
        //		GreenScore.text = "Points: 100";
        //		GreenWon.text = "Win: 0";
        //		GreenLose.text = "Lose: 1";

        //		BlueCongrats.text = "LOSER";
        //		BlueSafecard.text = "Safe Card: 0";
        //		BlueScore.text = "Points: 100";
        //		BlueWon.text = "Win: 0";
        //		BlueLose.text = "Lose: 1";



        //		break;
        //	case "GreenPawn":
        //		Debug.Log("green Won");
        //		if (player.name == TeamColor)
        //		{
        //			congrats.text = "Congratulation You Win the Game";
        //			UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);

        //		}
        //		else
        //		{
        //			congrats.text = "You lose the Game";
        //			UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
        //		}

        //		redCongrats.text = "LOSER";
        //		redSafecard.text = "Safe Card: 0";
        //		redScore.text = "Points: 100";
        //		redWon.text = "Win: 0";
        //		redLose.text = "Lose: 1";

        //		PurpleCongrats.text = "LOSER";
        //		PurpleSafecard.text = "Safe Card: 0";
        //		PurpleScore.text = "Points: 100";
        //		PurpleWon.text = "Win: 0";
        //		PurpleLose.text = "Lose: 1";

        //		GreenCongrats.text = "WINNER";
        //		GreenSafecard.text = "Safe Card: 1";
        //		GreenScore.text = "Points: 500";
        //		GreenWon.text = "Won: 1";
        //		GreenLose.text = "Lose: 0";

        //		BlueCongrats.text = "LOSER";
        //		BlueSafecard.text = "Safe Card: 0";
        //		BlueScore.text = "Points: 100";
        //		BlueWon.text = "Win: 0";
        //		BlueLose.text = "Lose: 1";



        //		break;
        //	case "BluePawn":
        //		Debug.Log("Blue Won");
        //		if (player.name == TeamColor)
        //		{
        //			congrats.text = "Congratulation You Win the Game";
        //			UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);

        //		}
        //		else
        //		{
        //			congrats.text = "You lose the Game";
        //			UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
        //		}

        //		redCongrats.text = "LOSER";
        //		redSafecard.text = "Safe Card: 0";
        //		redScore.text = "Points: 100";
        //		redWon.text = "Win: 0";
        //		redLose.text = "Lose: 1";

        //		PurpleCongrats.text = "LOSER";
        //		PurpleSafecard.text = "Safe Card: 0";
        //		PurpleScore.text = "Points: 100";
        //		PurpleWon.text = "Win: 0";
        //		PurpleLose.text = "Lose: 1";

        //		GreenCongrats.text = "LOSER";
        //		GreenSafecard.text = "Safe Card: 0";
        //		GreenScore.text = "Points: 100";
        //		GreenWon.text = "Win: 0";
        //		GreenLose.text = "Lose: 1";

        //		BlueCongrats.text = "WINNER";
        //		BlueSafecard.text = "Safe Card: 1";
        //		BlueScore.text = "Points: 500";
        //		BlueWon.text = "Won: 1";
        //		BlueLose.text = "Lose: 0";



        //		break;
        //}
        if (isPlyTeamWin)
        {

		PV.RPC("GameComplete", RpcTarget.AllBuffered, player.name);
        }

	}
	[PunRPC]
	public void GameComplete(string PawnName)
	{
		Debug.Log("GameComplete " + PawnName);

				int score = 0;
		switch (PawnName)
		{
			case "RedPawn":

				Debug.Log("RED Won");
				if ("Red" == TeamColor)
                {
                    congrats.text = "Congratulation You Win the Game";
					score = 500;
					UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);
				}
                else
                {
                    congrats.text = "You lose the Game";
					UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
				}

                redCongrats.text = "WINNER";
				redSafecard.text = "Safe Card: 1";
				redWon.text = "Win: 1";
				redLose.text = "Lose: 0";

				PurpleCongrats.text = "LOSER";
				PurpleSafecard.text = "Safe Card: 0";
				PurpleWon.text = "Win: 0";
				PurpleLose.text = "Lose: 1";

				GreenCongrats.text = "LOSER";
				GreenSafecard.text = "Safe Card: 0";
				GreenWon.text = "Win: 0";
				GreenLose.text = "Lose: 1";

				BlueCongrats.text = "LOSER";
				BlueSafecard.text = "Safe Card: 0";
				BlueWon.text = "Win: 0";
				BlueLose.text = "Lose: 1";

				break;
			case "PurplePawn":
				
				Debug.Log("purple Won");
				if ("Purple" == TeamColor)
				{
					congrats.text = "Congratulation You Win the Game";
					UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);
				}
				else
				{
					congrats.text = "You lose the Game";
					UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
				}

				redCongrats.text = "LOSER";
				redSafecard.text = "Safe Card: 0";
				redWon.text = "Win: 0";
				redLose.text = "Lose: 1";

				PurpleCongrats.text = "WINNER";
				PurpleSafecard.text = "Safe Card: 1";
				PurpleWon.text = "Won: 1";
				PurpleLose.text = "Lose: 0";

				GreenCongrats.text = "LOSER";
				GreenSafecard.text = "Safe Card: 0";
				GreenWon.text = "Win: 0";
				GreenLose.text = "Lose: 1";

				BlueCongrats.text = "LOSER";
				BlueSafecard.text = "Safe Card: 0";
				BlueWon.text = "Win: 0";
				BlueLose.text = "Lose: 1";
				break;
			case "GreenPawn":
				
				if ("Green" == TeamColor)
				{
					congrats.text = "Congratulation You Win the Game";
					UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);
				}
				else
				{
					congrats.text = "You lose the Game";
					UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
				}

				redCongrats.text = "LOSER";
				redSafecard.text = "Safe Card: 0";
				redWon.text = "Win: 0";
				redLose.text = "Lose: 1";

				PurpleCongrats.text = "LOSER";
				PurpleSafecard.text = "Safe Card: 0";
				PurpleWon.text = "Win: 0";
				PurpleLose.text = "Lose: 1";

				GreenCongrats.text = "WINNER";
				GreenSafecard.text = "Safe Card: 1";
				GreenWon.text = "Won: 1";
				GreenLose.text = "Lose: 0";

				BlueCongrats.text = "LOSER";
				BlueSafecard.text = "Safe Card: 0";
				BlueWon.text = "Win: 0";
				BlueLose.text = "Lose: 1";
				break;
			case "BluePawn":
				
				Debug.Log("Blue Won");
				if ("Blue" == TeamColor)
				{
					congrats.text = "Congratulation You Win the Game";
					UpdateScore("Score", 500, "SafeCard", 1, "Win", 1, "Lose", 0);
				}
				else
				{
					congrats.text = "You lose the Game";
					UpdateScore("Score", 100, "SafeCard", 0, "Win", 0, "Lose", 1);
				}

				redCongrats.text = "LOSER";
				redSafecard.text = "Safe Card: 0";
				redWon.text = "Win: 0";
				redLose.text = "Lose: 1";

				PurpleCongrats.text = "LOSER";
				PurpleSafecard.text = "Safe Card: 0";
				PurpleWon.text = "Win: 0";
				PurpleLose.text = "Lose: 1";

				GreenCongrats.text = "LOSER";
				GreenSafecard.text = "Safe Card: 0";
				GreenWon.text = "Win: 0";
				GreenLose.text = "Lose: 1";

				BlueCongrats.text = "WINNER";
				BlueSafecard.text = "Safe Card: 1";
				BlueWon.text = "Won: 1";
				BlueLose.text = "Lose: 0";
				break;
		}

		
	}

	public void GotoHome()
	{
		SceneManager.LoadScene(0);

	}

	public void TestUpdateData()
	{
		UpdateScore("Score", 100, "SafeCard", 1, "Win", 1, "Lose", 1);

	}

	public int val;
	public void UpdateScore(string Score, int ScoreValue, string SafeCard, int SafeCardValue, string Win, int WinValue, string Lose, int LoseValue)
	{
		loadingPanel.instance.ShowLoader("Updating User Data...");
		int _Score = UI.instance.Getint("Score") + ScoreValue;
		UI.instance.SetInt("Score", _Score);

		int _safeCard = UI.instance.Getint("SafeCard") + SafeCardValue;
		UI.instance.SetInt("SafeCard", _safeCard);

		int _win = UI.instance.Getint("Win") + WinValue;
		UI.instance.SetInt("Win", _win);

		int _lose = UI.instance.Getint("Lose") + LoseValue;
		UI.instance.SetInt("Lose", _lose);


		SaveAppearence("Score", UI.instance.Getint("Score"),
					"SafeCard", UI.instance.Getint("SafeCard"),
					"Win", UI.instance.Getint("Win"),
					"Lose", UI.instance.Getint("Lose"));



	}
	public void GetTitleData()
	{
		PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnTitleDataRecieved, OnError);
	}
	int Score;
	int safecard;
	int win;
	int lose;
	void OnTitleDataRecieved(GetUserDataResult result)
	{


		Score = int.Parse(result.Data["Score"].Value);
		UI.instance.SetInt("Score", Score);
		UI.instance.Score = UI.instance.Getint("Score");


		safecard = int.Parse(result.Data["SafeCard"].Value);
		UI.instance.SetInt("SafeCard", safecard);
		UI.instance.SafeCard = UI.instance.Getint("SafeCard");

		win = int.Parse(result.Data["Win"].Value);
		UI.instance.SetInt("Win", win);
		UI.instance.win = UI.instance.Getint("Win");

		lose = int.Parse(result.Data["Lose"].Value);
		UI.instance.SetInt("Lose", lose);
		UI.instance.lose = UI.instance.Getint("Lose");

		PlayFabManager.instance.SendLeaderboard(UI.instance.Score);

		UI.instance.UpdateScore();
		loadingPanel.instance.HideLoader();

	}
	public void SaveAppearence(string Score, int ScoreValue, string SafeCard, int SafeCardValue, string Win, int WinValue, string Lose, int LoseValue)
	{

		int score = ScoreValue;
		int safeCard = SafeCardValue;
		int win = WinValue;
		int lose = LoseValue;


		var request = new UpdateUserDataRequest
		{
			Data = new Dictionary<string, string>
			{

				{ "Score", score.ToString() },
				{ "SafeCard", safeCard.ToString() },
				{ "Win", win.ToString() },
				{ "Lose", lose.ToString()}

				}
		};

		PlayFabClientAPI.UpdateUserData(request, OnDataSend, OnError);

	}
	void OnDataSend(UpdateUserDataResult result)
	{
		Debug.Log(" Successful User Data Send ! ");
		GetTitleData();
	}


	void OnError(PlayFabError Error)
	{
		//messageText.text = Error.ErrorMessage;
		Debug.Log(" Error while Logging in / Creating Account : ");
		Debug.Log(Error.GenerateErrorReport());
		Debug.Log(Error.ErrorMessage);

	}
	public GameObject exitPopUp;
	public void ExitPopUp()
	{
		exitPopUp.SetActive(true);
		exitPopUp.transform.GetChild(0).DOScale(1, 1);

	}
	public void ExitPopUpHide()
	{
		exitPopUp.transform.GetChild(0).DOScale(0, 1);
		exitPopUp.SetActive(false);

	}
	public void ExitfromGame()
	{
		PhotonNetwork.LeaveRoom(true);
		RommsCanvas.instance.RoomType = "";
		if (UI.instance.gameObject.activeInHierarchy)
		{
			Destroy(UI.instance.gameObject);
			UI.instance = null;
		}
		if (PlayFabManager.instance.gameObject.activeInHierarchy)
		{
			Destroy(PlayFabManager.instance.gameObject);
			PlayFabManager.instance = null;
		}
		SceneManager.LoadScene(0);

	}

	public void exitbuysceen()
	{
		popup.SetActive(false);
		popupText.text = "Pawn successfully out from Jail...";
		popup.gameObject.SetActive(false);
		JailScreen.SetActive(false);
	}

	public void openRewardPanel(int ScoreValue,string rewardTxt)
    {
		rewardScreen.SetActive(true);
		RewardPanel.instance.GetScorePointsAndText(rewardTxt, ScoreValue);
		RewardPanel.instance.ShowAndHideScreen(true);
	}

	public void OpenInAppScreen()
	{

		StartCoroutine(OpenInAPP());

	}

	IEnumerator OpenInAPP()
    {
		if (InAppObj.activeInHierarchy)
		{

			InAppObj.transform.DOScale(0, 0.5f);
			yield return new WaitForSeconds(0.5f);
			InAppObj.gameObject.SetActive(false);
		}
		else
		{
			InAppObj.gameObject.SetActive(true);
			InAppObj.transform.DOScale(1, 0.5f);
		}
	}
}
