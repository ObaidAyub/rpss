using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class User
{
    public string name;
    public int value;
}
public class PlayerScript : MonoBehaviour
{ // Pawns For Two Players
    public Transform greenPawn;
    public Transform redPawn;
    public List<User> userInfo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void redPlayerMovement()
    {
        Vector3 direction = new Vector3(0f, 0f, 10f);
        redPawn.transform.Translate(direction * 10f * Time.deltaTime);
    }

    public void greenPlayerMovement()
    {
        Vector3 direction = new Vector3(0f, 0f, -12f);
        greenPawn.transform.Translate(direction * 10f * Time.deltaTime);
    }

}
