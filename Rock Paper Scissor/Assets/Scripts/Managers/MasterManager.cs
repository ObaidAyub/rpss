using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


[CreateAssetMenu(menuName ="Singletons/MasterManager")]
public class MasterManager : SingletonScriptableObject<MasterManager>
{

    [SerializeField]
    private GameSettings _gamesettings;
    public static GameSettings GameSettings { get { return Instance._gamesettings; } }

    [SerializeField]
    private List<NetworkedPrefab> _NetworkPrefabs = new List<NetworkedPrefab>();

    public static GameObject NetworkInstantiate(GameObject obj , Vector3 position , Quaternion rotation)
    {
            
        foreach (NetworkedPrefab networkPrefab in Instance._NetworkPrefabs)
        {
            if (networkPrefab.Prefab == obj)
            {
                if (networkPrefab.Path != string.Empty)
                {
                    GameObject result = PhotonNetwork.Instantiate(networkPrefab.Path, position, rotation);
                    return result;
                }
                else
                {

                    Debug.Log(" Path is Empty for GameObject name :" +networkPrefab.Prefab );
                    return null;
                
                }
            }
        
        }

        return null;

    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void PopulateNetworkedPrefabs()
    {

        //if (!Application.isEditor)
        //{
        //    return;
        //}
#if UNITY_EDITOR
 

        GameObject[] results = Resources.LoadAll<GameObject>("");
        for (int i = 0; i < results.Length; i++)
        {
            if (results[i].GetComponent<PhotonView>() != null)
            {
                string path = AssetDatabase.GetAssetPath(results[i]);
                //Instance._NetworkPrefabs.Add(new NetworkedPrefab(results[i], path));
             
            
            }
     

        }


        //for (int i = 0; i< Instance._NetworkPrefabs.Count; i++)
        //    {

        //    Debug.Log(Instance._NetworkPrefabs[i].Prefab.name + "," + Instance._NetworkPrefabs[i].Path);


        //}

#endif
    }

}
