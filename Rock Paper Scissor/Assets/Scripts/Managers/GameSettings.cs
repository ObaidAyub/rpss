using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Manager/GameSetting")]
public class GameSettings : ScriptableObject
{

    [SerializeField]
    private string _gameversion = "0.0.0";

    public string GameVersion { get { return _gameversion; } }

    [SerializeField]
    private string _nickName = "User";

public string NickName
    {

        get
        {

            int value = Random.Range(0, 9999);
            //return _nickName + value.ToString();
       
           
           
            return "User" + value.ToString();
         //return UI.ui.userNameText.text + value.ToString();
        
        }
    }

}
