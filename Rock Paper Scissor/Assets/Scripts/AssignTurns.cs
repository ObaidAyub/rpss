using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AssignTurns : MonoBehaviourPunCallbacks
{
    public static AssignTurns instance;
    //References
    public DiceScript diceScript;
    public DiceNumberTextScript diceNumberScript;

    public static int turnNumber;
    public Text Board2TurnText;
    public Text Board4TurnText;

    // 2 Players  
    public Button red6Btn;
    public Button green6Btn;

    // 4 Players
    public Button red4Button;
    public Button purple4Button;
    public Button green4Button;
    public Button blue4Button;

    //Boards
    public GameObject board2;
    public GameObject board4;

    [Header("Pawn")]//Added abrar
    public Transform redPawn;
    public Transform bluePawn;
    public Transform purplePawn;
    public Transform greenPawn;

    public static bool firstTime = true;

    public int turnCount;
    private PhotonView PV;

    public bool myTurn;

    private void Awake()
    {
        instance = this;
        PV = GetComponent<PhotonView>();
    }
    void Start()
    {
    }

    public void Onstart()
    {
        red6Btn.interactable = false;
        green6Btn.interactable = false;
        if (board2.activeInHierarchy == true)
        {
            Board4TurnText.gameObject.SetActive(false);
            Board2TurnText.gameObject.SetActive(true);
        }
        if (board4.activeInHierarchy == true)
        {
            Board2TurnText.gameObject.SetActive(false);
            Board4TurnText.gameObject.SetActive(true);
        }
        turnCount = 1;
        PV.RPC("TurnCheck", RpcTarget.AllBuffered, turnCount);
    }
    [PunRPC]
    public void TurnCheck(int turnNum)
    {
        turnNumber = turnNum;
        if (GameManager.modeType == GameManager.Mode.TwoPlayers)
        {
            if (turnNumber == 1)
            {
                TurnSelection("Red Turn", "Red");
                turnCount += 1;
            }
            else if (turnNumber == 2)
            {
                TurnSelection("Green Turn", "Green");
                if (turnCount == 2)
                {
                    turnCount = 1;
                }
                else
                {
                    turnCount += 1;
                }
            }
        }
        else if (GameManager.modeType == GameManager.Mode.FourPlayers)
        {
            if (turnNumber == 1)
            {
                TurnSelection("Red Turn", "Red");
                turnCount += 1;
            }
            else if (turnNumber == 2)
            {
                TurnSelection("Purple Turn", "Purple");
                turnCount += 1;
            }
            else if (turnNumber == 3)
            {
                TurnSelection("Green Turn", "Green");
                turnCount += 1;
            }
            else if (turnNumber == 4)
            {
                TurnSelection("Blue Turn", "Blue");
                if (turnCount == 4)
                {
                    turnCount = 1;
                }
                else
                {
                    turnCount += 1;
                }
            }
        }
    }

    private void TurnSelection(string turn, string Team)
    {
        firstTime = false;
        Board4TurnText.text = turn;
        GameManager.instance.activePlayer = redPawn;
        if ((string)PhotonNetwork.LocalPlayer.CustomProperties["Team"] == Team)
        {
            Board4TurnText.text = "Your Turn";
            red4Button.gameObject.SetActive(true);
            myTurn = true;
        }
        diceNumberScript.diceNumbertext.gameObject.SetActive(true);
        
    }
}