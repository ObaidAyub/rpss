using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using Photon.Pun;
using System;
using UnityEngine.Networking;
using DG.Tweening;

public enum TeamColor
{
    Black = 0, White = 1
}
//private class 
public class UI : MonoBehaviour
{

    public static UI instance;

    public string userNameText;

    public string teamColor;

    public string RoomName;

    public bool updateScoreOnce;

    [Header("Team Buttons")]
    [SerializeField] private Button whiteTeamButtonButton;
    [SerializeField] private Button blackTeamButtonButton;



    private void Awake()
    {
        instance = this;
        userid = SystemInfo.deviceUniqueIdentifier;
        fbId = GetString("fbId");
        facebookId = GetString("FacebookId");
        appleId = GetString("AppleId");
        appleToken = GetString("AppleToken");
        userName = GetString("UserName");
        displayName = GetString("DisplayName");
        email = GetString("Email");
        password = GetString("Password");
        _date = Getint("_date");
        _timer = GetString("_timer");
        DOB = GetString("DOB");
        DOB_day = GetString("DOB_day");
        DOB_month = GetString("DOB_month");
        DOB_year = GetString("DOB_year");
        
    }
    private class BoardSquareSetup//Added khudse
    {
        public Vector2Int position;
        public TeamColor teamColor;
    }

    [SerializeField] private BoardSquareSetup[] boardSquares;

    [SerializeField] private PlayerListingMenu networkManager;


    public float totalPrice = 500, firstPosPrice = 26, secondPosPrice = 16, thirdPosPrice = 10, forthPosPrice = 6.8f;
    void Start()
    {
        Debug.Log(Application.identifier);
        DontDestroyOnLoad(this);


        var First = PriceValue(totalPrice, firstPosPrice);

        Debug.Log("First Place : " + First);

        var Second = PriceValue(totalPrice, secondPosPrice);

        Debug.Log("Second Place : " + Second);

        var third = PriceValue(totalPrice, thirdPosPrice);

        Debug.Log("third Place : " + third);

        var forth = PriceValue(totalPrice, forthPosPrice);

        Debug.Log("Forth Place : " + forth);


    }

    public float PriceValue(float total, float price)
    {
        var amount = (total / 100) * price;

        return amount;
    }

    public string MyPlayfabID;
    public void GetAccountInfo()
    {
        loadingPanel.instance.ShowLoader("Get Profile Data...");
        GetAccountInfoRequest request = new GetAccountInfoRequest();
        PlayFabClientAPI.GetAccountInfo(request, Successs, fail);

    }


    void Successs(GetAccountInfoResult result)
    {

        MyPlayfabID = result.AccountInfo.PlayFabId;
        Debug.Log(result.AccountInfo.Username);

        UI.instance.SetString("fbId", result.AccountInfo.PlayFabId);

        GetPlayerProfile(MyPlayfabID);//Uncomment


    }

    public string titleId;
    void fail(PlayFabError error)
    {

        Debug.LogError(error.GenerateErrorReport());
    }
    void GetPlayerProfile(string playFabId)
    {

        userNameText = "";
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest()
        {
            PlayFabId = playFabId,
            ProfileConstraints = new PlayerProfileViewConstraints()
            {
                ShowDisplayName = true
                
            }
            
        },



        result => userNameText = result.PlayerProfile.DisplayName,
        error => Debug.LogError(error.GenerateErrorReport()));;
        Scene scene = SceneManager.GetActiveScene();

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            UI.instance.fbId = UI.instance.GetString("fbId");
        }


        //// yahan karni ha kuch kalakari


        StartCoroutine(getusername());

        
    }

     IEnumerator getusername()
    {
        yield return new WaitForSeconds(2);
        Debug.Log(userNameText);
        if (!string.IsNullOrEmpty(userNameText))
        {

            Authentication.Instance.userNameText.text = userNameText;
            SettingManager.instance.settigs_username.text = userNameText;
            PhotonNetwork.NickName = userNameText;
        }
        else
        {
            userNameText = UI.instance.userName;
            Authentication.Instance.userNameText.text = UI.instance.userName;
            SettingManager.instance.settigs_username.text = UI.instance.displayName;
            PhotonNetwork.NickName = UI.instance.userName;
        }
        loadingPanel.instance.HideLoader();
    }

    public void gameMode()
    {
        SceneManager.LoadScene(1);

    }
    public void gameMode1()
    {
        SceneManager.LoadScene(1);

    }

    string updateScoreText;
    string updateSafeCards;
    public void UpdateScore()
    {
        updateSafeCards = Getint("SafeCard").ToString();
        updateScoreText = Getint("Score").ToString();
        win = Getint("Win");
        lose = Getint("Lose");


        Scene scene = SceneManager.GetActiveScene();
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {

            Authentication.Instance.updateScoreText.text = updateScoreText;
            Authentication.Instance.updateSafeCardText.text = updateSafeCards;

            DailyReward.instance.OnstartCheck();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            GameManager.instance.points.text = updateScoreText;
            GameManager.instance.SafeCards.text = updateSafeCards;
        }
    }
    internal void RestrictTeamChoice(TeamColor occpiedTeam)//Added khudse
    {
        Button buttonToDeactivate = occpiedTeam == TeamColor.White ? whiteTeamButtonButton : blackTeamButtonButton;
        buttonToDeactivate.interactable = false;
    }
    public TeamColor GetSquareTeamColorAtIndex(int index)//Added khudse
    {
        return boardSquares[index].teamColor;
    }
    public void SelectTeam(int team)
    {
        networkManager.SetPlayerTeam(team);
    }




    public string userid, fbId, facebookId, appleId, appleToken, displayName, userName, email, password, _timer, DOB, DOB_day, DOB_month, DOB_year;

    public int _date, Score, SafeCard,win,lose, hours, minutes, seconds;


    public void SetInt(string KeyName, int Value)
    {
        PlayerPrefs.SetInt(KeyName, Value);
    }

    public int Getint(string KeyName)
    {
        return PlayerPrefs.GetInt(KeyName);
    }

    public void SetString(string KeyName, string Value)
    {
        PlayerPrefs.SetString(KeyName, Value);
    }

    public string GetString(string KeyName)
    {
        return PlayerPrefs.GetString(KeyName);
    }

    public class timer
    {
        public string time { get; set; }
    }

    public GameObject reportIssue;
    public InputField reportText;
    public Button reportSubmitButton, exitReportScrBtn;

    public void OpenReortIssueScreen()
    {
        reportIssue.transform.DOScale(1f, 0.5f);
        exitReportScrBtn.onClick.RemoveAllListeners();
        exitReportScrBtn.onClick.AddListener(CloseReortIssueScreen);
        
    }
    public void CloseReortIssueScreen()
    {
        reportIssue.transform.DOScale(0f, 0.5f);
        
        
    }

    public void sendReport()
    {
        if (string.IsNullOrEmpty(reportText.text.ToString()))
        {
            Debug.Log("string is empty");
            Authentication.Instance.errorPopup.SetActive(true);
            
            Authentication.Instance.errorPopUpHeading.text = "Report Issue ";
            Authentication.Instance.errorText.text = "Report is empty";
            
        }
        else
        {
            Debug.Log("string is not empty");
            StartCoroutine(Upload());
        }

    }

    IEnumerator Upload()
    {
        

        reportSubmitButton.interactable = false;

        WWWForm form = new WWWForm();
        form.AddField("name", userNameText);
        form.AddField("message", reportText.text);
        form.AddField("username", userNameText);
        form.AddField("user_id", fbId);

        using (UnityWebRequest www = UnityWebRequest.Post("https://rockpaperscissorsscoot.com/admin/api/v1/report_issues", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
                Authentication.Instance.errorPopup.SetActive(true);
                Authentication.Instance.errorText.text = www.error;
                reportSubmitButton.interactable = true;
                Authentication.Instance.errorPopUpHeading.text = "Report Issue ";
            }
            else
            {
                Debug.Log("Form upload complete!");
                reportSubmitButton.interactable = true;
                CloseReortIssueScreen();
                Authentication.Instance.errorPopUpHeading.text = "Report Issue ";
                Authentication.Instance.errorPopup.SetActive(true);
                Authentication.Instance.errorText.text = "Report Submitted Successfully...";
                reportText.text = "";
            }
        }
    }

    
}




