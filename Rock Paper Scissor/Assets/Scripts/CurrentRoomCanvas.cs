 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentRoomCanvas : MonoBehaviour
{
    [SerializeField]
    private PlayerListingMenu _playerListingMenu;
    [SerializeField]
    private LeaveRoomMenu _leaveRoomMenu;

    private RommsCanvas _roomCanvases;

    public LeaveRoomMenu LeaveRoomMenu { get { return _leaveRoomMenu; } }

    public void FirstInitialize(RommsCanvas canvases)
    {
        _roomCanvases = canvases;
        _playerListingMenu.FirstInitialize(canvases);
        _leaveRoomMenu.FirstInitialize(canvases);
    }

    public void show()
    {

        gameObject.SetActive(true);
    }


    public void hide()
    {
        gameObject.SetActive(false);

        }
}
