using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SimpleObjectMover : MonoBehaviourPun , IPunObservable  
{
    [SerializeField]
    private float moveSpeed = 0.5f;

    private Animator _animator;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

        //if (stream.IsWriting) //owner
        //{
        //    stream.SendNext(transform.position);
        //    //stream.SendNext(transform.rotation);
        //}
        //else if(stream.IsReading)
        // {

        //    transform.position = (Vector3)stream.ReceiveNext();

        //    //transform.rotation = (Quaternion)stream.ReceiveNext();
        //}


    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (base.photonView.IsMine)
        {
            float x = Input.GetAxisRaw("Horizontal");

            float y = Input.GetAxisRaw("Vertical");

            transform.position += new Vector3(x, y, 0f) * moveSpeed*Time.deltaTime;

            UpdateMovingBoolean((x != 0f || y != 0f));
        }
        
    }

    private void UpdateMovingBoolean(bool moving)
    {

        _animator.SetBool("Moving" , moving);
    }
}
