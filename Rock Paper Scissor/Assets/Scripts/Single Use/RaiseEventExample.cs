using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class RaiseEventExample : MonoBehaviourPun
{

    private SpriteRenderer _spriteRenderer;
    private const byte COLOR_CHANGE_EVENT = 0;


    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if (base.photonView.IsMine && Input.GetKeyDown(KeyCode.Space))
        {
            changeColor();
        }

    }


    private void OnEnable() // Events will fire when it is Enable //learn
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventRecieved;
    }


    private void OnDisable() // Events will fire when it is disable  //learn
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventRecieved;
    }


    private void NetworkingClient_EventRecieved(EventData obj)  //learn
    {

        if(obj.Code == COLOR_CHANGE_EVENT)
        {

            object[] datas = (object[])obj.CustomData;
            float r = (float)datas[0];
            float g = (float)datas[1];
            float b = (float)datas[2];

            _spriteRenderer.color = new Color(r, g, b, 1f);

        }

    }

    private void changeColor()
    {
        float r = Random.Range(0f, 1f);

        float g = Random.Range(0f, 1f);

        float b = Random.Range(0f, 1f);

        _spriteRenderer.color = new Color(r, g, b, 1f);

        object[] datas = new object[] {  r, g ,b };

        PhotonNetwork.RaiseEvent(COLOR_CHANGE_EVENT, datas, RaiseEventOptions.Default, SendOptions.SendUnreliable);


    }
}
