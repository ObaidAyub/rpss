using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Photon.Pun;

public class ChatManager : MonoBehaviour
{
    public InputField chatInput;
    public TextMeshProUGUI chatContent;
    private PhotonView _photon;
    private List<string> _messages = new List<string>();
    //private float _buildDelay = 0f;
    //private int _maximumMessages = 14;

    public Transform chatcontent;
    public ChatPrefabManager chatPrefab;

    public GameObject chatindicator;

    // Start is called before the first frame update
    void Start()
    {
        _photon = GetComponent<PhotonView>();
    }
    [PunRPC]
    void RPC_AddNewMessage(string msg,string teamColor)
    {
       // _messages.Add(msg);

        ChatPrefabManager chatObj = Instantiate(chatPrefab, chatcontent);
        chatObj.chatTxt.text = msg;

        if (GameManager.instance.TeamColor != teamColor)
        {
            Debug.Log(GameManager.instance.TeamColor + " " + UI.instance.teamColor);

            Debug.Log("not equal");
            chatindicator.SetActive(true);
        }
    }

    public void SendChat(string msg)
    {

        //string NewMessage = PhotonNetwork.NickName + ":   " + msg;
        string NewMessage = FindObjectOfType<UI>().userNameText + ":   " + msg;
        _photon.RPC("RPC_AddNewMessage", RpcTarget.All, NewMessage, GameManager.instance.TeamColor);
      


    }

    public void SubmitChat()
    {
        string blankCheck = chatInput.text;
        blankCheck = System.Text.RegularExpressions.Regex.Replace(blankCheck, @"\s", "");
        if (blankCheck == "")
        {
            //chatInput.ActivateInputField();
            chatInput.text = "";
            return;
        
        }

        SendChat(chatInput.text);
       // chatInput.ActivateInputField();
        chatInput.text = "";
    
    }

    void BuildChatContents()
    {

        string NewContents = "";
        foreach (string s in _messages)
        {
            NewContents += s + "\n";
        }
        chatContent.text = NewContents;
    
    }


    // Update is called once per frame
   /* void Update()
    {
        if (PhotonNetwork.InRoom)
        {
            chatContent.maxVisibleLines = _maximumMessages;
            if (_messages.Count > _maximumMessages)
            {

                _messages.RemoveAt(0);
            
            }

            if (_buildDelay < Time.time)
            {
                BuildChatContents();
                _buildDelay = Time.time + 0.25f;
            }

        }

        else if(_messages.Count>0 )
        {

            _messages.Clear();
            chatContent.text = "";
        }

        
    }*/
}
