﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyReward : MonoBehaviour
{
	//UI
	public static DailyReward instance;
	public Text timeLabel;
	public Button timerButton; 
	public Image _progress;
	//TIME ELEMENTS
	public int hours; 
	public int minutes; 
	public int seconds; 
	private bool _timerComplete = false;
	private bool _timerIsReady;
	private TimeSpan _startTime;
	private TimeSpan _endTime;
	private TimeSpan _remainingTime;
	//progress filler
	private float _value = 1f;
	public int RewardToEarn;
	void Start()
	{
		instance = this;

		//Debug.Log("Here" + System.DateTime.Now.Date.ToString());
		//Debug.Log("Here" + System.DateTime.Now.TimeOfDay.Milliseconds.ToString());
		string TimeOfDay = (System.DateTime.Now.TimeOfDay.ToString());
		//Debug.Log("TimeOfDay " + TimeOfDay);
		string s = TimeOfDay.Replace(":", "");
		//Debug.Log("TimeOfDay " + s);
		string sa = s.Replace(".", "");
		//Debug.Log("TimeOfDay " + sa);

		//TimeSpan temp = TimeSpan.Parse(TimeManager.sharedInstance.getCurrentTimeNow());
		////Debug.Log("Here " + temp);

	}
	public void OnstartCheck()
    {
		if (UI.instance.GetString("_timer") == "")
		{
			enableButton();
		}
		else
		{
			disableButton();
			StartCoroutine("CheckTime");
		}
	}
	private void updateTime()
	{
		if (UI.instance.GetString("_timer") == "Standby")
		{
			
			UI.instance.SetInt("_date", System.DateTime.Now.Day);
			UI.instance._date = UI.instance.Getint("_date");

			UI.instance.SetInt("hours", DateTime.Now.TimeOfDay.Hours);
			UI.instance.hours = UI.instance.Getint("hours");
			hours = UI.instance.hours;

			UI.instance.SetInt("minutes", DateTime.Now.TimeOfDay.Minutes);
			UI.instance.minutes = UI.instance.Getint("minutes");
			minutes = UI.instance.minutes;

			UI.instance.SetInt("seconds", DateTime.Now.TimeOfDay.Seconds);
			UI.instance.seconds = UI.instance.Getint("seconds");
			seconds = UI.instance.seconds;

			UI.instance.SetString("_timer", UI.instance.Getint("hours") + "." + UI.instance.Getint("minutes") + "." + UI.instance.Getint("seconds"));
			UI.instance._timer = UI.instance.GetString("_timer");

			Authentication.Instance.SaveAppearence("Score", UI.instance.Getint("Score"),
			"SafeCard", UI.instance.Getint("SafeCard"),
			"Win", UI.instance.Getint("Win"),
			"Lose", UI.instance.Getint("Lose"),
			"_date", UI.instance.Getint("_date"),
			"Hours", UI.instance.Getint("hours"),
			"Minutes", UI.instance.Getint("minutes"),
			"Seconds", UI.instance.Getint("seconds"));

		}
		else if (UI.instance.GetString("_timer") != "" && UI.instance.GetString("_timer") != "Standby")
		{
			int _old = UI.instance.Getint("_date");
			int _now = System.DateTime.Now.Day;
			if (_now > _old)
			{
				enableButton();
				return;
			}
			else if (_now == _old)
			{
				return;
			}
			else
			{
				return;
			}
		}
	}
	private void _configTimerSettings()
	{
		_startTime = TimeSpan.Parse(UI.instance.GetString("_timer"));
		_endTime = TimeSpan.Parse(hours + ":" + minutes + ":" + seconds);
		TimeSpan temp = TimeSpan.Parse(TimeManager.sharedInstance.getCurrentTimeNow());
		TimeSpan diff = temp.Subtract(_startTime);
		_remainingTime = _endTime.Subtract(diff);
		setProgressWhereWeLeftOff();
		if (diff >= _endTime)
		{
			_timerComplete = true;
			enableButton();
		}
		else
		{
			_timerComplete = false;
			disableButton();
			_timerIsReady = true;
		}
	}
	private void setProgressWhereWeLeftOff()
	{
		float ah = 1f / (float)_endTime.TotalSeconds;
		float bh = 1f / (float)_remainingTime.TotalSeconds;
		_value = ah / bh;
	}
	private void enableButton()
	{
		GameAnimations.instance.DailyRewardAnimation(true,gameObject);
		timerButton.interactable = true;
		timeLabel.text = "CLAIM REWARD";
	}
	private void disableButton()
	{
		timerButton.interactable = false;
		timeLabel.text = "NOT READY";
	}
	private IEnumerator CheckTime()
	{
		disableButton();
		timeLabel.text = "Checking the time";
		yield return new WaitForSeconds(1);
		updateTime();
	}
	public void rewardClicked()
	{
		claimReward(RewardToEarn);
		UI.instance.SetString("_timer", "Standby");
		StartCoroutine("CheckTime");
	}
	void Update()
	{
		if (_timerIsReady)
		{
			if (!_timerComplete && UI.instance.GetString("_timer") != "")
			{
				_value -= Time.deltaTime * 1f / (float)_endTime.TotalSeconds;

				if (_value <= 0 && !_timerComplete)
				{
					_timerComplete = true;
				}
			}
		}
	}
	private void validateTime()
	{
		StartCoroutine("CheckTime");
	}


	private void claimReward(int x)
	{
		////Debug.Log("YOU EARN " + x + " REWARDS");
	}

}