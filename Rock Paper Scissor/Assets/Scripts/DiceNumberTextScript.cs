﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceNumberTextScript : MonoBehaviourPunCallbacks
{
    public static DiceNumberTextScript instance;
    public Text diceNumbertext;
    public int diceNumber;
    private PhotonView PV;

    // Use this for initialization
    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    void Start()
    {
        instance = this;
        if (PV.IsMine)
        {
            PV.RPC("GetText", RpcTarget.AllBuffered);
        }
    }
    [PunRPC]
    public void GetText()
    {
        diceNumbertext = GetComponent<Text>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //diceNumbertext.text = diceNumber.ToString();//set krna hy
        //if (PV.IsMine)
        //{
        //    PV.RPC("GetDiceNumber", RpcTarget.AllBuffered);
        //}

    }
    [PunRPC]
    public void GetDiceNumber()
    {
        diceNumbertext.text = diceNumber.ToString();//set krna hy
    }
}
