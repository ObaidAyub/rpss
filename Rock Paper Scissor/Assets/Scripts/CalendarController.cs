﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalendarController : MonoBehaviour
{
    public GameObject _calendarPanel;
    public Text _yearNumText;
    public Text _monthNumText;

    public GameObject _item;

    public List<GameObject> _dateItems = new List<GameObject>();
    const int _totalDateNum = 42;

    private DateTime _dateTime;
    public static CalendarController _calendarInstance;

    void Start()
    {
        _calendarInstance = this;
        Vector3 startPos = _item.transform.localPosition;
        _dateItems.Clear();
        _dateItems.Add(_item);

        for (int i = 1; i < _totalDateNum; i++)
        {
            GameObject item = GameObject.Instantiate(_item) as GameObject;
            item.name = "Item" + (i + 1).ToString();
            item.transform.SetParent(_item.transform.parent);
            item.transform.localScale = Vector3.one;
            item.transform.localRotation = Quaternion.identity;
            item.transform.localPosition = new Vector3((i % 7) * 31 + startPos.x, startPos.y - (i / 7) * 25, startPos.z);

            _dateItems.Add(item);
        }

        _dateTime = DateTime.Now;

        CreateCalendar();

        _calendarPanel.SetActive(false);
    }

    void CreateCalendar()
    {
        DateTime firstDay = _dateTime.AddDays(-(_dateTime.Day - 1));
        int index = GetDays(firstDay.DayOfWeek);

        int date = 0;
        for (int i = 0; i < _totalDateNum; i++)
        {
            Text label = _dateItems[i].GetComponentInChildren<Text>();
            _dateItems[i].SetActive(false);

            if (i >= index)
            {
                DateTime thatDay = firstDay.AddDays(date);
                if (thatDay.Month == firstDay.Month)
                {
                    _dateItems[i].SetActive(true);

                    label.text = (date + 1).ToString();
                    date++;
                }
            }
        }
        _yearNumText.text = _dateTime.Year.ToString();
        _monthNumText.text = _dateTime.Month.ToString();
        switch (_dateTime.Month)
        {
            case 1: _monthNumText.text = "Jan"; break;
            case 2: _monthNumText.text = "Feb"; break;
            case 3: _monthNumText.text = "Mar"; break;
            case 4: _monthNumText.text = "April"; break;
            case 5: _monthNumText.text = "May"; break;
            case 6: _monthNumText.text = "June"; break;
            case 7: _monthNumText.text = "July"; break;
            case 8: _monthNumText.text = "Aug"; break;
            case 9: _monthNumText.text = "Sep"; break;
            case 10: _monthNumText.text = "Oct"; break;
            case 11: _monthNumText.text = "Nov"; break;
            case 12: _monthNumText.text = "Dec"; break;
        }
    }

    int GetDays(DayOfWeek day)
    {
        switch (day)
        {
            case DayOfWeek.Monday: return 1;
            case DayOfWeek.Tuesday: return 2;
            case DayOfWeek.Wednesday: return 3;
            case DayOfWeek.Thursday: return 4;
            case DayOfWeek.Friday: return 5;
            case DayOfWeek.Saturday: return 6;
            case DayOfWeek.Sunday: return 0;
        }

        return 0;
    }
    public void YearPrev()
    {
        _dateTime = _dateTime.AddYears(-1);
        CreateCalendar();
    }

    public void YearNext()
    {
        _dateTime = _dateTime.AddYears(1);
        CreateCalendar();
    }

    public void MonthPrev()
    {
        _dateTime = _dateTime.AddMonths(-1);
        CreateCalendar();
    }

    public void MonthNext()
    {
        _dateTime = _dateTime.AddMonths(1);
        CreateCalendar();
    }

    public void ShowCalendar()
    {
        _calendarPanel.SetActive(true);
        //_target = target;
        //_calendarPanel.transform.position = new Vector3(965, 475, 0);//Input.mousePosition-new Vector3(0,120,0);
    }

    public Text _target;
    public void OnDateItemClick(string day)
    {
        _target.text = _monthNumText.text + " : " + day + " : " + _yearNumText.text;
        _calendarPanel.SetActive(false);
        UI.instance.SetString("DOB", _target.text);
        UI.instance.DOB = UI.instance.GetString("DOB");

        UI.instance.SetString("DOB_day", day);
        UI.instance.SetString("DOB_month", _monthNumText.text);
        UI.instance.SetString("DOB_year", _yearNumText.text);

        UI.instance.DOB_day = UI.instance.GetString("DOB_day");
        UI.instance.DOB_month = UI.instance.GetString("DOB_month");
        UI.instance.DOB_year = UI.instance.GetString("DOB_year");

    }
}
