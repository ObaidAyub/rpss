using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserProvisionScr : MonoBehaviour
{
    public InputField username, useremail;

    public Text DOB;

    public Button saveBtn;

    private void OnEnable()
    {
        saveBtn.onClick.RemoveAllListeners();
        saveBtn.onClick.AddListener(userdataUpdated);
        GetUserDetailValues();
    }



    void GetUserDetailValues()
    {
        username.text = UI.instance.userNameText;
        useremail.text = UI.instance.email;


        if (string.IsNullOrEmpty(username.text))
        {
            useremail.text = "USER NAME";
        }
        else
        {

            username.text = UI.instance.userNameText;
        }

        if (string.IsNullOrEmpty(UI.instance.email))
        {
            useremail.text = "EMAIL ADDRESS";
        }
        else
        {

            useremail.text = UI.instance.email;
        }

        if (string.IsNullOrEmpty(UI.instance.DOB))
        {
            DOB.text = "DATE OF BIRTH";
        }
        else
        {

            DOB.text = UI.instance.DOB;
        }
    }

    public void updateUserNameServer()
    {
        
        UpdateUserName(username.text);
    }
    public void updateUserEmailOnServer()
    {
        updateEmailAddress(useremail.text);
    }
    public void UpdateDOB()
    {
        Debug.Log("Date of birth");

        //Authentication.Instance.SaveAppearence(
        //    "Score", UI.instance.Getint("Score"),
        //    "SafeCard", UI.instance.Getint("SafeCard"),
        //    "Win", UI.instance.Getint("Win"),
        //    "Lose", UI.instance.Getint("Lose"),
        //    "_date", UI.instance.Getint("_date"),
        //    "Hours", UI.instance.Getint("hours"),
        //    "Minutes", UI.instance.Getint("minutes"),
        //    "Seconds", UI.instance.Getint("seconds"));
        //string _day = UI.instance.DOB_day;
        //string _month = UI.instance.DOB_month;
        //string _year = UI.instance.DOB_year;
        //var request = new UpdateUserDataRequest
        //{
            
        //    Data = new Dictionary<string, string>
        //    {

                
        //        { "Day",    _day},
        //        { "Month",  _month},
        //        { "Year",   _year}

        //    }
        //};
        //PlayFabClientAPI.UpdateUserData(request, OnDataSend, Rerror);


        Authentication.Instance.errorPopUpHeading.text = "User Profile";
        Authentication.Instance.errorPopup.SetActive(true);
        Authentication.Instance.errorText.text = "User Profile Successfully Updated...";
    }
    void OnDataSend(UpdateUserDataResult result)
    {
        Debug.Log(" Successful User Data Send ! ");
    }
    void Rerror(PlayFabError fabError)
    {
        Debug.Log(fabError.ErrorMessage);
    }
    void ResetUserNamefb(string name)
    {

        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = name,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUserNameReset, OnError);
    }

    void OnUserNameReset(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("user update");
        UI.instance.SetString("UserName", useremail.text);
        UI.instance.email = UI.instance.GetString("UserName");
        //messageText.text = " User name updated ";
        //resetUserNameScreen.SetActive(false);
        //FindObjectOfType<UI>().GetAccountInfo();
    }


    void updateEmailAddress(string email)
    {

        var request = new AddOrUpdateContactEmailRequest
        {
            EmailAddress = email,
        };
        PlayFabClientAPI.AddOrUpdateContactEmail(request, upadateEmailAddress, OnError);

    }
    void upadateEmailAddress(AddOrUpdateContactEmailResult result)
    {
        Debug.Log("email update" );
        UI.instance.SetString("Email", useremail.text);
        UI.instance.email = UI.instance.GetString("Email");
        UpdateDOB();

        


    }
  

    void UpdateUserName(string Name)
    {
        Debug.Log(Name);
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = Name,
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUserName, OnError);
    }
    void OnUserName(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("username Updated ");
        //messageText.text = " User name updated ";
        //resetUserNameScreen.SetActive(false);
        FindObjectOfType<UI>().GetAccountInfo();
        updateUserEmailOnServer();
       
    }
    public void userdataUpdated()
    {
        updateUserNameServer();
        
    }
    public void OnError(PlayFabError Error)
    {
        Authentication.Instance.errorPopup.SetActive(true);
        Authentication.Instance.errorText.text = Error.ErrorMessage;
        Authentication.Instance.errorPopUpHeading.text = "Error";

        if (Error.ErrorMessage == "Email address not available")
        {

        }

        if (Error.ErrorMessage == "Invalid input parameters")
        {
            Authentication.Instance.errorText.text = "Field Data is not valid";

        }

        if (Error.ErrorMessage == "User not found")
        {


        }
        
        loadingPanel.instance.HideLoader();
    }

    //public class DOB
    //{
    //    public string Day;
    //    public string month;
    //    public string yearv;
    //}

}
