using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Facebook.Unity;
using System;
using UnityEngine.Networking;
using System.IO;

public class FriendListPrefab : MonoBehaviour
{
    public TextMeshProUGUI playerName;
    public Image profilePic;
    public Button requestBtn, chatBtn, VideoChatBtn;
    public string userId;

    private void Start()
    {
        requestBtn.onClick.RemoveAllListeners();
        requestBtn.onClick.AddListener(SendRequestOnMessenger);
    }

    public void SendRequestOnMessenger()
    {
        FB.AppRequest(
  message: "I Just got points! Can you beat it?",
  to: GetUserID(),
  data: "{\"challenge_score\":" + 0.ToString() + "}",
  title: "Friend Smash Challenge!",
  callback: appRequestCallback);
    }

    private void appRequestCallback(IAppRequestResult result)
    {
        Debug.Log(result.RawResult);
    }

    IEnumerable<string> GetUserID()
    {
        yield return userId;
    }
    public void GetUserPicture(string ImagePath)
    {
        string path = Application.persistentDataPath;
        string ImgLink = ImagePath;
        string DIRPath = Path.Combine(path, "ProfileImg");
        string FilePath = Path.Combine(DIRPath + ".jpeg");
        StartCoroutine(setimage(ImgLink, DIRPath, FilePath));
    }

    private Sprite profImg;
    public Texture2D texture;

    IEnumerator setimage(string ImgLink, string DIRPath, string FilePath)
    {
        if (string.IsNullOrEmpty(ImgLink))
        {
            yield break;
        }

        Debug.Log(FilePath);
        if (System.IO.File.Exists(FilePath))
        {
            Debug.Log("file is exist");
            byte[] bytes = System.IO.File.ReadAllBytes(FilePath);
            texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);

            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            profImg = sprite;
            profilePic.sprite = sprite;
            profilePic.enabled = false;
            profilePic.enabled = true;

            yield break;
        }

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(ImgLink);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {

        }
        else
        {
            texture = new Texture2D(1, 1);
            texture = DownloadHandlerTexture.GetContent(www);

            if (!Directory.Exists(DIRPath))
            {

                Directory.CreateDirectory(DIRPath);

                Debug.Log("Created Dir" + DIRPath);
            }

            byte[] bytes = texture.EncodeToJPG();
            File.WriteAllBytes(FilePath, bytes);

            Destroy(texture);
            bytes = System.IO.File.ReadAllBytes(FilePath);
            //yield return new WaitForSeconds(2);
            texture = new Texture2D(1, 1);
            texture.LoadImage(bytes);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            profImg = sprite;
            profilePic.sprite = sprite;
            profilePic.enabled = false;
            profilePic.enabled = true;

            www.Dispose();
            bytes = new byte[0];
            System.GC.Collect();

        }
    }
}
