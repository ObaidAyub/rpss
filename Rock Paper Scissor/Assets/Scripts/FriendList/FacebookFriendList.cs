using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FacebookFriendList : MonoBehaviour
{
    public static FacebookFriendList instance;
    public GameObject fbFriendPanel;
    public Text PlayerCount;
    public Transform content;
    public FriendListPrefab friendListPrefab;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void OnPlayerListEnabel()
    {
        OnContentDestroy();
        fbFriendPanel.transform.GetComponent<RectTransform>().DOLocalMoveY(0, 0.5f);
        FacebookAuth.instance.GetFriendsPlayingThisGame(friendListPrefab, content);
    }
    public void OnPlayerListDisable()
    {
        fbFriendPanel.transform.GetComponent<RectTransform>().DOLocalMoveY(1000, 0.5f);
    }
    public void OnContentDestroy()
    {
        for (int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }
    }
}

