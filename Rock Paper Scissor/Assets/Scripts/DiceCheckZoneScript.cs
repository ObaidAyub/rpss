﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceCheckZoneScript : MonoBehaviourPunCallbacks
{
	public static DiceCheckZoneScript instance;
	Vector3 diceVelocity;
	private PhotonView PV;

	public bool asignturn = false;
	public string collidingName;
	public int DiceValue;

	private void Awake()
	{
		instance = this;
		PV = GetComponent<PhotonView>();
	}
  
	public bool diceStay = true; 

	void OnTriggerStay(Collider col)
	{
		if (AssignTurns.instance.myTurn)
		{
			if (col.CompareTag("Dice"))
			{
				if (diceStay && col.name != "Dice")
				{
					int num = 0;
					switch (col.gameObject.name)
					{
						case "Side1":
							DiceNumberTextScript.instance.diceNumber = 6;
							break;

						case "Side2":
							DiceNumberTextScript.instance.diceNumber = 5;
							break;

						case "Side3":
							DiceNumberTextScript.instance.diceNumber = 4;
							break;

						case "Side4":
							DiceNumberTextScript.instance.diceNumber = 3;
							break;

						case "Side5":
							DiceNumberTextScript.instance.diceNumber = 2;
							break;

						case "Side6":
							DiceNumberTextScript.instance.diceNumber = 1;
							break;
					}

					DiceScript.instance.number = DiceNumberTextScript.instance.diceNumber;
					DiceNumberTextScript.instance.diceNumbertext.text = DiceNumberTextScript.instance.diceNumber.ToString();

					num = DiceNumberTextScript.instance.diceNumber;
					collidingName = col.gameObject.name;
					PV.RPC("Assignment", RpcTarget.AllBuffered, collidingName, num);
					GameManager.instance.activePlayer.GetComponent<Stone>().allowMove = true;
					AssignTurns.instance.myTurn = false;
				}
			}
		}
	}
	
	[PunRPC]
	public void Assignment(string nameCol, int DiceNum)
	{
        if (GameManager.instance.TeamColor == UI.instance.teamColor)
        {
			diceStay = false;
        }
			DiceNumberTextScript.instance.diceNumber = DiceNum;
			DiceScript.instance.number = DiceNum;
			DiceNumberTextScript.instance.diceNumbertext.text = DiceNum.ToString();
			GameManager.instance.activePlayer.GetComponent<Stone>().allowMove = true;
	}
}
