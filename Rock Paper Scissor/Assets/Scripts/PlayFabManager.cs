using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;
using Newtonsoft.Json;
using PlayFab.Json;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class PlayFabManager : MonoBehaviour
{
    string loggedInPlayFabid;

    public static PlayFabManager instance;
    public GameObject nameWindow;
    public GameObject leaderBoardWindow, inAppWindow;

    public InputField nameInput;

    public GameObject rowPrefab;
    public Transform rowsParent;

    public string PlayerName;
    public string avatardata;
    public Character characterAvatar;
    public AvatarSystem avatarSystem;

    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        DontDestroyOnLoad(this);
        instance = this;
        Login();
    }


    void Login()
    {
        //UI.instance.ShowLoader("Connecting to server...");

        loadingPanel.instance.ShowLoader("Connecting to server...");
        var request = new LoginWithCustomIDRequest
        {

            CustomId = SystemInfo.deviceUniqueIdentifier,

            CreateAccount = true,
            InfoRequestParameters = new GetPlayerCombinedInfoRequestParams
            {
                GetPlayerProfile = true

            }


        };
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
    }





    void OnLogInSuccess(LoginResult result)
    {
        loggedInPlayFabid = result.PlayFabId;
        Debug.Log("Successful LogIn / Account Create ");
        string name = null;
        if (result.InfoResultPayload.PlayerProfile != null)
        {
            name = result.InfoResultPayload.PlayerProfile.DisplayName;

        }
        if (name == null)
        {
            nameWindow.SetActive(true);

        }

        else
        {

            leaderBoardWindow.SetActive(true);
        }


    }

    public void SubmitNameButton()
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {

            DisplayName = nameInput.text,

        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameUpdate, OnError);

    }

    public void OnDisplayNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Updated Display Name");
        nameWindow.SetActive(false);
        leaderBoardWindow.SetActive(true);
    }

    void OnSuccess(LoginResult result)
    {
        
        loadingPanel.instance.HideLoader();

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            Authentication.Instance.AutoLogin(UI.instance.userName, UI.instance.password);
        }
    }

    void OnError(PlayFabError Error)
    {
        Debug.Log(Error.GenerateErrorReport());
        loadingPanel.instance.HideLoader();
        Login();
    }

    public void SendLeaderboard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
            new StatisticUpdate
            {
                StatisticName="leaderBoard",
                Value = score,
            }

            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);

    }

    public void UserNameAudio()
    {
        SoundManager.PlaySound("btnSound");

    }
    void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {


    }
    public void GetLeaderBoard()
    {
        SoundManager.PlaySound("btnSound");
        var request = new GetLeaderboardRequest
        {

            StatisticName = "leaderBoard",
            StartPosition = 0,
            MaxResultsCount = 10

        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderBoardGet, OnError);

    }
    void OnLeaderBoardGetScore(GetLeaderboardResult result)
    {

    }
    void OnLeaderBoardGet(GetLeaderboardResult result)
    {
        foreach (Transform item in rowsParent)
        {
            Destroy(item.gameObject);

        }

        foreach (var item in result.Leaderboard)
        {
            GameObject newGo = Instantiate(rowPrefab, rowsParent);
            Text[] texts = newGo.GetComponentsInChildren<Text>();

            fabIds.Add(item.PlayFabId);
            texts[0].text = item.DisplayName;
            texts[1].text = item.StatValue.ToString();
        }
    }
    public Character[] boardAvatar;
    public List<Character> characterBoard;
    public List<string> fabIds;

    public void GetAvatarForBoard(List<string> fabId)
    {
        foreach (var num in fabId)
        {
            Debug.LogError("GetAvatarForBoard hy");
            int index = 0;
            var request1 = new GetUserDataRequest
            {
                PlayFabId = fabId[index]
            };
            PlayFabClientAPI.GetUserData(request1, OnAvatarForBoardRecieved, OnError);
            index++;
        }
    }
    public void OnAvatarForBoardRecieved(GetUserDataResult result1)
    {
        foreach (var num in fabIds)
        {
            Debug.LogError("OnAvatarForBoardRecieved hy");
            if (result1.Data != null & result1.Data.ContainsKey("Avatar"))
            {
                Avatars avatar = JsonConvert.DeserializeObject<Avatars>(result1.Data["Avatar"].Value);
                avatarSystem.SetAvatarBoard(avatar);
            }
        }

    }

    public void GetLeaderBoardAroundPlayer()
    {
        var request = new GetLeaderboardAroundPlayerRequest
        {
            StatisticName = "SampleScore",
            MaxResultsCount = 10
        };
        PlayFabClientAPI.GetLeaderboardAroundPlayer(request, OnLeaderBoardAroundPlayerGet, OnError);
    }

    void OnLeaderBoardAroundPlayerGet(GetLeaderboardAroundPlayerResult result)
    {
        foreach (Transform item in rowsParent)
        {
            Destroy(item.gameObject);
        }
        foreach (var item in result.Leaderboard)
        {
            GameObject newGo = Instantiate(rowPrefab, rowsParent);
            Text[] texts = newGo.GetComponentsInChildren<Text>();
            texts[0].text = (item.Position).ToString();
            texts[1].text = item.DisplayName;
            texts[2].text = item.StatValue.ToString();

            if (item.PlayFabId == loggedInPlayFabid)
            {
                texts[0].color = Color.black;
                texts[1].color = Color.black;
                texts[2].color = Color.black;
            }
        }



    }
    public void SaveCharacter()
    {
        SoundManager.PlaySound("btnSound");

        var request = new UpdateUserDataRequest
        {
            Data = new Dictionary<string, string>
            {
                {"Avatar",JsonConvert.SerializeObject(avatarSystem.ReturnClass())}
            }
        };
        PlayFabClientAPI.UpdateUserData(request, OnAvatarDataSent, OnError);
    }
    public void OnAvatarDataSent(UpdateUserDataResult result)
    {
        Debug.Log("Avatar Data Sent");
        GetAvatar();
    }

    public void GetAvatar()
    {
        PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnAvatarDataReceived, OnError);
    }
    void OnAvatarDataReceived(GetUserDataResult result)
    {

        loadingPanel.instance.ShowLoader("Fetching Details");
        //Debug.Log("Received Avatar Data" + result.Data["Avatar"].Value);
        if (result.Data != null & result.Data.ContainsKey("Avatar"))
        {
            string AvatarData = result.Data["Avatar"].Value;
            avatardata = AvatarData;
            avatarSystem.SetAvatar(AvatarData, "");

        }
        else
        {

            loadingPanel.instance.HideLoader();
        }

    }
    public void WaitToOff()
    {
        StartCoroutine(DisablingAvatarPanel());
    }
    public IEnumerator DisablingAvatarPanel()
    {
        yield return new WaitForSeconds(2f);
        avatarSystem.gameObject.SetActive(false);

    }
    public int playerNum = 0;
    public void SelectTwoPlayer()
    {
        playerNum = 2;
        RommsCanvas.instance.roomPanel.transform.DOScale(1, 1);
        RommsCanvas.instance.PlayerSelectionScreen.transform.GetChild(0).DOScale(0, 1);
        RommsCanvas.instance.PlayerSelectionScreen.SetActive(false);
        if (RommsCanvas.instance.RoomType == "private")
        {
            if (playerNum == 2)
            {
                CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
                CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(false);
                CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
                CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(false);
            }
            
        }
    }
    public void SelectFourPlayer()
    {
        playerNum = 4;
        RommsCanvas.instance.roomPanel.transform.DOScale(1, 1);
        RommsCanvas.instance.PlayerSelectionScreen.transform.GetChild(0).DOScale(0, 1);
        RommsCanvas.instance.PlayerSelectionScreen.SetActive(false);
        if (RommsCanvas.instance.RoomType == "private")
        {
            if (playerNum == 4)
            {
                CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
                CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(true);
                CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
                CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(true);
            }
        }
    }

    public void closeSelectionScreen()
    {
        RommsCanvas.instance.PlayerSelectionScreen.transform.GetChild(0).DOScale(0, 1);
        RommsCanvas.instance.PlayerSelectionScreen.SetActive(false);
    }

    
}
