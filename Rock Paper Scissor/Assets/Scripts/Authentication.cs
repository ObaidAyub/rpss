using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class Authentication : MonoBehaviour
{
	public static Authentication Instance;



	public Text userNameText;
	public Text updateScoreText;
	public Text updateSafeCardText;

	public GameObject privateEnterPanel;
	[Header("UI")]
	public Text messageText;
	public InputField emailInput;
	public InputField resetPwdInput;
	public InputField passwordInput;
	public InputField userInput;//Added
	public InputField confirmPass;//Added
	public InputField resetUserNameInput;//Added
	public Text errorText,errorPopUpHeading;//Added



	public GameObject logInScreen;//Added
	public GameObject PlayButton;//Added
	public GameObject topBar;//Added
	public GameObject signUpScreen;//Added
	public GameObject resetPassScreen; //Added
	public GameObject resetUserNameScreen; //Added
	public GameObject errorPopup; //Added
	public Text dataBaseScore; //Added


	[Header("Login Panel")]
	public InputField loginUserName;
	public InputField loginPassword;

	public Text privateRoomName;
	public GameObject privateRoomBtn;
	public static bool loggedIn;
	

	public GameObject loader;

	public GameObject splash;

	private void Awake()
	{
		Instance = this;

	}
	
	IEnumerator Start()
    {
		yield return new WaitForSeconds(4.5f);
		splash.gameObject.SetActive(false);
		getdeviceforRestoreinapp();
	}


	public void OnError(PlayFabError Error)
	{
		if (SceneManager.GetActiveScene().buildIndex == 0)
		{
			errorPopup.SetActive(true);
			errorText.text = Error.ErrorMessage;
			errorPopUpHeading.text = "Error";
			Debug.Log(Error.GenerateErrorReport());
			Debug.Log(Error.ErrorMessage);

			if (Error.ErrorMessage == "Email address not available")
			{
				PlayerPrefs.DeleteAll();
			}

			if (Error.ErrorMessage == "Invalid input parameters")
			{
				EnableLoginScreen();
			}

			if (Error.ErrorMessage == "User not found")
			{
				EnableLoginScreen();
			}
			loadingPanel.instance.HideLoader();
        }
        else
        {
			Debug.Log(Error.GenerateErrorReport());
			Debug.Log(Error.ErrorMessage);
		}
	}

	public void GetAppearence()
	{
		print(dataBaseScore);

		PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnDataRecieved, OnError);

	}

	void OnDataRecieved(GetUserDataResult result)
	{

		Debug.Log(" Recieved User Data " + result.Data);
		Debug.Log(result.Data.ToString());
		if (result.Data != null && result.Data.ContainsKey("Score"))
		{
			dataBaseScore.text = "DataBase :" + result.Data["Score"].Value;
			print(dataBaseScore);
		}
		else
		{
			Debug.Log(" Player Data Not Complete ");

		}
	}

	public void SaveAppearence(string Score, int ScoreValue, string SafeCard, int SafeCardValue, string Win, int WinValue, string Lose, int LoseValue, string date, int dateValue,
		string hours, int hoursValue, string minutes, int minutesValue, string seconds, int secondsValue)
	{
		int score = ScoreValue;
		int safeCard = SafeCardValue;
		int win = WinValue;
		int lose = LoseValue;
		int _date = dateValue;
		string _day = UI.instance.DOB_day;
		string _month = UI.instance.DOB_month;
		string _year = UI.instance.DOB_year;
		var request = new UpdateUserDataRequest
		{
			Data = new Dictionary<string, string>
			{
				
				{ "Score", score.ToString() },
				{ "SafeCard", safeCard.ToString() },
				{ "Win", win.ToString() },
				{ "Lose", lose.ToString()},
				{ "Date", _date.ToString()},
				{ "Hours", hoursValue.ToString()},
				{ "Minutes", minutesValue.ToString()},
				{ "Seconds", secondsValue.ToString()},
				{ "Day",    _day},
				{ "Month",  _month},
				{ "Year",   _year}

			}
		};

		PlayFabClientAPI.UpdateUserData(request, OnDataSend, OnError);
	}
	//UI.instance._date = UI.instance.Getint("_date");

	public void GetTitleData()
	{
		PlayFabClientAPI.GetUserData(new GetUserDataRequest(), OnTitleDataRecieved, OnError);
	}

	int Score;
	int safecard;
	int win;
	int lose;
	int date;
	int hour;
	int min;
	int sec;
	void OnTitleDataRecieved(GetUserDataResult result)
	{

		loadingPanel.instance.ShowLoader("Getting User Data...");
		if (result.Data.ContainsKey("Day"))
		{
			string email = (result.Data["Day"].Value);
			UI.instance.SetString("DOB_day", email);
			UI.instance.email = UI.instance.GetString("DOB_day");
			Debug.Log("DOB_day " + email);
		}
		if (result.Data.ContainsKey("Month"))
		{
			string email = (result.Data["Month"].Value);
			UI.instance.SetString("DOB_month", email);
			UI.instance.email = UI.instance.GetString("DOB_month");
			Debug.Log("DOB_month " + email);
		}
		if (result.Data.ContainsKey("Year"))
		{
			string email = (result.Data["Year"].Value);
			UI.instance.SetString("DOB_year", email);
			UI.instance.email = UI.instance.GetString("DOB_year");
			Debug.Log("DOB_year " + email);
		}

		if (result.Data.ContainsKey("Score"))
		{
			Score = int.Parse(result.Data["Score"].Value);
			UI.instance.SetInt("Score", Score);
			UI.instance.Score = UI.instance.Getint("Score");
		}

		if (result.Data.ContainsKey("SafeCard"))
		{
			safecard = int.Parse(result.Data["SafeCard"].Value);
			UI.instance.SetInt("SafeCard", safecard);
			UI.instance.SafeCard = UI.instance.Getint("SafeCard");
		}

		if (result.Data.ContainsKey("Win"))
		{
			win = int.Parse(result.Data["Win"].Value);
			UI.instance.SetInt("Win", win);
			UI.instance.win = UI.instance.Getint("Win");
		}

		if (result.Data.ContainsKey("Lose"))
		{
			lose = int.Parse(result.Data["Lose"].Value);
			UI.instance.SetInt("Lose", lose);
			UI.instance.lose = UI.instance.Getint("Lose");
		}
		if (result.Data.ContainsKey("Date"))
		{
			date = int.Parse(result.Data["Date"].Value);

			UI.instance.SetInt("_date", date);

			UI.instance._date = UI.instance.Getint("_date");
		}
        
		if (result.Data.ContainsKey("Hours"))
		{
			hour = int.Parse((result.Data["Hours"].Value));

			UI.instance.SetInt("hours", hour);

			UI.instance.hours = UI.instance.Getint("hours");
		}
		if (result.Data.ContainsKey("Minutes"))
		{
			min = int.Parse((result.Data["Minutes"].Value));

			UI.instance.SetInt("minutes", min);

			UI.instance.minutes = UI.instance.Getint("minutes");
		}
		if (result.Data.ContainsKey("Seconds"))
		{
			sec = int.Parse((result.Data["Seconds"].Value));

			UI.instance.SetInt("seconds", sec);

			UI.instance.seconds = UI.instance.Getint("seconds");
		}
		PlayFabManager.instance.SendLeaderboard(UI.instance.Score);
		if (UI.instance.Getint("hours") == 0 && UI.instance.Getint("minutes") == 0 && UI.instance.Getint("seconds") == 0)
		{
			UI.instance.SetString("_timer", "");
			UI.instance._timer = UI.instance.GetString("_timer");
        }
        else
        {

		UI.instance.SetString("_timer", UI.instance.Getint("hours") + "." + UI.instance.Getint("minutes") + "." + UI.instance.Getint("seconds"));
		UI.instance._timer = UI.instance.GetString("_timer");
        }

		UI.instance.UpdateScore();
        loadingPanel.instance.HideLoader();

	}





	void OnDataSend(UpdateUserDataResult result)
	{
		Debug.Log(" Successful User Data Send ! ");
	}
	public bool fbSignUp = false;
	public void RegisterThroughFB(string fbUserid, string userName, string DisplayName, string email)
    {
		
		string myEmail = "";

		if (string.IsNullOrEmpty(email))
        {
			myEmail = userName + fbUserid + "@rpss.com";
			email = myEmail;
			Debug.Log(email);
        }

		if (UI.instance.fbId == fbUserid)
		{

			AutoLogin(userName, fbUserid);
		}
		else
		{


			var request = new RegisterPlayFabUserRequest
			{
				Userid = UI.instance.userid,
				Username = userName,
				DisplayName = DisplayName,
				Email = email,
				Password = fbUserid,
				RequireBothUsernameAndEmail = true
			};

			PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);

			Debug.Log("SignUpThrough fb");

			UI.instance.SetString("UserName", userName);
			UI.instance.SetString("DisplayName", DisplayName);
			UI.instance.SetString("Email", email);
			UI.instance.SetString("Password", fbUserid);

			UI.instance.userName = UI.instance.GetString("UserName");
			UI.instance.displayName = UI.instance.GetString("DisplayName");
			UI.instance.email = UI.instance.GetString("Email");
			UI.instance.password = UI.instance.GetString("Password");
		}
    }

	public void ShowValidationPopup(string Error)
    {

		errorPopup.SetActive(true);
		errorPopUpHeading.text = "Validation Error";
		errorText.text = Error;
	}

	public void RegisterButton()
	{
		playSoundFun();
		loadingPanel.instance.ShowLoader("Signing up User...");

		if (string.IsNullOrEmpty(userInput.text))
		{
			ShowValidationPopup("User Name can not be Empty");
			loadingPanel.instance.HideLoader();
			return;
		}
		if (string.IsNullOrEmpty(emailInput.text))
		{
			ShowValidationPopup("Email can not be Empty");
			loadingPanel.instance.HideLoader();
			return;
		}
		if (string.IsNullOrEmpty(passwordInput.text))
		{
			ShowValidationPopup("Password can not be Empty");
			loadingPanel.instance.HideLoader();
			return;
		}

		if (passwordInput.text != confirmPass.text)
		{
			ShowValidationPopup("Password not matched");
			loadingPanel.instance.HideLoader();
			return;
		}
		if (passwordInput.text.Length < 6)
		{
			ShowValidationPopup("Password Too Short");
			loadingPanel.instance.HideLoader();
			return;

		}


		var request = new RegisterPlayFabUserRequest
		{
			Userid = UI.instance.userid,
			Username = userInput.text,//Added
			DisplayName = userInput.text,
			Email = emailInput.text,
			Password = passwordInput.text,
			RequireBothUsernameAndEmail = true
		};
		if (passwordInput.text.Equals(confirmPass.text))
		{
			PlayFabClientAPI.RegisterPlayFabUser(request, OnRegisterSuccess, OnError);
			UI.instance.SetString("UserName", userInput.text);
			UI.instance.SetString("DisplayName", userInput.text);
			UI.instance.SetString("Email", emailInput.text);
			UI.instance.SetString("Password", passwordInput.text);

			UI.instance.userName = UI.instance.GetString("UserName");
			UI.instance.displayName = UI.instance.GetString("DisplayName");
			UI.instance.email = UI.instance.GetString("Email");
			UI.instance.password = UI.instance.GetString("Password");
		}
		else
		{
			Debug.Log("Error in Pass");
		}
	}




	void OnRegisterSuccess(RegisterPlayFabUserResult result)
	{

		print("OnRegisterSuccess" + result);

		messageText.text = " Registered ";

		onRegisterSucess();
	}
	public void onRegisterSucess()
    {
		signUpScreen.SetActive(false);//Added
		AutoLogin(UI.instance.userName, UI.instance.password);
	}

	void OnLogInSuccess(LoginResult result)
    {
        LoginSucess();
    }

    public void LoginSucess()
    {
        Debug.Log("Login Sucess");
        messageText.text = " Logged In";

		if (FirebaseUniversalLink.Instance.FromLinkCheck)
		{
			privateEnterPanel.SetActive(true);
		}
		if (!autologin)
        {
            UI.instance.SetString("UserName", loginUserName.text);
            UI.instance.SetString("Password", loginPassword.text);
			UI.instance.SetString("DisplayName", loginUserName.text);

			UI.instance.userName = UI.instance.GetString("UserName");
            UI.instance.password = UI.instance.GetString("Password");
        }
		UI.instance.displayName = UI.instance.GetString("DisplayName");


		logInScreen.SetActive(false);
        topBar.SetActive(true);
		//DailyReward.instance.OnstartCheck();

		GetTitleData();

        UI.instance.GetAccountInfo();

        PlayFabManager.instance.GetAvatar();



        PlayButton.SetActive(true);
        loggedIn = true;


        
		userNameText.text = UI.instance.userName;
		SettingManager.instance.settigs_username.text = UI.instance.displayName;


		PhotonNetwork.NickName = UI.instance.userName;

		
		loadingPanel.instance.HideLoader();


	}

	public void loginthroughFB(string fbUserName, string fbPass,string fbID)
	{
		Debug.Log("Login Sucess through fb");
		messageText.text = " Logged In";

		if (FirebaseUniversalLink.Instance.FromLinkCheck)
		{
			privateEnterPanel.SetActive(true);
		}
		if (!autologin)
		{
			UI.instance.SetString("UserName", fbUserName);
			UI.instance.SetString("Password", fbPass);
			UI.instance.SetString("DisplayName", fbUserName);
			UI.instance.SetString("FacebookId", fbID);

			UI.instance.userName = UI.instance.GetString("UserName");
			UI.instance.password = UI.instance.GetString("Password");
			UI.instance.facebookId = UI.instance.GetString("FacebookId");
		}
		UI.instance.displayName = UI.instance.GetString("DisplayName");


		logInScreen.SetActive(false);
		topBar.SetActive(true);
		//DailyReward.instance.OnstartCheck();

		GetTitleData();

		UI.instance.GetAccountInfo();

		PlayFabManager.instance.GetAvatar();



		PlayButton.SetActive(true);
		loggedIn = true;

		userNameText.text = UI.instance.userName;
		SettingManager.instance.settigs_username.text = UI.instance.displayName;

		PhotonNetwork.NickName = UI.instance.userName;


		loadingPanel.instance.HideLoader();

	}

	public void BackFromRoomCheck()
	{
		if (SceneManager.GetActiveScene().buildIndex == 0)
		{

			if (loggedIn)
			{

				signUpScreen.SetActive(false);
				topBar.SetActive(true);


				GetTitleData();
				UI.instance.GetAccountInfo();
				PlayFabManager.instance.GetAvatar();
				PlayButton.SetActive(true);

			}
		}
	}

	public void JoinPrivateRoom()
	{
		PhotonNetwork.JoinRoom(privateRoomName.text);

	}
	public void OpenLobby()
	{
		playSoundFun();
		SceneManager.LoadScene(1);

	}
	 public bool autologin;
	public void AutoLogin(string user , string pass)
    {
		Debug.Log("autoLoginn " + autologin);

		if (!string.IsNullOrEmpty(UI.instance.userName) && !string.IsNullOrEmpty(UI.instance.password))
        {
			loadingPanel.instance.ShowLoader("Auto Logging in User...");

			var request = new LoginWithPlayFabRequest
			{

				Username = user,
				Password = pass,
			};
			PlayFabClientAPI.LoginWithPlayFab(request, OnLogInSuccess, OnError);
			autologin = true;

		}
        else if(!string.IsNullOrEmpty(UI.instance.facebookId))
        {
			loadingPanel.instance.ShowLoader("Logging in User...");
			//FacebookAuth.instance.LoginFb();
        }
		else if (!string.IsNullOrEmpty(UI.instance.appleId))
		{
			loadingPanel.instance.ShowLoader("Logging in User...");
			//FacebookAuth.instance.LoginFb();
		}
		else
        {
			autologin = false;
			EnableLoginScreen();
		}
		
	}


    public void LoginButton()
	{
		Debug.Log("login");
		playSoundFun();

		if (string.IsNullOrEmpty(loginUserName.text))
		{
			ShowValidationPopup("User Name can not be Empty");
			return;
		}
		if (string.IsNullOrEmpty(loginPassword.text))
		{
			ShowValidationPopup("password can not be Empty");
			return;
		}



		var request = new LoginWithPlayFabRequest
		{

			Username = loginUserName.text,
			Password = loginPassword.text,
		};
		PlayFabClientAPI.LoginWithPlayFab(request, OnLogInSuccess, OnError);

	}

	public void playSoundFun()
	{

		SoundManager.PlaySound("btnSound");
	}

	public void ResetPasswordButton()
	{
		playSoundFun();
		var request = new SendAccountRecoveryEmailRequest
		{
			Email = resetPwdInput.text,
			TitleId = "E26E9",

		};
		PlayFabClientAPI.SendAccountRecoveryEmail(request, OnPasswordReset, OnError);

	}

	

	void OnPasswordReset(SendAccountRecoveryEmailResult result)
	{
		messageText.text = " Password Reset Mail Sent ";
	}

	

	// Panels\

	public void EnableLoginScreen()
	{
		Debug.Log("EnableLoginScreen");
		playSoundFun();
		signUpScreen.SetActive(false);
		logInScreen.SetActive(true);
		GameAnimations.instance.LoginAnimation();
	}


	public void EnableSignUpScreen()
	{
		playSoundFun();
		signUpScreen.SetActive(true);
		logInScreen.SetActive(false);
		GameAnimations.instance.SignUpAnimation();
	}

	public void OpenResetPasswordPanel()
	{
		playSoundFun();
		logInScreen.SetActive(false);
		resetPassScreen.SetActive(true);
	}

	public void Logout()
    {

        if (string.IsNullOrEmpty( UI.instance.facebookId))
        {
			FacebookAuth.instance.LogOutFb();
        }

		PlayerPrefs.DeleteAll();

		if (UI.instance.gameObject.activeInHierarchy)
		{
			Destroy(UI.instance.gameObject);
			UI.instance = null;
		}
		if (PlayFabManager.instance.gameObject.activeInHierarchy)
		{
			Destroy(PlayFabManager.instance.gameObject);
			PlayFabManager.instance = null;
		}


		SceneManager.LoadScene(0);
	}
	public void loginthroughApple(string appleuserName, string Email, string fbID)
	{
		Debug.Log("Login Sucess through Apple");
		messageText.text = " Logged In";

		if (FirebaseUniversalLink.Instance.FromLinkCheck)
		{
			privateEnterPanel.SetActive(true);
		}
		if (!autologin)
		{
			UI.instance.SetString("UserName", appleuserName);
			UI.instance.SetString("Email", Email);
			UI.instance.SetString("DisplayName", appleuserName);
			UI.instance.SetString("AppleId", fbID);

			UI.instance.userName = UI.instance.GetString("UserName");
			UI.instance.password = UI.instance.GetString("Password");
			UI.instance.facebookId = UI.instance.GetString("AppleId");
		}
		UI.instance.displayName = UI.instance.GetString("DisplayName");

		
		

		logInScreen.SetActive(false);
		topBar.SetActive(true);
		//DailyReward.instance.OnstartCheck();

		GetTitleData();

		UI.instance.GetAccountInfo();

		PlayFabManager.instance.GetAvatar();



		PlayButton.SetActive(true);
		loggedIn = true;

		userNameText.text = UI.instance.userName;
		SettingManager.instance.settigs_username.text = UI.instance.displayName;

		PhotonNetwork.NickName = UI.instance.userName;

        if (string.IsNullOrEmpty( userNameText.text.ToString()))
        {
			

		}

		loadingPanel.instance.HideLoader();

	}
	public void SetUserName(string Name)
    {
		var request = new UpdateUserTitleDisplayNameRequest
		{
			DisplayName = Name,
		};
		PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUserName, OnError);
	}
	void OnUserName(UpdateUserTitleDisplayNameResult result)
	{
		Debug.Log("username Updated ");
		//messageText.text = " User name updated ";
		//resetUserNameScreen.SetActive(false);
		//FindObjectOfType<UI>().GetAccountInfo();
	}




	public GameObject restorebtn;
	public void getdeviceforRestoreinapp()
    {
        if (Application.platform == RuntimePlatform.Android )
        {
			restorebtn.SetActive(false);
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			restorebtn.SetActive(true);
		}
    }
}
