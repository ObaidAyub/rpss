using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class RoomListing : MonoBehaviour
{

    [SerializeField]
    private Text _text;

    public string roomName;
    public int MaxPlayer;
    [SerializeField]
    public RoomInfo RoomInfo { get; private set; }

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        _text.text = roomInfo.MaxPlayers + " , " + roomInfo.Name;
        MaxPlayer = roomInfo.MaxPlayers;
        roomName = roomInfo.Name;
        UI.instance.RoomName = RoomInfo.Name;
    }

    public void OnClick_Button()
    {

        PhotonNetwork.JoinRoom(RoomInfo.Name);

        if (MaxPlayer == 2)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(false);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(false);
            PlayFabManager.instance.playerNum = 2;
        }
        else if (MaxPlayer == 4)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(true);
            PlayFabManager.instance.playerNum = 4;
        }
    }
}
