using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMusiic : MonoBehaviour
{
	static AudioSource audioSource;
	public static AudioClip BgSound;


	private void Awake()
	{
		//DontDestroyOnLoad(this.gameObject);
	}
	private void OnEnable()
	{
		if(PlayerPrefs.GetInt("Sound",1) == 1)
		{
			this.gameObject.GetComponent<AudioSource>().enabled = true;
		}
	}
	private void Start()
	{
		audioSource = GetComponent<AudioSource>();
		BgSound = Resources.Load<AudioClip>("BgSound");
	}

	public static void PlaySound(string clip)
	{

		switch (clip)
		{
			case "BgSound":
				audioSource.Stop();
				break;
		}
	}
}
