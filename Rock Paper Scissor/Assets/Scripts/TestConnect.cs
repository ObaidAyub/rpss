﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TestConnect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update

    public Text userName;
    void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            Debug.Log("it's connected on MasterServer");
            loadingPanel.instance.HideLoader();
            // it's connected on MasterServer
        }
        else
        {

        onStart("Connecting to Game Server");
        }
    }

    void onStart(string msg)
    {
        //print(" Connecting To Server ");

        //UI.instance.ShowLoader("Connecting to Game Server");
        //Authentication.Instance.loader.GetComponent<loadingPanel>().ShowLoader("Connecting to Game Server");
        loadingPanel.instance.ShowLoader(msg);
        //PhotonNetwork.SendRate = 20; // its 20 on default. // this is how many packets a player can send

        //PhotonNetwork.SerializationRate = 5; // by default its 10.  //Data is sending 5 times per second

        PhotonNetwork.AutomaticallySyncScene = true; // this will synce for all.
        PhotonNetwork.NickName = UI.instance.userName;// MasterManager.GameSettings.NickName;
        PhotonNetwork.GameVersion = MasterManager.GameSettings.GameVersion;
        PhotonNetwork.ConnectUsingSettings();

        //userName.text = "Hello " + UI.instance.userName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public Text privateRoomName;
    public void JoinPrivateRoom()
    {
        //SceneManager.LoadScene(1);
        if (!string.IsNullOrEmpty(privateRoomName.text.ToLower().ToString()))
        {
            PhotonNetwork.JoinRoom(privateRoomName.text.ToLower());
            RommsCanvas.instance.EnableRoomPanel();
            RommsCanvas.instance.RoomType = "private";
        }

    }
    public override void OnConnectedToMaster()
    {
        
        Debug.Log(" Connected To Photon ",this);


        //if (CreateRoomMenu.privateRoom == true)
        //{
        //    Debug.Log("true");

        //   PhotonNetwork.CurrentRoom.IsVisible = false;

        //}
        JoinPrivateRoom();

        //Debug.Log(" My NickName Is :"+PhotonNetwork.LocalPlayer.NickName,this);
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();

            
        }
        //UI.instance.Hideloader();

        //Authentication.Instance.PlayButton.SetActive(true);
        loadingPanel.instance.HideLoader();

        //if (!PhotonNetwork.InLobby)
        //{
        //    PhotonNetwork.JoinLobby();

        //}
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log(" Failed to contect Photon:  " +cause.ToString(), this);
        loadingPanel.instance.ShowLoader(cause.ToString());

        onStart("Trying to connecting the server again...");
    }

    public override void OnJoinedLobby()
    {
        //print("Joined Lobby");

        loadingPanel.instance.HideLoader();
        //StartCoroutine(MultiplayerGameController.instance.ShowInterstitial());
    }
}
