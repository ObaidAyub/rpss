using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MyTestConnect : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Text privateRoomName;
    public void JoinPrivateRoom()
    {
        //SceneManager.LoadScene(1);
        PhotonNetwork.JoinRoom(privateRoomName.text);

    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true; // this will synce for all.

        PhotonNetwork.NickName = MasterManager.GameSettings.NickName;
        PhotonNetwork.GameVersion = MasterManager.GameSettings.GameVersion;
        PhotonNetwork.ConnectUsingSettings();

        Debug.Log(" Connected To Photon ", this);

        JoinPrivateRoom();

        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();

        }

       
    }

   
}
