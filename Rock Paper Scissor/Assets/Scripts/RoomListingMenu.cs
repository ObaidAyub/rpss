using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class RoomListingMenu : MonoBehaviourPunCallbacks
{
    public static RoomListingMenu instance;

    [SerializeField]
    private Transform _content;
    [SerializeField]
    private RoomListing _roomListing;

    private List<RoomListing> _listings = new List<RoomListing>();

    private RommsCanvas rommsCanvas;


    private void Awake()
    {
        instance = this;
    }
    public void FirstInitialize(RommsCanvas canvases)
    {
        rommsCanvas = canvases;
    }




    public override void OnJoinedRoom()
    {
        rommsCanvas.currentRoomCanvas.show();
        //_content.DestroyChildren();
        //_listings.Clear();
    }


    
    

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            Debug.Log(info + " " + info.MaxPlayers); 
            // Removed From Room List
            if (info.RemovedFromList)
            {
                int index = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (index != -1)
                {
                    Destroy(_listings[index].gameObject);
                    _listings.RemoveAt(index);
                }
            }
            //Added to Room List
            else
            {
                int index = _listings.FindIndex(x => x.RoomInfo.Name == info.Name);
                if (index == -1)
                {
                    Debug.Log("Creat room list");
                    RoomListing listing = Instantiate(_roomListing, _content);
                    if (listing != null)
                    {
                        listing.SetRoomInfo(info);
                        _listings.Add(listing);
                    }
                }
                else
                { 
                // modify listing here

                }
            }
        }
    }

    public void DestroyRoomOnLeave()
    {
        for (int i = 0; i < _content.childCount; i++)
        {
            Destroy(_content.GetChild(i).gameObject);
        }
    }
}
