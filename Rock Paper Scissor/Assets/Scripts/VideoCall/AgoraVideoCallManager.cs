using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AgoraVideoCallManager : MonoBehaviour
{
    public static AgoraVideoCallManager instance;

    public TestHome agoraVideoFun;

    public RawImage PlayerRed, PlayerGreen, PlayerPurple, PlayerBlue;

    public Button redCancleBtn;
                              
    public Button redCameraBtn;
    //public bool videoInitFlag;
    private PhotonView PV;

    public RectTransform callRecivingPanel;

    public Button acceptCall, rejectCall;

    public string V_id;
    public bool isConnected = false;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        PV = GetComponent<PhotonView>();
        acceptCall.onClick.RemoveAllListeners();
        acceptCall.onClick.AddListener(() =>
        {
            StartCall();
            showCallPopup(false);
        });

        rejectCall.onClick.RemoveAllListeners();
        rejectCall.onClick.AddListener(() => { showCallPopup(false); });


        redCameraBtn.onClick.RemoveAllListeners();
       

        redCameraBtn.onClick.AddListener(agoraVideoFun.switchCamera);
       
    }

    void Awake()
    {
        PlayerRed.gameObject.SetActive(false);
        PlayerGreen.gameObject.SetActive(false);
        PlayerPurple.gameObject.SetActive(false);
        PlayerBlue.gameObject.SetActive(false);

        redCancleBtn.gameObject.SetActive(false);
       

        redCameraBtn.gameObject.SetActive(false);
        

    }
    public void StartCall()
    {
        loadingPanel.instance.ShowLoader("Initializing Video call..");
        agoraVideoFun.onJoinButtonClicked();


        PV.RPC("JoinVideoCall", RpcTarget.AllBuffered, GameManager.instance.TeamColor);
    }

    [PunRPC]
    public void JoinVideoCall(string teamColor)
    {
        Debug.Log("teamColor" + teamColor);
        V_id = teamColor;

        if (teamColor == GameManager.instance.TeamColor)
        {
            Debug.Log("Return");
            return;
        }

        switch (teamColor)
        {
            case "Red":
                AgoraVideoCallManager.instance.PlayerRed.gameObject.SetActive(true);
                //AgoraVideoCallManager.instance.redCancleBtn.gameObject.SetActive(true);
                break;
            case "Purple":
                AgoraVideoCallManager.instance.PlayerPurple.gameObject.SetActive(true);
                //AgoraVideoCallManager.instance.purpleCancleBtn.gameObject.SetActive(true);
                break;
            case "Green":
                AgoraVideoCallManager.instance.PlayerGreen.gameObject.SetActive(true);
                //AgoraVideoCallManager.instance.greenCancleBtn.gameObject.SetActive(true);
                break;
            case "Blue":
                AgoraVideoCallManager.instance.PlayerBlue.gameObject.SetActive(true);
                //AgoraVideoCallManager.instance.blueCancleBtn.gameObject.SetActive(true);
                break;
        }
        if (!isConnected)
        {
            showCallPopup(true);
        }

        loadingPanel.instance.HideLoader();

    }

    // Update is called once per frame
    public void LeaveCall()
    {
        agoraVideoFun.onLeaveButtonClicked();
        isConnected = false;

    }

    public void showCallPopup(bool isopen)
    {
        if (isopen)
        {
        callRecivingPanel.DOMoveY(0, 0.5f);
        }
        else
        {
            callRecivingPanel.DOMoveY(-500, 0.5f);
        }


    }

}
