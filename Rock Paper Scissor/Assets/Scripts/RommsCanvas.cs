using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RommsCanvas : MonoBehaviour
{

    public static RommsCanvas instance;
    [SerializeField]
    private CreateOrJoinRoomCnvas _createOrJoinRoomCanvas;

    public CreateOrJoinRoomCnvas CreateOrJoinRoomCanvas { get { return _createOrJoinRoomCanvas; } }

    [SerializeField]
    private CurrentRoomCanvas _currentRoomCanvas;

    public CurrentRoomCanvas currentRoomCanvas { get { return _currentRoomCanvas; } }

    public GameObject roomPanel, PlayerSelectionScreen;

    public string RoomType;
    private void Awake()
    {
        instance = this;
        FirstInitialize();
        RommsCanvas.instance.RoomType = "";
    }

    private void FirstInitialize()
    {
        CreateOrJoinRoomCanvas.FirstInitialize(this);
        currentRoomCanvas.FirstInitialize(this);
    }


    public void EnableRoomPanel()
    {
        roomPanel.transform.DOScale(0, 1);
        PlayerSelectionScreen.SetActive(true);
        PlayerSelectionScreen.transform.GetChild(0).DOScale(1, 1);
    }
    public void backBtn()
    {
        roomPanel.transform.DOScale(0f, 0.2f);
        if (_currentRoomCanvas.gameObject.activeInHierarchy)
        {

        LeaveRoomMenu.instance.OnClick_LeaveRoom();
        }
    }
}
