 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOrJoinRoomCnvas : MonoBehaviour
{
    public static CreateOrJoinRoomCnvas instance;

    

    [SerializeField]
    private RoomListingMenu _roomListingMenu;

    [SerializeField]
    private CreateRoomMenu _createRoomMenu;
    private RommsCanvas _roomCanvases;

    public GameObject pawnSelectionScr;

    public GameObject[] PawnObjs;

    private void Awake()
    {
        instance = this;
    }
    public void FirstInitialize(RommsCanvas canvases)
    {
        _roomCanvases = canvases;
        _createRoomMenu.FirstInitialize(canvases);
        _roomListingMenu.FirstInitialize(canvases);
    }
}
