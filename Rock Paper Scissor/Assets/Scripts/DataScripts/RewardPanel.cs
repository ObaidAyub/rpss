using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System.Diagnostics.SymbolStore;

public class RewardPanel : MonoBehaviour
{
    public static RewardPanel instance;
    public RectTransform rewardPanel;
    public TextMeshProUGUI pointSlogan, pointsTxt;

    private void Awake()
    {
        instance = this;
    }   // Start is called before the first frame update
    public void GetScorePointsAndText(string slogan, int scorePoints)
    {
        pointSlogan.text = slogan;

        pointsTxt.text = scorePoints + " ScorePoints";

        Debug.Log("Score Points: " + scorePoints);

        Debug.Log("Previous Points: " + UI.instance.Getint("Score"));

        int scoreValue = UI.instance.Getint("Score") + scorePoints;

        UI.instance.SetInt("Score", scoreValue);

        GameManager.instance.points.text = UI.instance.Getint("Score").ToString();

        Authentication.Instance.SaveAppearence("Score", UI.instance.Getint("Score"),
            "SafeCard", UI.instance.Getint("SafeCard"),
            "Win", UI.instance.Getint("Win"),
            "Lose", UI.instance.Getint("Lose"),
            "_date", UI.instance.Getint("_date"),
            "Hours", UI.instance.Getint("hours"),
            "Minutes", UI.instance.Getint("minutes"),
            "Seconds", UI.instance.Getint("seconds"));

        UI.instance.UpdateScore();
    }

    public void ShowAndHideScreen(bool show)
    {
        if (show)
        {
            this.gameObject.SetActive(show);
            rewardPanel.transform.DOLocalMoveY(0, 0.5f);
        }
        else
        {
            StartCoroutine(populateRwardScreen(show));
        }
    }

    IEnumerator populateRwardScreen(bool show)
    {
        rewardPanel.transform.DOLocalMoveY(1000, 0.5f);
        yield return new WaitForSeconds(0.5f);
            this.gameObject.SetActive(show);
        
    }
}
