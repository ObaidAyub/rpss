using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InAppManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static InAppManager instance;
    public Text Message;
    public GameObject Popup;

    private void Awake()
    {
        instance = this;
    }
    public void PurchaseInApp()
    {
        Popup.gameObject.SetActive(true);
        Message.text = "Will Come in Next Update...!";
    }
}
