using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using LoginResult = PlayFab.ClientModels.LoginResult;



public class FacebookAuth : MonoBehaviour
{
    // Import statements introduce all the necessary classes for this example.

    // holds the latest message to be displayed on the screen

    public static FacebookAuth instance;
    private string _message;

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    void Awake()
    {

        instance = this;

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }
    IEnumerator Start()
    {
        yield return new WaitForSeconds(2);
        RetriveLoginOnPreviousDevice();


    }
    public void RetriveLoginOnPreviousDevice()
    {

        Debug.Log("RetriveLoginOnPreviousDevice");


#if UNITY_IPHONE 
        GetProfileInfo();

#endif
#if UNITY_ANDROID
    FB.Android.RetrieveLoginStatus(LoginStatusCallback);

#endif


    }

    private void GetProfileInfo()
    {
        var profile = FB.Mobile.CurrentProfile();
        if (profile != null)
        {
            Debug.Log(profile.Name);
            Debug.Log(profile.UserID);
            Debug.Log(profile.Email);
            Debug.Log(profile.ImageURL);
            Debug.Log(profile.Birthday);
            Debug.Log(profile.AgeRange);
            Debug.Log(profile.FriendIDs);
            Debug.Log(profile.Gender);
            Debug.Log(profile.LinkURL);
            Debug.Log(profile.Hometown);
            Debug.Log(profile.Location);
            Authentication.Instance.loginthroughFB(profile.Name, "", profile.UserID);
        }
    }
    private void LoginStatusCallback(ILoginStatusResult result)
    {
        Debug.Log("result " + result);

        if (!string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Error: " + result.Error);
            //loginUIManager.instance.loadingScr.SetActive(false);
        }
        else if (result.Failed)
        {
            Debug.Log("Failure: Access Token could not be retrieved");
            //loginUIManager.instance.loadingScr.SetActive(false);
        }
        else
        {
            Debug.Log("Success: " + result.AccessToken.UserId);

            FetchFBProfile();
        }
    }
    private void FetchFBProfile()
    {
        FB.API("/me?fields=first_name,email,picture,name", HttpMethod.GET, FetchProfileCallback, new Dictionary<string, string>() { });
    }
    private void FetchProfileCallback(IGraphResult result)
    {
        Debug.Log(result.RawResult);
        Root fbItem = JsonConvert.DeserializeObject<Root>(result.RawResult);

        //loginUIManager.instance.GetUserData(myDeserializedClass.name,
        //                                    myDeserializedClass.id,
        //                                    myDeserializedClass.email,
        //                                    myDeserializedClass.picture.data.url
        //                                    );
        Debug.Log(fbItem.name);
        Debug.Log(fbItem.id);
        Debug.Log(fbItem.email);
        Debug.Log(fbItem.picture.data.url);

       Authentication.Instance.loginthroughFB(fbItem.name,"",fbItem.id);
    }
    public void LoginFb()
    {
        Debug.Log("Initializing Facebook..."); // logs the given message and displays it on the screen using OnGUI method

        // This call is required before any other calls to the Facebook API. We pass in the callback to be invoked once initialization is finished
        OnFacebookInitialized();
    }

    private void OnFacebookInitialized()
    {
        Debug.Log("Logging into Facebook...");

        // Once Facebook SDK is initialized, if we are logged in, we log out to demonstrate the entire authentication cycle.
        //if (FB.IsLoggedIn)
        //    FB.LogOut();

        // We invoke basic login procedure and pass in the callback to process the result
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        // If result has no errors, it means we have authenticated in Facebook successfully
        Debug.Log(result.RawResult);
        if (result == null || string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Facebook Auth Complete! Access Token: " + AccessToken.CurrentAccessToken.TokenString + "\nLogging into PlayFab...");

            /*
             * We proceed with making a call to PlayFab API. We pass in current Facebook AccessToken and let it create
             * and account using CreateAccount flag set to true. We also pass the callback for Success and Failure results
             */
            PlayFabClientAPI.LoginWithFacebook(new LoginWithFacebookRequest { CreateAccount = true, AccessToken = AccessToken.CurrentAccessToken.TokenString },
                OnPlayfabFacebookAuthComplete, OnPlayfabFacebookAuthFailed);
        }
        else
        {
            // If Facebook authentication failed, we stop the cycle with the message
            Debug.Log("Facebook Auth Failed: " + result.Error + "\n" + result.RawResult +  true);
        }
    }

    // When processing both results, we just set the message, explaining what's going on.
    private void OnPlayfabFacebookAuthComplete(LoginResult result)
    {
        Debug.Log("PlayFab Facebook Auth Complete. Session ticket: " + result.SessionTicket);
        FetchFBProfile();
    }

    private void OnPlayfabFacebookAuthFailed(PlayFabError error)
    {
        Debug.Log("PlayFab Facebook Auth Failed: " + error.GenerateErrorReport() + true);
    }

    //public void Debug.Log(string message, bool error = false)
    //{
    //    _message = message;
    //    if (error)
    //        Debug.LogError(_message);
    //    else
    //        Debug.Log(_message);
    //}
    public void shareWithFriends()
    {
        FB.ShareLink(
           new Uri("https://developers.facebook.com/"),
           callback: ShareCallback);
    }

    private void ShareCallback(IShareResult result)
    {
        if (result.Cancelled || !String.IsNullOrEmpty(result.Error))
        {
            Debug.Log("ShareLink Error: " + result.Error);
        }
        else if (!String.IsNullOrEmpty(result.PostId))
        {
            // Print post identifier of the shared content
            Debug.Log(result.PostId);
        }
        else
        {
            // Share succeeded without postID
            Debug.Log("ShareLink success!");
        }
    }
    public string FriendsText;
    public void GetFriendsPlayingThisGame(FriendListPrefab friendListPrefab, Transform content)
    {
        string query = "/me/friends";
        FB.API(query, HttpMethod.GET, result =>
        {
            Debug.Log(result.RawResult);

            UserFriedData myDeserializedClass = JsonConvert.DeserializeObject<UserFriedData>(result.RawResult);

            FacebookFriendList.instance.PlayerCount.text = myDeserializedClass.summary.total_count.ToString();

            for (int i = 0; i < myDeserializedClass.data.Count; i++)
            {
                FriendListPrefab item = Instantiate(friendListPrefab, content);
                item.playerName.text = myDeserializedClass.data[i].name;
                item.userId = myDeserializedClass.data[i].id;
                Debug.Log(myDeserializedClass.data[i].picture.data.url);
                item.GetUserPicture(myDeserializedClass.data[i].picture.data.url);
            }
        });
    }

    public void LogOutFb()
    {
        FB.LogOut();
    }

   

    public class Data
    {
        public int height { get; set; }
        public bool is_silhouette { get; set; }
        public string url { get; set; }
        public int width { get; set; }
    }

    public class Picture
    {
        public Data data { get; set; }
    }

    public class Root
    {
        public string first_name { get; set; }
        public string email { get; set; }
        public Picture picture { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }


    #region

    
    public class Cursors
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class Datam
    {
        public int height { get; set; }
        public bool is_silhouette { get; set; }
        public string url { get; set; }
        public int width { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public User_Picture picture { get; set; }
    }

    public class Paging
    {
        public Cursors cursors { get; set; }
    }

    public class User_Picture
    {
        public Datam data { get; set; }
    }

    public class UserFriedData
    {
        public List<Datam> data { get; set; }
        public Paging paging { get; set; }
        public Summary summary { get; set; }
    }

    public class Summary
    {
        public int total_count { get; set; }
    }


    #endregion
}