﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class loadingPanel : MonoBehaviour
{

    public static loadingPanel instance;

    public GameObject LoadingScreen;
    public GameObject loader;
    public Text loadingText;

    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if (LoadingScreen.activeInHierarchy)
        {
        loader.transform.Rotate(0, 0, 35f * Time.deltaTime * 5f);

        }
    }
    public void ShowLoader(string ShowText)
    {
        LoadingScreen.gameObject.SetActive(true);
        loadingText.text = ShowText;
    }

    public void HideLoader()
    {
        LoadingScreen.gameObject.SetActive(false);
    }

}
