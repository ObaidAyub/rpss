using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using LoginResult = PlayFab.ClientModels.LoginResult;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using System.Text;
using UnityEngine.UI;

public class AppleAuthentication : MonoBehaviour
{
    private const string AppleUserIdKey = "AppleUserId";

    private IAppleAuthManager _appleAuthManager;
    public Button SignInWithAppleButton;
    //public LoginMenuHandler LoginMenu;
    //public GameMenuHandler GameMenu;

    public string _UserName, _Email, _Id, _IdToken, _password, _user;

    private void Start()
    {
        // If the current platform is supported
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
            var deserializer = new PayloadDeserializer();
            // Creates an Apple Authentication manager with the deserializer
            this._appleAuthManager = new AppleAuthManager(deserializer);
        }
        Debug.Log("Here");
        this.InitializeLoginMenu();
    }

    private void Update()
    {
        // Updates the AppleAuthManager instance to execute
        // pending callbacks inside Unity's execution loop
        if (this._appleAuthManager != null)
        {
            this._appleAuthManager.Update();
        }

        //this.LoginMenu.UpdateLoadingMessage(Time.deltaTime);
    }

    public void SignInWithAppleButtonPressed()
    {
        this.SetupLoginMenuForAppleSignIn();
        this.SignInWithApple();
    }

    private void InitializeLoginMenu()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);

        // Check if the current platform supports Sign In With Apple
        if (this._appleAuthManager == null)
        {
            Debug.Log("unsupported device");
            this.SetupLoginMenuForUnsupportedPlatform();
            return;
        }

        // If at any point we receive a credentials revoked notification, we delete the stored User ID, and go back to login
        this._appleAuthManager.SetCredentialsRevokedCallback(result =>
        {
            Debug.Log("Received revoked callback " + result);
            this.SetupLoginMenuForSignInWithApple();
            PlayerPrefs.DeleteKey(AppleUserIdKey);
        });

        // If we have an Apple User Id available, get the credential status for it
        if (PlayerPrefs.HasKey(AppleUserIdKey))
        {
            Debug.Log("checking credientials");
            var storedAppleUserId = PlayerPrefs.GetString(AppleUserIdKey);
            this.SetupLoginMenuForCheckingCredentials();
            this.CheckCredentialStatusForUserId(storedAppleUserId);
        }
        // If we do not have an stored Apple User Id, attempt a quick login
        else
        {
            Debug.Log("Quick login");
            this.SetupLoginMenuForQuickLoginAttempt();
            this.AttemptQuickLogin();
        }
    }

    private void SetupLoginMenuForUnsupportedPlatform()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);
        this.SetSignInWithAppleButton(visible: false, enabled: false);
        //this.LoginMenu.SetLoadingMessage(visible: true, message: "Unsupported platform");
        Debug.Log("Unsupported platform");
    }

    private void SetupLoginMenuForSignInWithApple()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);
        this.SetSignInWithAppleButton(visible: true, enabled: true);
        //this.LoginMenu.SetLoadingMessage(visible: false, message: string.Empty);
    }

    private void SetupLoginMenuForCheckingCredentials()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);
        this.SetSignInWithAppleButton(visible: true, enabled: false);
        //this.LoginMenu.SetLoadingMessage(visible: true, message: "Checking Apple Credentials");
    }

    private void SetupLoginMenuForQuickLoginAttempt()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);
        this.SetSignInWithAppleButton(visible: true, enabled: false);
        //this.LoginMenu.SetLoadingMessage(visible: true, message: "Attempting Quick Login");
    }

    private void SetupLoginMenuForAppleSignIn()
    {
        //this.LoginMenu.SetVisible(visible: true);
        //this.GameMenu.SetVisible(visible: false);
        this.SetSignInWithAppleButton(visible: true, enabled: false);
        //this.LoginMenu.SetLoadingMessage(visible: true, message: "Signing In with Apple");
    }

    private void SetupGameMenu(string appleUserId, ICredential credential)
    {
        //this.LoginMenu.SetVisible(visible: false);
        //this.GameMenu.SetVisible(visible: true);
        SetupAppleData(appleUserId, credential);
        //PlayFabClientAPI.LoginWithFacebook(new LoginWithFacebookRequest { CreateAccount = true, AccessToken = AccessToken.CurrentAccessToken.TokenString },
        //        OnPlayfabFacebookAuthComplete, OnPlayfabFacebookAuthFailed);

        

    }

    private void CheckCredentialStatusForUserId(string appleUserId)
    {
        // If there is an apple ID available, we should check the credential state
        this._appleAuthManager.GetCredentialState(
            appleUserId,
            state =>
            {
                switch (state)
                {
                    // If it's authorized, login with that user id
                    case CredentialState.Authorized:
                        this.SetupGameMenu(appleUserId, null);
                        return;

                    // If it was revoked, or not found, we need a new sign in with apple attempt
                    // Discard previous apple user id
                    case CredentialState.Revoked:
                    case CredentialState.NotFound:
                        this.SetupLoginMenuForSignInWithApple();
                        PlayerPrefs.DeleteKey(AppleUserIdKey);
                        return;
                }
            },
            error =>
            {
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                Debug.LogWarning("Error while trying to get credential state " + authorizationErrorCode.ToString() + " " + error.ToString());
                this.SetupLoginMenuForSignInWithApple();
            });
    }

    private void AttemptQuickLogin()
    {
        Debug.Log("AttemptQuickLogin");
        var quickLoginArgs = new AppleAuthQuickLoginArgs();

        // Quick login should succeed if the credential was authorized before and not revoked
        this._appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                // If it's an Apple credential, save the user ID, for later logins
                var appleIdCredential = credential as IAppleIDCredential;
                if (appleIdCredential != null)
                {
                    PlayerPrefs.SetString(AppleUserIdKey, credential.User);
                }
                Debug.Log("crediential user : " + credential.User);
                Debug.Log("crediential : " + credential);
                this.SetupGameMenu(credential.User, credential);
            },
            error =>
            {
                // If Quick Login fails, we should show the normal sign in with apple menu, to allow for a normal Sign In with apple
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                Debug.Log("Quick Login Failed " + authorizationErrorCode.ToString() + " " + error.ToString());
                this.SetupLoginMenuForSignInWithApple();
            });
    }

    private void SignInWithApple()
    {
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        this._appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                // If a sign in with apple succeeds, we should have obtained the credential with the user id, name, and email, save it
                PlayerPrefs.SetString(AppleUserIdKey, credential.User);
                this.SetupGameMenu(credential.User, credential);
            },
            error =>
            {
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                Debug.LogWarning("Sign in with Apple failed " + authorizationErrorCode.ToString() + " " + error.ToString());
                this.SetupLoginMenuForSignInWithApple();
            });

        
    }
    public void SetupAppleData(string appleUserId, ICredential receivedCredential)
    {
        //this.AppleUserIdLabel.text = "Apple User ID: " + appleUserId;

        //Id.text = "Apple User ID: " + appleUserId;
        _Id = appleUserId;
        UI.instance.SetString("AppleId",appleUserId);

        if (receivedCredential == null)
        {
            //this.AppleUserCredentialLabel.text = "NO CREDENTIALS RECEIVED\nProbably credential status for " + appleUserId + "was Authorized";
            Debug.Log("No creditential returns : ");
            //AttemptQuickLogin();
            PlayFabClientAPI.LoginWithApple(new LoginWithAppleRequest
            {
                CreateAccount = true,
                IdentityToken = UI.instance.appleToken,
                TitleId = "E26E9",
                PlayerSecret = "B1HUZFT69WFWKGYKJWYR4WBCXRQR7PIUFQG516D1H8RJOIFKUP"
            },
            OnPlayfabAppleAuthComplete, OnPlayfabAppleAuthFailed);
            return;
        }

        var appleIdCredential = receivedCredential as IAppleIDCredential;
        var passwordCredential = receivedCredential as IPasswordCredential;

       

        //UserName, Email, Id, IdToken;


        if (appleIdCredential != null)
        {
            var stringBuilder = new StringBuilder();
            //stringBuilder.AppendLine("RECEIVED APPLE ID CREDENTIAL.\nYOU CAN LOGIN/CREATE A USER WITH THIS");
            //stringBuilder.AppendLine("<b>Username:</b> " + appleIdCredential.User);
            //stringBuilder.AppendLine("<b>Real user status:</b> " + appleIdCredential.RealUserStatus.ToString());


            if (appleIdCredential.State != null)
                stringBuilder.AppendLine("<b>State:</b> " + appleIdCredential.State);

            if (appleIdCredential.IdentityToken != null)
            {
                var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken, 0, appleIdCredential.IdentityToken.Length);
                //stringBuilder.AppendLine("<b>Identity token (" + appleIdCredential.IdentityToken.Length + " bytes)</b>");
                //stringBuilder.AppendLine(identityToken.Substring(0, 45) + "...");

                //IdToken.text = identityToken;
                _IdToken = identityToken;
                UI.instance.SetString("AppleToken", _IdToken);
            }

            if (appleIdCredential.AuthorizationCode != null)
            {
                var authorizationCode = Encoding.UTF8.GetString(appleIdCredential.AuthorizationCode, 0, appleIdCredential.AuthorizationCode.Length);
                stringBuilder.AppendLine("<b>Authorization Code (" + appleIdCredential.AuthorizationCode.Length + " bytes)</b>");
                stringBuilder.AppendLine(authorizationCode.Substring(0, 45) + "...");
            }

            if (appleIdCredential.AuthorizedScopes != null)
                stringBuilder.AppendLine("<b>Authorized Scopes:</b> " + string.Join(", ", appleIdCredential.AuthorizedScopes));

            //Debug.Log("email : " + appleIdCredential.Email);
            if (appleIdCredential.Email != null)
            {
                //stringBuilder.AppendLine();
                //stringBuilder.AppendLine("<b>EMAIL RECEIVED: YOU WILL ONLY SEE THIS ONCE PER SIGN UP. SEND THIS INFORMATION TO YOUR BACKEND!</b>");
                //stringBuilder.AppendLine("<b>You can test this again by revoking credentials in Settings</b>");
                //stringBuilder.AppendLine("<b>Email:</b> " + appleIdCredential.Email);

                //Email.text = appleIdCredential.Email;
                _Email = appleIdCredential.Email;
                UI.instance.SetString("Email", _Email);
            }
            Debug.Log("FullName : " + appleIdCredential.FullName);
            if (appleIdCredential.FullName != null)
            {
                var fullName = appleIdCredential.FullName;
                //stringBuilder.AppendLine();
                //stringBuilder.AppendLine("<b>NAME RECEIVED: YOU WILL ONLY SEE THIS ONCE PER SIGN UP. SEND THIS INFORMATION TO YOUR BACKEND!</b>");
                //stringBuilder.AppendLine("<b>You can test this again by revoking credentials in Settings</b>");
                //stringBuilder.AppendLine("<b>Name:</b> " + fullName.ToLocalizedString());
                //stringBuilder.AppendLine("<b>Name (Short):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Short));
                //stringBuilder.AppendLine("<b>Name (Medium):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Medium));
                //stringBuilder.AppendLine("<b>Name (Long):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Long));
                //stringBuilder.AppendLine("<b>Name (Abbreviated):</b> " + fullName.ToLocalizedString(PersonNameFormatterStyle.Abbreviated));

                //UserName.text = "User Name : " + fullName.ToLocalizedString(PersonNameFormatterStyle.Long);
                _UserName = fullName.ToLocalizedString(PersonNameFormatterStyle.Long);

                UI.instance.SetString("UserName", _UserName);
                UI.instance.SetString("DisplayName", _UserName);

                if (appleIdCredential.FullName.PhoneticRepresentation != null)
                {
                    var phoneticName = appleIdCredential.FullName.PhoneticRepresentation;
                    //stringBuilder.AppendLine("<b>Phonetic name:</b> " + phoneticName.ToLocalizedString());
                    //stringBuilder.AppendLine("<b>Phonetic name (Short):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Short));
                    //stringBuilder.AppendLine("<b>Phonetic name (Medium):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Medium));
                    //stringBuilder.AppendLine("<b>Phonetic name (Long):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Long));
                    //stringBuilder.AppendLine("<b>Phonetic name (Abbreviated):</b> " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Abbreviated));

                    //UserName.text = "User Name : " + phoneticName.ToLocalizedString(PersonNameFormatterStyle.Long);
                    _UserName = phoneticName.ToLocalizedString(PersonNameFormatterStyle.Long);
                    UI.instance.SetString("UserName", _UserName);
                    UI.instance.SetString("DisplayName", _UserName);
                }
            }


            //this.AppleUserCredentialLabel.text = stringBuilder.ToString();
        }
        else if (passwordCredential != null)
        {
            //var stringBuilder = new StringBuilder();
            //stringBuilder.AppendLine("USERNAME/PASSWORD RECEIVED (iCloud?)");
            //stringBuilder.AppendLine("<b>Username:</b> " + passwordCredential.User);
            //stringBuilder.AppendLine("<b>Password:</b> " + passwordCredential.Password);
            //this.AppleUserCredentialLabel.text = stringBuilder.ToString();
            _password = "Password : " + passwordCredential.Password;
            _user = "User : " + passwordCredential.User;
        }
        else
        {
            //this.AppleUserCredentialLabel = "Unknown credentials for user " + receivedCredential.User;
        }


        Debug.Log(appleUserId);
        Debug.Log(appleIdCredential.FullName);
        Debug.Log(appleIdCredential.Email);
        Debug.Log(appleIdCredential.User);
        Debug.Log(appleIdCredential.RealUserStatus);
        Debug.Log("token " + _IdToken);

        PlayFabClientAPI.LoginWithApple(new LoginWithAppleRequest { CreateAccount = true, IdentityToken = _IdToken, TitleId = "E26E9",
            PlayerSecret = "B1HUZFT69WFWKGYKJWYR4WBCXRQR7PIUFQG516D1H8RJOIFKUP"},
            OnPlayfabAppleAuthComplete, OnPlayfabAppleAuthFailed);

    }
    private void OnPlayfabAppleAuthComplete(LoginResult result)
    {
        Debug.Log("PlayFab apple Auth Complete. Session ticket: " + result.SessionTicket);
        Debug.Log("PlayFab apple Auth Complete. Session ticket: " + result.PlayFabId);

        if (string.IsNullOrEmpty(_UserName))
        {
            _UserName = UI.instance.GetString("UserName");
        }
        if (string.IsNullOrEmpty(_Email))
        {
            _Email = UI.instance.GetString("Email");
        }
        if (string.IsNullOrEmpty(_Id))
        {
            _Id = UI.instance.GetString("AppleId");
        }
        if (!string.IsNullOrEmpty(_UserName))
        {
        Authentication.Instance.SetUserName(_UserName);

        }


        Authentication.Instance.loginthroughApple(_UserName, _Email, _Id);

        
    }

    private void OnPlayfabAppleAuthFailed(PlayFabError error)
    {
        Debug.Log("PlayFab apple Auth Failed: " + error.GenerateErrorReport() + true);
    }

    public void SetSignInWithAppleButton(bool visible, bool enabled)
        {
        //this.SignInWithAppleParent.SetActive(visible);
        Debug.Log("Here");
        this.SignInWithAppleButton.gameObject.SetActive(enabled);
        }


    
}
