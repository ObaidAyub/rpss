using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;


public class DiceScript : MonoBehaviourPunCallbacks
{
	public static DiceScript instance;

	public List<int> randomTurnList = new List<int>();
	public List<int> randomValue = new List<int>();
	public List<int> finalValue = new List<int>();
	public List<Transform> colourName = new List<Transform>();
	public Animator anim;
	public List<AnimationClip> animList = new List<AnimationClip>();
	public List<AnimationClip> animationList = new List<AnimationClip>();
	int randomNum;

	int HighestValue1st;
	int HighestValue2ND;
	int HighestValue3RD;
	int HighestValue4TH;

	public bool firstTime;

	// References
	public AssignTurns assignTurns;



	// Dice
	public Transform dice2;
	public Transform dice4;
	public Transform dice6;

	static Rigidbody rb;
	public Vector3 diceVelocity;
	public GameManager gm;
	// Bool
	bool randomTurn = true;

	public int number;
	public List<int> pawnNumber;

	int maxIndex;
	int maxIndex2;
	int maxIndex3;
	int maxIndex4;
	int maxValue;
	int maxValue2;
	int maxValue3;
	int maxValue4;

	public Text randomAnimText;

	private PhotonView PV;

	public GameObject Dicehost;

	//private ExitGames.Client.Photon.Hashtable _myCustomPropertiesTurn = new ExitGames.Client.Photon.Hashtable();

	private void Awake()
	{
		instance = this;
		PV = GetComponent<PhotonView>();
       
    }
	
	void Start()
	{
		
		PV.RPC("InitioalizeDice", RpcTarget.AllBuffered);
		


	}
	[PunRPC]
	public void InitioalizeDice()
	{
		animList.Add(Resources.Load<AnimationClip>("One"));
		animList.Add(Resources.Load<AnimationClip>("Four"));
		animList.Add(Resources.Load<AnimationClip>("Five"));
		animList.Add(Resources.Load<AnimationClip>("Six"));
		animList.Add(Resources.Load<AnimationClip>("Two1"));
		animList.Add(Resources.Load<AnimationClip>("Three1"));

		gm = FindObjectOfType<GameManager>();

		rb = GetComponent<Rigidbody>();
		float dirX = Random.Range(0, 500);
		float dirY = Random.Range(0, 500);
		float dirZ = Random.Range(0, 500);
		dice4.transform.position = new Vector3(-2.5f, 2, 3.9f);
		dice4.transform.rotation = Quaternion.identity;
		rb.AddForce(dice4.transform.up * 500);
		rb.AddTorque(dirX, dirY, dirZ);
		
		diceVelocity = rb.velocity;//Khudse
	}

    #region 2 player data
    public void Dicebtn2Player()
	{
		DiceNumberTextScript.instance.diceNumber = 0;
		float dirX = Random.Range(0, 500);
		float dirY = Random.Range(0, 500);
		float dirZ = Random.Range(0, 500);
		dice2.transform.position = new Vector3(-2.5f, 2, 3.9f);
		dice2.transform.rotation = Quaternion.identity;
		rb.AddForce(dice2.transform.up * 500);
		rb.AddTorque(dirX, dirY, dirZ);

	}

	IEnumerator w8GreenBtn()
	{
		yield return new WaitForSeconds(3f);
		assignTurns.green6Btn.interactable = true;
	}

	public void redBtn()
	{
		assignTurns.Board2TurnText.text = "Green Turn";
		assignTurns.red6Btn.interactable = false;
		StartCoroutine(w8GreenBtn());
		float dirX = Random.Range(0, 500);
		float dirY = Random.Range(0, 500);
		float dirZ = Random.Range(0, 500);
		dice6.transform.position = new Vector3(-2.5f, 2, 3.9f);
		dice6.transform.rotation = Quaternion.identity;
		rb.AddForce(dice6.transform.up * 500);
		rb.AddTorque(dirX, dirY, dirZ);



	}



	IEnumerator w8RedBtn()
	{
		yield return new WaitForSeconds(3f);
		assignTurns.red6Btn.interactable = true;
	}
	public void greenBtn()
	{
		assignTurns.Board2TurnText.text = "Red Turn";
		assignTurns.green6Btn.interactable = false;
		StartCoroutine(w8RedBtn());
		float dirX = Random.Range(0, 500);
		float dirY = Random.Range(0, 500);
		float dirZ = Random.Range(0, 500);
		dice6.transform.position = new Vector3(-2.5f, 2, 3.9f);
		dice6.transform.rotation = Quaternion.identity;
		rb.AddForce(dice6.transform.up * 500);
		rb.AddTorque(dirX, dirY, dirZ);
	}
    #endregion

    Vector3 redPlayerDicePosition = new Vector3(1438.68f, 882, -41.53f);

	Vector3 purplePlayerDicePosition = new Vector3(1438.68f, 882, -41.53f);
	
	Vector3 greenPlayerDicePosition = new Vector3(1438.68f, 882, -41.53f);

	Vector3 bluePlayerDicePosition = new Vector3(1438.68f, 882, -41.53f);

	private void DicePostionAsForPlayer()
    {
        switch (GameManager.instance.TeamColor)
        {
            case "Red":
                Dicehost.transform.position = redPlayerDicePosition;
                break;
            case "Purple":
                Dicehost.transform.position = purplePlayerDicePosition;
                break;
            case "Green":
                Dicehost.transform.position = greenPlayerDicePosition;
                break;
            case "Blue":
                Dicehost.transform.position = bluePlayerDicePosition;
                break;
        }
    }

	private void OnClickDiceRoll(int diceValue, Transform Pawn, Transform AssignTurnPawn, Button AssignTurnBtn)
	{
		DicePostionAsForPlayer();

		if (randomValue.Count == 3)
		{
			StartCoroutine(CheckCo());
		}
		dice6.gameObject.SetActive(true);
		anim = GetComponent<Animator>();
		Ran(diceValue);
		GameManager.instance.activePlayer = Pawn;

		colourName.Add(AssignTurnPawn);

		AssignTurnBtn.gameObject.SetActive(false);
	}

	public void Red4()
    {
        var diceValue = RandomAnim();

        Dicehost.transform.position = redPlayerDicePosition;
        DiceCheckZoneScript.instance.diceStay = true;
        assignTurns.myTurn = true;

        OnClickDiceRoll(diceValue, GameManager.instance.red4Pawn, assignTurns.redPawn.transform, assignTurns.red4Button);
		if (GameManager.modeType == GameManager.Mode.TwoPlayers)
		{
			StartCoroutine(w8GreenBtn4());
		}
        else
        {
			StartCoroutine(w8PurpleBtn4());

        }

		PV.RPC("red4Btn", RpcTarget.AllBuffered, diceValue , "Red");
    }
	[PunRPC]
	public void red4Btn(int ran, string Name)
	{
		if (GameManager.instance.TeamColor == Name)
		{
			return;
		}
		OnClickDiceRoll(ran, GameManager.instance.red4Pawn, assignTurns.redPawn.transform, assignTurns.red4Button);

		if (GameManager.modeType == GameManager.Mode.TwoPlayers)
		{
			StartCoroutine(w8GreenBtn4());
		}
		else
		{
			StartCoroutine(w8PurpleBtn4());

		}
	}

	public void Purple4()
	{

		var diceValue = RandomAnim();

		Dicehost.transform.position = purplePlayerDicePosition;
		DiceCheckZoneScript.instance.diceStay = true;
		assignTurns.myTurn = true;

		OnClickDiceRoll(diceValue, GameManager.instance.purple4Pawn, assignTurns.purplePawn.transform, assignTurns.purple4Button);

		StartCoroutine(w8GreenBtn4());

		PV.RPC("purple4Btn", RpcTarget.AllBuffered, diceValue,"Purple");
	}
	[PunRPC]
	public void purple4Btn(int ran, string Name)
	{
		if (GameManager.instance.TeamColor == Name)
		{
			return;
		}
		OnClickDiceRoll(ran, GameManager.instance.purple4Pawn, assignTurns.purplePawn.transform, assignTurns.purple4Button);

		StartCoroutine(w8GreenBtn4());
	}

	public void Green4()
	{
		var diceValue = RandomAnim();

		Dicehost.transform.position = greenPlayerDicePosition;
		DiceCheckZoneScript.instance.diceStay = true;
		assignTurns.myTurn = true;

		OnClickDiceRoll(diceValue, GameManager.instance.green4Pawn, assignTurns.greenPawn.transform, assignTurns.green4Button);

		if (GameManager.modeType == GameManager.Mode.TwoPlayers)
		{
			StartCoroutine(w8RedBtn4());
		}
		else
		{
			StartCoroutine(w8BlueBtn4());

		}
		

		PV.RPC("green4Btn", RpcTarget.AllBuffered, diceValue, "Green");
	}
	[PunRPC]
	public void green4Btn(int ran, string Name)
	{
		if (GameManager.instance.TeamColor == Name)
		{
			return;
		}
		OnClickDiceRoll(ran, GameManager.instance.green4Pawn, assignTurns.greenPawn.transform, assignTurns.green4Button);

		if (GameManager.modeType == GameManager.Mode.TwoPlayers)
		{
			StartCoroutine(w8RedBtn4());
		}
		else
		{
			StartCoroutine(w8BlueBtn4());

		}
	}
	public void Blue4()
	{
		var diceValue = RandomAnim();

		Dicehost.transform.position = bluePlayerDicePosition;
		DiceCheckZoneScript.instance.diceStay = true;
		assignTurns.myTurn = true;

		OnClickDiceRoll(diceValue, GameManager.instance.blue4Pawn, assignTurns.bluePawn.transform, assignTurns.blue4Button);

		StartCoroutine(w8RedBtn4());

		PV.RPC("blue4Btn", RpcTarget.AllBuffered, diceValue, "Blue");
	}
	[PunRPC]
	public void blue4Btn(int ran, string Name)
	{
		if (GameManager.instance.TeamColor == Name)
		{
			return;
		}
		OnClickDiceRoll(ran, GameManager.instance.blue4Pawn, assignTurns.bluePawn.transform, assignTurns.blue4Button);

		StartCoroutine(w8RedBtn4());
	}

	public Text color;

	IEnumerator w8RedBtn4()
	{
		string col = (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
		yield return new WaitForSeconds(2f);
		number = DiceNumberTextScript.instance.diceNumber;

		if (GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailed && (number == 6 || number == 1))
		{
			Unjailed();
		}

		yield return new WaitForSeconds(2f);
		if (col == "Red")
		{
			assignTurns.Board4TurnText.text = "Your Turn";
			DicePostionAsForPlayer();
			assignTurns.red4Button.gameObject.SetActive(true);
		}
		else
		{
			assignTurns.Board4TurnText.text = "Red Turn";
			DicePostionAsForPlayer();
		}

	}
	IEnumerator w8PurpleBtn4()
	{
		string col = (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
		yield return new WaitForSeconds(2f);
		number = DiceNumberTextScript.instance.diceNumber;
		if (GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailed && (number == 6 || number == 1))
		{
			Unjailed();
		}

		yield return new WaitForSeconds(2f);
		if (col == "Purple")
		{
			assignTurns.Board4TurnText.text = "Your Turn";
			assignTurns.purple4Button.gameObject.SetActive(true);
			DicePostionAsForPlayer();
		}
		else
		{
			assignTurns.Board4TurnText.text = "Purple Turn";
			DicePostionAsForPlayer();
		}


	}
	IEnumerator w8GreenBtn4()
	{
		string col = (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
		yield return new WaitForSeconds(2f);
		number = DiceNumberTextScript.instance.diceNumber;
		if (GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailed && (number == 6 || number == 1))
		{
			Unjailed();
		}

		yield return new WaitForSeconds(2f);
		if (col == "Green")
		{
			assignTurns.Board4TurnText.text = "Your Turn";
			assignTurns.green4Button.gameObject.SetActive(true);
			DicePostionAsForPlayer();
		}
		else
		{
			assignTurns.Board4TurnText.text = "Green Turn";
			DicePostionAsForPlayer();
		}
	}
	IEnumerator w8BlueBtn4()
	{
		string col = (string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
		yield return new WaitForSeconds(2f);
		number = DiceNumberTextScript.instance.diceNumber;
		if (GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailed && (number == 6 || number == 1))
		{
			Unjailed();
		}

		yield return new WaitForSeconds(2f);
		if (col == "Blue")
		{
			assignTurns.Board4TurnText.text = "Your Turn";
			assignTurns.blue4Button.gameObject.SetActive(true);
			DicePostionAsForPlayer();
		}
		else
		{
			assignTurns.Board4TurnText.text = "Blue Turn";
			DicePostionAsForPlayer();
		}

	}
	public int RandomAnim()
	{
		randomNum = Random.Range(1, 7);
		return randomNum;
	}
	[PunRPC]
	public void Ran(int ran)
	{
		if (firstTime)
        {
			randomTurnList.Add(ran);
        }

		anim.Play(ran.ToString());
	}

	public IEnumerator CheckCo()
	{
		yield return new WaitForSeconds(3);
		StartCoroutine(Complet());

	}

	public IEnumerator Complet()
	{
		yield return new WaitForSeconds(3);

		finalValue = new List<int>(randomValue);


	}
	public void Unjailed()
	{
		GameManager.instance.activePlayer.gameObject.SetActive(true);
		GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailed = false;
		GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().onImaginary = false;
		GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().jailedImage.enabled = false;
		GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().routePositionImaginary = 0;
		GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().routePosition = 0;
		//GameManager.instance.activePlayer.gameObject.GetComponent<Stone>().doubleTurn = true;
	}
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(this.transform.position);
			stream.SendNext(this.transform.rotation);


		}
		else
		{
			this.transform.position = (Vector3)stream.ReceiveNext();
			this.transform.rotation = (Quaternion)stream.ReceiveNext();

		}
	}
}
