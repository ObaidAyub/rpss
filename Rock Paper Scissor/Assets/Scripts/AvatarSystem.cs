using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Avatars
{
    public int faceIndex;
    public int lipsIndex;
    public int glassesIndex;
    public int eyesIndex;
    public int genderIndex;
    public Avatars(int faceIndex, int lipsIndex, int glassesIndex, int eyesIndex, int genderIndex)
    {
        this.faceIndex = faceIndex;
        this.lipsIndex = lipsIndex;
        this.glassesIndex = glassesIndex;
        this.eyesIndex = eyesIndex;
        this.genderIndex = genderIndex;
    }
}
public class AvatarSystem : MonoBehaviour
{
    public static AvatarSystem instance;
    public List<GameObject> selectionHolder;
    public List<Sprite> faceImages;
    public List<Sprite> faceImagesFemale;
    public List<Sprite> eyeImages;
    public List<Sprite> lipsImages;
    public List<Sprite> glassesImages;
    public List<GameObject> AvatarHolder;

    [Header("*******Specs indices*******")]
    public int baseIndexFace = 0;
    public int baseIndexLips = 0;
    public int baseIndexGlasses = 0;
    public int baseIndexEye = 0;
    public int baseIndexGender = 0;


    public enum AvatarGender
    {
        Male,
        Female
    }
    public AvatarGender Gender;
    public Character activeGender;
    public Character profileAvatar;
    public Character settingAvatar;
    public Character userprofileAvatar;
    public enum SpecSelected
    {
        Face,
        Lips,
        Eyes,
        Glasses
    }
    public SpecSelected SelectedSpec;

    public void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {
        //FindObjectOfType<PlayFabManager>().GetAvatar();
        if (Gender == AvatarGender.Male)
        {
            SelectGender(0);
        }
        else
        {
            SelectGender(1);

        }
    }

    public void FaceButton()
    {
        SoundManager.PlaySound("btnSound");
        for (int i = 0; i < selectionHolder.Count; i++)
        {
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 206.2933f);
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 177.2834f);
            if (Gender == AvatarGender.Male)
                selectionHolder[i].GetComponent<Image>().sprite = faceImages[i];
            else
                selectionHolder[i].GetComponent<Image>().sprite = faceImagesFemale[i];

        }
        SelectedSpec = SpecSelected.Face;
    }
    public void EyeButton()
    {
        SoundManager.PlaySound("btnSound");
        for (int i = 0; i < selectionHolder.Count; i++)
        {
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 154.7931f);
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 132.2299f);
            selectionHolder[i].GetComponent<Image>().sprite = eyeImages[i];
        }
        SelectedSpec = SpecSelected.Eyes;
    }
    public void LipsButton()
    {
        SoundManager.PlaySound("btnSound");
        for (int i = 0; i < selectionHolder.Count; i++)
        {
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 110.3964f);
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 45.0417f);
            //selectionHolder[i].GetComponent<RectTransform>().sizeDelta= new Vector2(154.7931f, 132.2299f);
            selectionHolder[i].GetComponent<Image>().sprite = lipsImages[i];
        }
        SelectedSpec = SpecSelected.Lips;
    }
    public void GlassesButton()
    {
        SoundManager.PlaySound("btnSound");
        for (int i = 0; i < selectionHolder.Count; i++)
        {
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 154.7931f);
            selectionHolder[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 132.2299f);
            selectionHolder[i].GetComponent<Image>().sprite = glassesImages[i];
        }
        SelectedSpec = SpecSelected.Glasses;
    }
    public void SelectGender(int index)
    {
        SoundManager.PlaySound("btnSound");
        baseIndexGender = index;
        if (index == 0)
        {
            Gender = AvatarGender.Male;
            AvatarHolder[0].SetActive(true);
            AvatarHolder[1].SetActive(false);
            activeGender = AvatarHolder[0].GetComponent<Character>();

        }
        else
        {
            Gender = AvatarGender.Female;
            AvatarHolder[0].SetActive(false);
            AvatarHolder[1].SetActive(true);
            activeGender = AvatarHolder[1].GetComponent<Character>();
        }
        if (SelectedSpec == SpecSelected.Face)
            FaceButton();
        if (SelectedSpec == SpecSelected.Eyes)
            EyeButton();
        if (SelectedSpec == SpecSelected.Lips)
            LipsButton();
        if (SelectedSpec == SpecSelected.Glasses)
            GlassesButton();
    }
    public void SelectSpec(int index)
    {
        if (SelectedSpec == SpecSelected.Face)
        {
            if (Gender == AvatarGender.Male)
            {
                activeGender.GetComponent<Image>().sprite = faceImages[index];
            }
            else
            {
                activeGender.GetComponent<Image>().sprite = faceImagesFemale[index];
            }
            baseIndexFace = index;
        }
        if (SelectedSpec == SpecSelected.Lips)
        {
            if (Gender == AvatarGender.Male)
            {
                activeGender.lips.sprite = lipsImages[index];
            }
            else
            {
                activeGender.lips.sprite = lipsImages[index];
            }
            baseIndexLips = index;
        }
        if (SelectedSpec == SpecSelected.Eyes)
        {
            if (Gender == AvatarGender.Male)
            {
                activeGender.eye.sprite = eyeImages[index];
            }
            else
            {
                activeGender.eye.sprite = eyeImages[index];
            }
            baseIndexEye = index;
        }
        if (SelectedSpec == SpecSelected.Glasses)
        {

            activeGender.glasses.sprite = glassesImages[index];
            baseIndexGlasses = index;
        }
    }

    public Avatars ReturnClass()
    {
        //Debug.LogError(new Avatar(baseIndexFace, baseIndexLips, baseIndexGlasses));
        return new Avatars(baseIndexFace, baseIndexLips, baseIndexGlasses, baseIndexEye, baseIndexGender);
    }
    public void SetAvatarBoard(Avatars avatar)
    {
        for (int i = 0; i < 3; i++)
        {
            
            if (avatar.genderIndex == 1)
               FindObjectOfType<PlayFabManager>().characterBoard[i].GetComponent<Image>().sprite = faceImagesFemale[avatar.faceIndex];
            else
                FindObjectOfType<PlayFabManager>().characterBoard[i].GetComponent<Image>().sprite = faceImages[avatar.faceIndex];

            FindObjectOfType<PlayFabManager>().characterBoard[i].lips.sprite = lipsImages[avatar.lipsIndex];
            FindObjectOfType<PlayFabManager>().characterBoard[i].glasses.sprite = glassesImages[avatar.glassesIndex];
            FindObjectOfType<PlayFabManager>().characterBoard[i].eye.sprite = eyeImages[avatar.eyesIndex];
        }
    }
    public void SetAvatar(string avatar, string playerColor="", string playerName ="")
    {

        if (string.IsNullOrEmpty(avatar))
        {
            return;
        }

        Avatars avatardata = JsonConvert.DeserializeObject<Avatars>(avatar);

        if (avatardata.genderIndex == 0)
        {
            Gender = AvatarGender.Male;
        }
        else
            Gender = AvatarGender.Female;

        if (SceneManager.GetActiveScene().buildIndex == 0)
        {

            if (Gender == AvatarGender.Male)
            {
                profileAvatar.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                settingAvatar.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                userprofileAvatar.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
            }
            else
            {
                profileAvatar.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                settingAvatar.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                userprofileAvatar.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
            }

            profileAvatar.lips.sprite = lipsImages[avatardata.lipsIndex];
            profileAvatar.glasses.sprite = glassesImages[avatardata.glassesIndex];
            profileAvatar.eye.sprite = eyeImages[avatardata.eyesIndex];

            settingAvatar.lips.sprite = lipsImages[avatardata.lipsIndex];
            settingAvatar.glasses.sprite = glassesImages[avatardata.glassesIndex];
            settingAvatar.eye.sprite = eyeImages[avatardata.eyesIndex];

            userprofileAvatar.lips.sprite = lipsImages[avatardata.lipsIndex];
            userprofileAvatar.glasses.sprite = glassesImages[avatardata.glassesIndex];
            userprofileAvatar.eye.sprite = eyeImages[avatardata.eyesIndex];




            loadingPanel.instance.HideLoader();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            switch (playerColor)
            {
                case "Red":
                    if (Gender == AvatarGender.Male)
                    {
                        GameManager.instance.redPlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                        GameManager.instance.redPlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                    }   
                    else
                    {
                        GameManager.instance.redPlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                        GameManager.instance.redPlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                    }
                    GameManager.instance.redPlayer.lips.sprite = lipsImages[avatardata.lipsIndex];
                    GameManager.instance.redPlayer.glasses.sprite = glassesImages[avatardata.glassesIndex];
                    GameManager.instance.redPlayer.eye.sprite = eyeImages[avatardata.eyesIndex];

                    GameManager.instance.redPlayerName.text = playerName;

                    
                    break;
                case "Blue":
                    if (Gender == AvatarGender.Male)
                    {
                        GameManager.instance.bluePlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                        GameManager.instance.bluePlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                    }
                    else
                    {
                        GameManager.instance.bluePlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                        GameManager.instance.bluePlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                    }
                    GameManager.instance.bluePlayer.lips.sprite = lipsImages[avatardata.lipsIndex];
                    GameManager.instance.bluePlayer.glasses.sprite = glassesImages[avatardata.glassesIndex];
                    GameManager.instance.bluePlayer.eye.sprite = eyeImages[avatardata.eyesIndex];

                    GameManager.instance.bluePlayerName.text = playerName;
                    break;
                case "Green":
                    if (Gender == AvatarGender.Male)
                    {
                        GameManager.instance.greenPlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                        GameManager.instance.greenPlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                    }
                    else
                    {
                        GameManager.instance.greenPlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                        GameManager.instance.greenPlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                    }
                    GameManager.instance.greenPlayer.lips.sprite = lipsImages[avatardata.lipsIndex];
                    GameManager.instance.greenPlayer.glasses.sprite = glassesImages[avatardata.glassesIndex];
                    GameManager.instance.greenPlayer.eye.sprite = eyeImages[avatardata.eyesIndex];

                    GameManager.instance.greenPlayerName.text = playerName;
                    break;
                case "Purple":
                    if (Gender == AvatarGender.Male)
                    {
                        GameManager.instance.purplePlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                        GameManager.instance.purplePlayer.GetComponent<Image>().sprite = faceImages[avatardata.faceIndex];
                    }
                    else
                    {
                        GameManager.instance.purplePlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                        GameManager.instance.purplePlayer.GetComponent<Image>().sprite = faceImagesFemale[avatardata.faceIndex];
                    }
                    GameManager.instance.purplePlayer.lips.sprite = lipsImages[avatardata.lipsIndex];
                    GameManager.instance.purplePlayer.glasses.sprite = glassesImages[avatardata.glassesIndex];
                    GameManager.instance.purplePlayer.eye.sprite = eyeImages[avatardata.eyesIndex];

                    GameManager.instance.purplePlayerName.text = playerName;
                    break;
            }
            Debug.LogError(playerColor);
        }
    }
}
