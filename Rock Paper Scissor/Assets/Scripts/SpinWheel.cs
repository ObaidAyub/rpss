﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class SpinWheel : MonoBehaviour
{
	public List<int> prize;
	public List<AnimationCurve> animationCurves;

	private bool spinning;
	private float anglePerItem;
	private int randomTime;
    int itemNumber;
	public Text rewardText;
	public GameObject rewardPanel;




	void Start()
	{
		spinning = false;
		anglePerItem = 360 / prize.Count;
	}

	public void spinButton()
	{
		if (!spinning)
		{
			randomTime = Random.Range(1, 4);
			itemNumber = Random.Range(0, prize.Count);
			//if (itemNumber % 2 == 0)
			//{
			//	itemNumber += 1;
			//}
			float maxAngle = 360 * randomTime + (itemNumber * anglePerItem);
			StartCoroutine(SpinTheWheel(5 * randomTime, maxAngle));
		}

	}
	public void RewardPopup(string reward, int score)
	{
		rewardText.text = score.ToString() + " " + reward;
		rewardPanel.SetActive(true);

		switch (reward)
		{
			case "SafeCard":
				int safecards = UI.instance.Getint("SafeCard") + score;
				UI.instance.SetInt("SafeCard", safecards);
				UI.instance.SafeCard = UI.instance.Getint("SafeCard");

				Authentication.Instance.SaveAppearence("Score", UI.instance.Getint("Score"),
					"SafeCard", safecards,
					"Win", UI.instance.Getint("Win"),
					"Lose", UI.instance.Getint("Lose"),
					"_date", UI.instance.Getint("_date"),
			"Hours", UI.instance.Getint("hours"),
			"Minutes", UI.instance.Getint("minutes"),
			"Seconds", UI.instance.Getint("seconds"));

				UI.instance.UpdateScore();
				PlayFabManager.instance.SendLeaderboard(UI.instance.Score);

				break;
			case "Points":
				int Score = UI.instance.Getint("Score") + score;
				UI.instance.SetInt("Score", Score);
				UI.instance.Score = UI.instance.Getint("Score");

				Authentication.Instance.SaveAppearence("Score", Score,
					"SafeCard", UI.instance.Getint("SafeCard"),
					"Win", UI.instance.Getint("Win"),
					"Lose", UI.instance.Getint("Lose"),
					"_date", UI.instance.Getint("_date"),
			"Hours", UI.instance.Getint("hours"),
			"Minutes", UI.instance.Getint("minutes"),
			"Seconds", UI.instance.Getint("seconds"));

				UI.instance.UpdateScore();

				break;
		}

	}

	IEnumerator SpinTheWheel(float time, float maxAngle)
	{
		Debug.Log(time + " max angle " + maxAngle);
		spinning = true;

		float timer = 0.0f;
		float startAngle = transform.eulerAngles.z;
		maxAngle = maxAngle - startAngle;

		int animationCurveNumber = Random.Range(0, animationCurves.Count);
		Debug.Log("Animation Curve No. : " + animationCurveNumber);

		while (timer < time)
		{
			//to calculate rotation
			float angle = maxAngle * animationCurves[animationCurveNumber].Evaluate(timer / time);
			transform.eulerAngles = new Vector3(0.0f, 0.0f, angle + startAngle);
			timer += Time.deltaTime;
			yield return 0;
		}

		transform.eulerAngles = new Vector3(0.0f, 0.0f, maxAngle + startAngle);
		spinning = false;
		switch (prize[itemNumber])
		{
			case 1:
				RewardPopup("Points", 300);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 2:
				RewardPopup("Points", 500);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);
				break;
			case 3:
				RewardPopup("Points", 150);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 4:
				RewardPopup("Points", 100);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 5:
				RewardPopup("Points", 400);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 6:
				RewardPopup("Points", 50);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 7:
				RewardPopup("SafeCard", 1);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
			case 8:
				RewardPopup("Points", 250);
				GameAnimations.instance.DailyRewardAnimation(false, rewardPanel);

				break;
		}
	}
}