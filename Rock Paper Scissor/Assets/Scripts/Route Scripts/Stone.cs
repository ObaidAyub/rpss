using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using DG.Tweening;
using PlayFab;
using PlayFab.ClientModels;
using System.Runtime.InteropServices.WindowsRuntime;

public class Stone : MonoBehaviourPunCallbacks
{
	public static Stone instance;
	//public UI ui;
	//public Authentication authentication;
    public int steps;
	public Route currentRoot;
	public int routePosition = 0;
	public int routePositionImaginary = 0;
	public bool isMoving = false;
	public bool allowMove;
	public bool onImaginary;
	public bool reachedCenter;//Abrar
	//public bool doubleTurn;
	public SpriteRenderer[] arrowSprites;//Abrar
	public Transform collidedPawn;
	public Sprite pawnImage;
	public Image jailedImage;
	public string AnimName;
	public AnimationClip AnimClip;
	
	public List<GameObject> safePawn;
	public int safeReached = 0;
	private PhotonView PV;
	public int numNum = 0;
	public enum ChoosenRPS
	{
		None,
		Rock,
		Paper,
		Scissor
	}

	public ChoosenRPS selectedRPS;
	public bool jailed = false;
	private void Awake()
	{
		instance = this;
		PV = GetComponent<PhotonView>();
		

	}
	private void Start()
	{
	}
	public bool pathSeelcted;
	private void Update()
	{
		if (!isMoving)
		{
			if (allowMove)
			{
				steps = DiceScript.instance.number;
				var nextStep = routePosition + DiceScript.instance.number;
				if ((DiceScript.instance.number == 6 || DiceScript.instance.number == 1) && jailed)
				{
					jailed = false;
					onImaginary = false;
					jailedImage.enabled = false;
					routePositionImaginary = 0;
					routePosition = 0;
				}
				if (nextStep > 7 && !pathSeelcted)
				{
					reachedCenter = false;
					int randomno = RandomSprite();
					allowMove = false;

					if (GameManager.modeType == GameManager.Mode.TwoPlayers)
					{
						switch (GameManager.instance.TeamColor)
						{
							case "Red":
								{
									PathMoveIndication(2);
								}
								break;
							case "Green":
								{
									PathMoveIndication(0);
								}
								break;
						}
                    }
                    else
                    {

					PathSelection();
                    }
				}
				else
				{
					

					if ((nextStep < 16 && !jailed))
					{

						StartCoroutine(Move());
					}
					else
					{
						int a = 15 - routePosition;
						Debug.Log(a);
						int b = steps - a;
						Debug.Log(b);
						steps = steps - b;
						Debug.Log(steps);
						StartCoroutine(Move());
					}
				}

			}

		}
	}

	private void OnTriggerEnter(Collider otherPlayer)//Abrar
	{
		Debug.Log("on Trigger for RPS panel ");

		if (GameManager.instance.activePlayer.gameObject.name == "RedPawn" && this.gameObject.CompareTag("Red") && otherPlayer.gameObject.layer == 6)
		{
			
			SetRPS(this.gameObject, otherPlayer.gameObject);
			PV.RPC("RPSTrigger", RpcTarget.AllBuffered, GameManager.instance.activePlayer.gameObject.name, otherPlayer.name);
			
		}
		if (GameManager.instance.activePlayer.gameObject.name == "BluePawn" && this.gameObject.CompareTag("Blue") && otherPlayer.gameObject.layer == 6)
		{
			SetRPS(this.gameObject, otherPlayer.gameObject);
			PV.RPC("RPSTrigger", RpcTarget.AllBuffered, GameManager.instance.activePlayer.gameObject.name, otherPlayer.name);

		}
		if (GameManager.instance.activePlayer.gameObject.name == "GreenPawn" && this.gameObject.CompareTag("Green") && otherPlayer.gameObject.layer == 6)
		{
			SetRPS(this.gameObject, otherPlayer.gameObject);
			PV.RPC("RPSTrigger", RpcTarget.AllBuffered, GameManager.instance.activePlayer.gameObject.name, otherPlayer.name);

		}
		if (GameManager.instance.activePlayer.gameObject.name == "PurplePawn" && this.gameObject.CompareTag("Purple") && otherPlayer.gameObject.layer == 6)
		{
			SetRPS(this.gameObject, otherPlayer.gameObject);
			PV.RPC("RPSTrigger", RpcTarget.AllBuffered, GameManager.instance.activePlayer.gameObject.name, otherPlayer.name);

		}




	}
	[PunRPC]
    public void RPSTrigger(string Player,string otherPlayer)
	{
		Debug.Log("Active Player " + Player + " other Player " + otherPlayer + " Come Here");



            Debug.Log(otherPlayer);
            switch (otherPlayer)
            {
				case "RedPawn":
					SetRPS(GameManager.instance.activePlayer.gameObject, GameManager.instance.red4Pawn.gameObject);
					break;
				case "PurplePawn":
					SetRPS(GameManager.instance.activePlayer.gameObject, GameManager.instance.purple4Pawn.gameObject);
					break;
				case "GreenPawn":
					SetRPS(GameManager.instance.activePlayer.gameObject, GameManager.instance.green4Pawn.gameObject);
					break;
				case "BluePawn":
					SetRPS(GameManager.instance.activePlayer.gameObject, GameManager.instance.blue4Pawn.gameObject);
					break;

			}

    }


	public void SetRPS(GameObject Player, GameObject Oppnent)
	{
		Debug.Log("Player :" + Player.name + "Player :" + Oppnent.name);

        collidedPawn = Oppnent.transform;
		Oppnent.GetComponent<Stone>().collidedPawn = this.gameObject.transform;
		GameManager.instance.RPS_PanelActivation(Player, Oppnent);
		//GameManager.instance.firstTurn = Player.GetComponent<Stone>();
		//GameManager.instance.secondTurn = Oppnent.GetComponent<Stone>();
	}



	public IEnumerator Move()
	{
		if (isMoving)
		{

			yield break;
		}
		isMoving = true;


		while (steps > 0)
		{
			routePosition++;
			routePosition %= currentRoot.childNodesList.Count;
			Vector3 nextPos = currentRoot.childNodesList[routePosition].position;

			Vector3 pawnYPos = currentRoot.childNodesList[routePosition].position;
			pawnYPos.y = 0.2f;

			nextPos = new Vector3(currentRoot.childNodesList[routePosition].position.x, pawnYPos.y, currentRoot.childNodesList[routePosition].position.z); // Saad

			while (MoveToNextNode(nextPos))
			{
				yield return null;
			}


			//yield return new WaitForSeconds(0.1f);
			steps--;
		}
		isMoving = false;
		allowMove = false;
		if (routePosition == 15)
		{

			CheckForWin();

		}
	}
	public void CheckForWin()
	{
		safeReached++;
		if (safeReached == 4)
		{
			Debug.Log(AnimName + " == " + GameManager.instance.TeamColor);
			if (AnimName == GameManager.instance.TeamColor)
			{

				GameManager.instance.ShowFinalGameScreen(this.gameObject, true);
			}
			else
			{
				GameManager.instance.ShowFinalGameScreen(this.gameObject, false);
			}
		}
		else
		{
			for (int i = 0; i < 8; i++)
			{
				currentRoot.GetComponent<Route>().childNodesList.RemoveAt(8);
			}
			for (int i = 0; i < safeReached; i++)
			{
				safePawn[i].SetActive(false);

			}
			this.gameObject.transform.position = this.currentRoot.childNodesList[0].position;
			routePosition = 0;
			pathSeelcted = false;

			/// show the points rewads
			ShowPointScreen();
		}
	}
	public void ShowPointScreen()
    {
		if (GameManager.instance.TeamColor == "Red" && GameManager.instance.activePlayer.name == "RedPawn")
		{
			PopulatePointandSavetoServer(25);
		}
		if (GameManager.instance.TeamColor == "Purple" && GameManager.instance.activePlayer.name == "PurplePawn")
		{
			PopulatePointandSavetoServer(25);

		}
		if (GameManager.instance.TeamColor == "Green" && GameManager.instance.activePlayer.name == "GreenPawn")
		{
			PopulatePointandSavetoServer(25);

		}
		if (GameManager.instance.TeamColor == "Blue" && GameManager.instance.activePlayer.name == "BluePawn")
		{
			PopulatePointandSavetoServer(25);

		}
	}
	
	public void PopulatePointandSavetoServer(int ScoreValue)
    {
		GameManager.instance.openRewardPanel(ScoreValue, "You successfully reach the safezone and got ");
		//send request to server for point update

		int score = UI.instance.Getint("Score");
		int safeCard = UI.instance.Getint("SafeCard");
		int win = UI.instance.Getint("Win");
		int lose = UI.instance.Getint("Lose");
		int _date = UI.instance.Getint("_date");
		int hoursValue = UI.instance.Getint("hours");
		int minutesValue = UI.instance.Getint("minutes");
		int secondsValue = UI.instance.Getint("seconds");

		var request = new UpdateUserDataRequest
		{
			Data = new Dictionary<string, string>
			{

				{ "Score", score.ToString() },
				{ "SafeCard", safeCard.ToString() },
				{ "Win", win.ToString() },
				{ "Lose", lose.ToString()},
				{ "Date", _date.ToString()},
				{ "Hours", hoursValue.ToString()},
				{ "Minutes", minutesValue.ToString()},
				{ "Seconds", secondsValue.ToString()}

			}
		};

		PlayFabClientAPI.UpdateUserData(request, OnDataSend, OnError);
	}
	void OnDataSend(UpdateUserDataResult result)
	{
		Debug.Log(" Successful User Data Send ! ");
	}
	void OnError(PlayFabError Error)
	{
		Debug.Log(" Error  User Data not Send ! ");
	}

		public bool Chalao()
	{
		if (routePosition == 7)
		{
			return true;
		}
		else
			return false;
	}
	public int RandomSprite()
	{

		int ran = Random.Range(0, arrowSprites.Length);
		return ran;

	}
	public int indexNumber = -1;
	public int IncrementIndex()
	{
		indexNumber++;
		if (indexNumber == 3)
		{
			indexNumber = 0;
		}
		return indexNumber;
	}
	//[PunRPC]
	public void PathSelection()//Abrar
	{
		Debug.Log("path Selection");
        if (GameManager.instance.TeamColor =="Red" && GameManager.instance.activePlayer.name == "RedPawn")
        {
			GameManager.instance.PathSelectionScreen.SetActive(true);

		}
		if (GameManager.instance.TeamColor == "Purple" && GameManager.instance.activePlayer.name == "PurplePawn")
		{
			GameManager.instance.PathSelectionScreen.SetActive(true);

		}
		if (GameManager.instance.TeamColor == "Green" && GameManager.instance.activePlayer.name == "GreenPawn")
		{
			GameManager.instance.PathSelectionScreen.SetActive(true);

		}
		if (GameManager.instance.TeamColor == "Blue" && GameManager.instance.activePlayer.name == "BluePawn")
		{
			GameManager.instance.PathSelectionScreen.SetActive(true);

		}

		switch (GameManager.instance.TeamColor)
        {
			case "Red":
				GameManager.instance.RedArrow.gameObject.SetActive(false);
				break;
			case "Purple":
				GameManager.instance.PurpleArrow.gameObject.SetActive(false);
				break;
			case "Green":
				GameManager.instance.GreenArrow.gameObject.SetActive(false);
				break;
			case "Blue":
				GameManager.instance.BlueArrow.gameObject.SetActive(false);
				break;
		}


    }
	public void PathMoveIndication(int ran)
    {

		
        Indication(ran);
        
       
        PV.RPC("MoveIndication", RpcTarget.AllBuffered, ran, GameManager.instance.TeamColor);
    }

    private void Indication(int ran)
    {
        GameManager.instance.PathSelectionScreen.SetActive(false);

        DiceScript.instance.color.text = "  " + ran.ToString();
        int spriteIndex = ran;
        arrowSprites[spriteIndex].gameObject.SetActive(true);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[6]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[5]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[4]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[3]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[2]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[1]);
		currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().imaginaryNodeList[0]);

        currentRoot.GetComponent<Route>().childNodesList.Add(arrowSprites[spriteIndex].gameObject.GetComponentInParent<Route>().safeNode);
        pathSeelcted = true;
        allowMove = true;
    }
    [PunRPC]
	public void MoveIndication(int ran, string Name)
	{
        if (GameManager.instance.TeamColor == Name)
        {
			Debug.Log("return " + Name);
			return;
        }
		Indication(ran);
	}
	bool MoveToNextNode(Vector3 goal)
	{
		return goal != (transform.position = Vector3.MoveTowards(transform.position, goal, 8f * Time.deltaTime));
	}
	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.IsWriting)
		{
			stream.SendNext(this.transform.position);
			stream.SendNext(this.transform.rotation);
		}
		else
		{
			this.transform.position = (Vector3)stream.ReceiveNext();
			this.transform.rotation = (Quaternion)stream.ReceiveNext();
		}
	}
}
