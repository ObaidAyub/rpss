﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class CreateRoomMenu : MonoBehaviourPunCallbacks
{

    public static CreateRoomMenu instance;
    public Text privateRoomName;
    [SerializeField]
    private Text _roomName;

    public string roomNameText;

    private RommsCanvas _roomCanvases;
    public int playerNum;
    private void Awake()
    {
        instance = this;
    }
    public void FirstInitialize(RommsCanvas canvases)
    {
        _roomCanvases = canvases;
    }


    public static bool privateRoom;
    public GameObject privateShareBtn;
    public void OnClick_PrivateRoom()
    {
        SoundManager.PlaySound("btnSound");
        loadingPanel.instance.ShowLoader("Private Room Creating...");
        privateRoom = true;
        
        Debug.Log("true");

        if (!PhotonNetwork.IsConnected)
        {
            return;
        }
        //Create Room
        // Join Or Create Room
        //_roomName.text = "working";


        RoomOptions options = new RoomOptions();
        options.BroadcastPropsChangeToAll = true;

        playerNum = PlayFabManager.instance.playerNum;

        options.MaxPlayers = ((byte)PlayFabManager.instance.playerNum);
        MultiplayerGameController.instance.playerNum = options.MaxPlayers;
        PhotonNetwork.CreateRoom(_roomName.text.ToLower(), options, TypedLobby.Default);
        roomNameText = _roomName.text.ToLower();
        MultiplayerGameController.instance.roomName = _roomName.text.ToLower();
        UI.instance.RoomName = _roomName.text.ToLower();
        //PhotonNetwork.JoinOrCreateRoom(_roomName.text, options, TypedLobby.Default);
        // PhotonNetwork.CurrentRoom.IsVisible = false;
        CreateOrJoinRoomCnvas.instance.pawnSelectionScr.SetActive(true);


        if (PlayFabManager.instance.playerNum == 2)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(false);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(false);
        }
        else if (PlayFabManager.instance.playerNum == 4)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(true);
        }
            for (int i = 0; i < MultiplayerGameController.instance.TeamButton.Count; i++)
        {

            MultiplayerGameController.instance.TeamButton[i].interactable = true;
        }


    }

    public void OnClick_CreateRoom()
    {
        SoundManager.PlaySound("btnSound");
        loadingPanel.instance.ShowLoader("Room Creating...");
        privateRoom = false;
        Debug.Log("false");

        if (!PhotonNetwork.IsConnected)
        {
            return;     
        }
        //Create Room
        // Join Or Create Room

        RoomOptions options = new RoomOptions();
        options.BroadcastPropsChangeToAll = true;

        playerNum = PlayFabManager.instance.playerNum;

        options.MaxPlayers = ((byte)PlayFabManager.instance.playerNum);
        MultiplayerGameController.instance.playerNum = options.MaxPlayers;
        PhotonNetwork.JoinOrCreateRoom(_roomName.text.ToLower(), options, TypedLobby.Default);
        MultiplayerGameController.instance.roomName = _roomName.text.ToLower();
        UI.instance.RoomName = _roomName.text.ToLower();
        CreateOrJoinRoomCnvas.instance.pawnSelectionScr.SetActive(true);

        if (PlayFabManager.instance.playerNum == 2)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(false);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(false);
        }
        else if (PlayFabManager.instance.playerNum == 4)
        {
            CreateOrJoinRoomCnvas.instance.PawnObjs[0].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[1].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[2].gameObject.SetActive(true);
            CreateOrJoinRoomCnvas.instance.PawnObjs[3].gameObject.SetActive(true);
        }


        for (int i = 0; i < MultiplayerGameController.instance.TeamButton.Count; i++)
        {

            MultiplayerGameController.instance.TeamButton[i].interactable = true;
        }


    }
    
    public void JoinPrivateRoom()
    {
        SceneManager.LoadScene(1);
        PhotonNetwork.JoinRoom(privateRoomName.text);
       
    }
        
    public override void OnCreatedRoom()
    {
        if (CreateRoomMenu.privateRoom == true)
        {
            privateShareBtn.SetActive(true);

            Debug.Log("True In Method");
           
            PhotonNetwork.CurrentRoom.IsVisible = false;

        }
     

        Debug.Log(" Created Room Successfully ",this);
        if (_roomCanvases)
        {
            Debug.Log("hahahahahha");
        }
        if (_roomCanvases.currentRoomCanvas)
        {
            Debug.Log("hehehehehehe");
        }
        _roomCanvases.currentRoomCanvas.show();

        loadingPanel.instance.HideLoader();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {

        Debug.Log(" Room Creation Failed ;" + message, this);
        loadingPanel.instance.HideLoader();

    }

    public void BackToMainMenu()
    {
        SoundManager.PlaySound("btnSound");

        if (UI.instance.gameObject.activeInHierarchy)
        {
            Destroy(UI.instance.gameObject);
            UI.instance = null;
        }
        if (PlayFabManager.instance.gameObject.activeInHierarchy)
        {
            Destroy(PlayFabManager.instance.gameObject);
            PlayFabManager.instance = null;
        }

        SceneManager.LoadScene(0);
    }

  
}
