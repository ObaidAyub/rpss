using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiplayerGameController :/* GameController, IOnEventCallback,*/ MonoBehaviourPunCallbacks
{
    public static MultiplayerGameController instance;
    private PhotonView PV;
    public string roomName;
    public int playerNum;
    public List<Button> TeamButton;
    public Button startBtn;

    private ExitGames.Client.Photon.Hashtable _myCustomProperties = new ExitGames.Client.Photon.Hashtable();

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }
    public void OnEvent(EventData photonEvent)
    {
        throw new System.NotImplementedException();
    }
	private void Start()
	{
        instance = this;
        
    }

    public void SetLocalPlayer(TeamColor team)
    {
        //localPlayer = team == TeamColor.White ? whitePlayer : blackPlayer;
    }
    public void SelectingTeam(int index)
    {
        string result;
        PV.RPC("SelectTeam", RpcTarget.AllBuffered, index);
        for (int i = 0; i < TeamButton.Count; i++)
        {
            if (i != index)
                TeamButton[i].interactable = false; /*(/*PhotonNetwork.IsMasterClient*/
        }
        switch (index)
        {
            case 0:
                result = "Red";
                _myCustomProperties["Team"] = result;
                UI.instance.teamColor = result;
                break;
            case 1:
                result = "Blue";
                _myCustomProperties["Team"] = result;
                UI.instance.teamColor = result;
                break;
            case 2:
                result = "Green";
                _myCustomProperties["Team"] = result;
                UI.instance.teamColor = result;
                break;
            case 3:
                result = "Purple";
                _myCustomProperties["Team"] = result;
                UI.instance.teamColor = result;
                break;
        }


        PhotonNetwork.LocalPlayer.CustomProperties = _myCustomProperties;
        CreateOrJoinRoomCnvas.instance.pawnSelectionScr.SetActive(false);

        Debug.Log("_myCustomProperties : " + _myCustomProperties);
    }
    [PunRPC]
    public void SelectTeam(int ind)
    {
        TeamButton[ind].interactable = false; /*(/*PhotonNetwork.IsMasterClient*/
    }
    public void GetTeam()
    {
        string teamName =(string)PhotonNetwork.LocalPlayer.CustomProperties["Team"];
        Debug.LogError(_myCustomProperties["Team"]);
    }
}
