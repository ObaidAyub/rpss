using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitToOff : MonoBehaviour
{
	// Start is called before the first frame update
	private void OnEnable()
	{
		StartCoroutine(WaitToOffPanel());
	}
	public IEnumerator WaitToOffPanel()
	{
		yield return new WaitForSeconds(3f);
		this.gameObject.SetActive(false);
	}
}
