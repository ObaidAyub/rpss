
#if UNITY_EDITOR
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

public class RedourcesPrefab : IPreProcessBuildWithReport
{
    public int callbackOrder {get { return 0; } }

    public void OnPreProcessBuild(BuildReport report)
    {

        MasterManager.PopulateNetworkedPrefabs();

    }

}
#endif