﻿using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using System;
using System.Collections.Generic;
using agora_gaming_rtc;
using Photon.Pun;
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
using UnityEngine.Android;
#endif
using System.Collections;

/// <summary>
///    TestHome serves a game controller object for this application.
/// </summary>
public class TestHome : MonoBehaviour
{

    // Use this for initialization
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
    private ArrayList permissionList = new ArrayList();
#endif
    static TestHelloUnityVideo app = null;

    private string HomeSceneName = "SceneHome";

    private string PlaySceneName = "SceneHelloVideo";

    // PLEASE KEEP THIS App ID IN SAFE PLACE
    // Get your own App ID at https://dashboard.agora.io/

    private string AppID = "a3f3b10940224fe78428f446786ce2fd";


    //[HideInInspector]
    public string ChannelName;

    PhotonView PV;
   

    void Awake()
    {
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
		permissionList.Add(Permission.Microphone);         
		permissionList.Add(Permission.Camera);               
#endif

        // keep this alive across scenes
    }

    void Start()
    {
        PV = GetComponent<PhotonView>();
        CheckAppId();

        ChannelName = UI.instance.RoomName;
    }

    void Update()
    {
        CheckPermissions();
    }

    private void CheckAppId()
    {
        Debug.Assert(AppID.Length > 10, "Please fill in your AppId first on Game Controller object.");
        GameObject go = GameObject.Find("AppIDText");
        if (go != null)
        {
            Text appIDText = go.GetComponent<Text>();
            if (appIDText != null)
            {
                if (string.IsNullOrEmpty(AppID))
                {
                    appIDText.text = "AppID: " + "UNDEFINED!";
                }
                else
                {
                    appIDText.text = "AppID: " + AppID.Substring(0, 4) + "********" + AppID.Substring(AppID.Length - 4, 4);
                }
            }
        }
    }

    /// <summary>
    ///   Checks for platform dependent permissions.
    /// </summary>
    private void CheckPermissions()
    {
#if (UNITY_2018_3_OR_NEWER && UNITY_ANDROID)
        foreach(string permission in permissionList)
        {
            if (!Permission.HasUserAuthorizedPermission(permission))
            {                 
				Permission.RequestUserPermission(permission);
			}
        }
#endif
    }

    public void onJoinButtonClicked()
    {
        // get parameters (channel name, channel profile, etc.)
        //GameObject go = GameObject.Find("ChannelName");
        //InputField field = go.GetComponent<InputField>();

        // create app if nonexistent
        if (ReferenceEquals(app, null))
        {
            app = new TestHelloUnityVideo(); // create app
            app.loadEngine(AppID); // load engine
        }

        // join channel and jump to next scene
        app.join(ChannelName);

        //VideoPanel.SetActive(true);

        //joiningPanel.SetActive(false);
        //SceneManager.sceneLoaded += OnLevelFinishedLoading; // configure GameObject after scene is loaded
        //SceneManager.LoadScene(PlaySceneName, LoadSceneMode.Single);
    }

    public void onLeaveButtonClicked()
    {
        if (!ReferenceEquals(app, null))
        {
            app.leave(); // leave channel
            app.unloadEngine(); // delete engine
            app = null; // delete app
                        //SceneManager.LoadScene(HomeSceneName, LoadSceneMode.Single);

            // call a rcp for
            AgoraVideoCallManager.instance.redCancleBtn.gameObject.SetActive(false);
            AgoraVideoCallManager.instance.redCameraBtn.gameObject.SetActive(false);
            PV.RPC("ExitCall", RpcTarget.AllBuffered, GameManager.instance.TeamColor);
        }
        
    }

    [PunRPC]
    public void ExitCall(string teamColor)
    {
        Debug.Log("teamColor" + teamColor);

        if (teamColor == GameManager.instance.TeamColor)
        {
            Debug.Log("Return");
            return;
        }

        switch (teamColor)
        {
            case "Red":
                AgoraVideoCallManager.instance.PlayerRed.gameObject.SetActive(false);
                
                break;
            case "Purple":
                AgoraVideoCallManager.instance.PlayerPurple.gameObject.SetActive(false);
                break;
            case "Green":
                AgoraVideoCallManager.instance.PlayerGreen.gameObject.SetActive(false);
                break;
            case "Blue":
                AgoraVideoCallManager.instance.PlayerBlue.gameObject.SetActive(false);
                break;
        }
       
        AgoraVideoCallManager.instance.showCallPopup(false);
    }
        public void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == PlaySceneName)
        {
            if (!ReferenceEquals(app, null))
            {
                app.onSceneHelloVideoLoaded(); // call this after scene is loaded
            }
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }
    }

    void OnApplicationPause(bool paused)
    {
        if (!ReferenceEquals(app, null))
        {
            app.EnableVideo(paused);
        }
    }

    void OnApplicationQuit()
    {
        if (!ReferenceEquals(app, null))
        {
            app.unloadEngine();
            IRtcEngine.Destroy();
        }
    }
    public void switchCamera()
    {
        if (!ReferenceEquals(app, null))
        {
            app.SwitchCamera(); // call this after scene is loaded
        }
    }
}
