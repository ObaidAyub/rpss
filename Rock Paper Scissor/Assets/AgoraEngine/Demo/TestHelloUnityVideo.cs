using UnityEngine;
using UnityEngine.UI;

using agora_gaming_rtc;
using agora_utilities;


// this is an example of using Agora Unity SDK
// It demonstrates:
// How to enable video
// How to join/leave channel
// 
public class TestHelloUnityVideo
{

    // instance of agora engine
    private IRtcEngine mRtcEngine;
    private Text MessageText;



    // a token is a channel key that works with a AppID that requires it. 
    // Generate one by your token server or get a temporary token from the developer console
    private string token = "";

    // load agora engine
    public void loadEngine(string appId)
    {
        // start sdk
        Debug.Log("initializeEngine");

        if (mRtcEngine != null)
        {
            Debug.Log("Engine exists. Please unload it first!");
            return;
        }

        // init engine
        mRtcEngine = IRtcEngine.GetEngine(appId);

        // enable log
        mRtcEngine.SetLogFilter(LOG_FILTER.DEBUG | LOG_FILTER.INFO | LOG_FILTER.WARNING | LOG_FILTER.ERROR | LOG_FILTER.CRITICAL);
    }

    public void join(string channel)
    {
        Debug.Log("calling join (channel = " + channel + ")");

        if (mRtcEngine == null)
            return;

        // set callbacks (optional)
        mRtcEngine.OnJoinChannelSuccess = onJoinChannelSuccess;
        mRtcEngine.OnUserJoined = onUserJoined;
        mRtcEngine.OnUserOffline = onUserOffline;
        mRtcEngine.OnWarning = (int warn, string msg) =>
        {
            Debug.LogWarningFormat("Warning code:{0} msg:{1}", warn, IRtcEngine.GetErrorDescription(warn));
        };
        mRtcEngine.OnError = HandleError;

        // enable video
        mRtcEngine.EnableVideo();
        // allow camera output callback
        mRtcEngine.EnableVideoObserver();

        // join channel
        /*  This API Assumes the use of a test-mode AppID
             mRtcEngine.JoinChannel(channel, null, 0);
        */

        /*  This API Accepts AppID with token; by default omiting info and use 0 as the local user id */
        mRtcEngine.JoinChannelByKey(channelKey: token, channelName: channel);
    }

    public string getSdkVersion()
    {
        string ver = IRtcEngine.GetSdkVersion();
        return ver;
    }

    public void leave()
    {
        Debug.Log("calling leave");

        if (mRtcEngine == null)
            return;

        // leave channel
        mRtcEngine.LeaveChannel();
        mRtcEngine.DisableVideoObserver();


        AgoraVideoCallManager.instance.PlayerRed.gameObject.SetActive(false);
        AgoraVideoCallManager.instance.redCancleBtn.gameObject.SetActive(false);
        AgoraVideoCallManager.instance.redCameraBtn.gameObject.SetActive(false);


        AgoraVideoCallManager.instance.PlayerPurple.gameObject.SetActive(false);
        
        AgoraVideoCallManager.instance.PlayerGreen.gameObject.SetActive(false);
       

        AgoraVideoCallManager.instance.PlayerBlue.gameObject.SetActive(false);
        


    }


    public void unloadEngine()
    {
        Debug.Log("calling unloadEngine");

        // delete
        //if (mRtcEngine != null)
        //{
        //    IRtcEngine.Destroy();  // Place this call in ApplicationQuit
        //    mRtcEngine = null;
        //}

    }

    public void EnableVideo(bool pauseVideo)
    {
        if (mRtcEngine != null)
        {
            if (!pauseVideo)
            {
                mRtcEngine.EnableVideo();
            }
            else
            {
                mRtcEngine.DisableVideo();
            }
        }
    }

    // accessing GameObject in Scnene1
    // set video transform delegate for statically created GameObject
    public void onSceneHelloVideoLoaded()
    {
        // Attach the SDK Script VideoSurface for video rendering
        GameObject quad = GameObject.Find("Quad");
        if (ReferenceEquals(quad, null))
        {
            Debug.Log("failed to find Quad");
            return;
        }
        else
        {
            quad.AddComponent<VideoSurface>();
        }

        GameObject quad2 = GameObject.Find("Quad2");
        if (ReferenceEquals(quad2, null))
        {
            Debug.Log("failed to find Cube");
            return;
        }
        else
        {
            quad2.AddComponent<VideoSurface>();
        }

        GameObject text = GameObject.Find("MessageText");
        if (!ReferenceEquals(text, null))
        {
            MessageText = text.GetComponent<Text>();
        }

        GameObject bobj = GameObject.Find("HelpButton");
        if (bobj != null)
        {
            Button button = bobj.GetComponent<Button>();
            if (button!=null)
            {
                button.onClick.AddListener(HandleHelp);
	        }
	    }

    }

    void HandleHelp()
    {
#if UNITY_2020_3_OR_NEWER && PLATFORM_STANDALONE_OSX
        // this very easy to forget for MacOS
        HandleError(-2, "if you don't see any video, did you set the MacOS plugin bundle to AnyCPU?");
#else
        HandleError(-1, "if you don't see any video, please check README for help");
#endif
    }

    // implement engine callbacks
    private void onJoinChannelSuccess(string channelName, uint uid, int elapsed)
    {
        AgoraVideoCallManager.instance.isConnected = true;
        switch (GameManager.instance.TeamColor)
        {
            case "Red":
                AgoraVideoCallManager.instance.PlayerRed.gameObject.SetActive(true);
                break;
            case "Purple":
                AgoraVideoCallManager.instance.PlayerPurple.gameObject.SetActive(true);
                
                break;
            case "Green":
                AgoraVideoCallManager.instance.PlayerGreen.gameObject.SetActive(true);
                
                break;
            case "Blue":
                AgoraVideoCallManager.instance.PlayerBlue.gameObject.SetActive(true);
                break;
        }
        AgoraVideoCallManager.instance.redCancleBtn.gameObject.SetActive(true);
        AgoraVideoCallManager.instance.redCameraBtn.gameObject.SetActive(true);
        loadingPanel.instance.HideLoader();
    }

    // When a remote user joined, this delegate will be called. Typically
    // create a GameObject to render video on it
    int playerNum;
    private void onUserJoined(uint uid, int elapsed)
    {
        playerNum += 1;
        Debug.Log("onUserJoined: uid = " + uid + " elapsed = " + elapsed + " Player number : " + playerNum );
        // this is called in main thread

        // find a game object to render video stream from 'uid'
        GameObject go = GameObject.Find(uid.ToString());
        if (!ReferenceEquals(go, null))
        {
            return; // reuse
        }
        
        VideoSurface videoSurface = makeImageSurface(AgoraVideoCallManager.instance.V_id);
        if (!ReferenceEquals(videoSurface, null))
        {
            // configure videoSurface
            videoSurface.SetForUser(uid);

            videoSurface.SetEnable(true);

            videoSurface.SetVideoSurfaceType(AgoraVideoSurfaceType.RawImage);
        }
    }
    public VideoSurface makePlaneSurface(string goName)
    {
        
        Debug.Log("Came Here for positions" + playerNum);


        VideoSurface videoSurface = AgoraVideoCallManager.instance.PlayerGreen.gameObject.GetComponent<VideoSurface>();

        return videoSurface;
    }

    public VideoSurface makeImageSurface(string goName)
    {
        Debug.Log("Here " + goName);

        VideoSurface videoSurface;
        switch (goName)
        {
            case "Red":
                videoSurface = AgoraVideoCallManager.instance.PlayerRed.gameObject.GetComponent<VideoSurface>();
                break;
            case "Purple":
                videoSurface = AgoraVideoCallManager.instance.PlayerPurple.gameObject.GetComponent<VideoSurface>();
                break;
            case "Green":
                videoSurface = AgoraVideoCallManager.instance.PlayerGreen.gameObject.GetComponent<VideoSurface>();
                break;
            case "Blue":
                videoSurface = AgoraVideoCallManager.instance.PlayerBlue.gameObject.GetComponent<VideoSurface>();
                break;

            default:
                videoSurface = AgoraVideoCallManager.instance.PlayerRed.gameObject.GetComponent<VideoSurface>();
                break;
        }
        return videoSurface;
    }
    // When remote user is offline, this delegate will be called. Typically
    // delete the GameObject for this user
    private void onUserOffline(uint uid, USER_OFFLINE_REASON reason)
    {
        // remove video stream
        Debug.Log("onUserOffline: uid = " + uid + " reason = " + reason);
        // this is called in main thread
        GameObject go = GameObject.Find(uid.ToString());
        if (!ReferenceEquals(go, null))
        {
            Object.Destroy(go);
        }
    }

    #region Error Handling
    private int LastError { get; set; }
    private void HandleError(int error, string msg)
    {
        if (error == LastError)
        {
            return;
        }

        if (string.IsNullOrEmpty(msg))
        {
            msg = string.Format("Error code:{0} msg:{1}", error, IRtcEngine.GetErrorDescription(error));
        }

        switch (error)
        {
            case 101:
                msg += "\nPlease make sure your AppId is valid and it does not require a certificate for this demo.";
                break;
        }

        Debug.LogError(msg);
        if (MessageText != null)
        {
            if (MessageText.text.Length > 0)
            {
                msg = "\n" + msg;
            }
            MessageText.text += msg;
        }

        LastError = error;
    }

    #endregion

    public void SwitchCamera() { mRtcEngine.SwitchCamera(); }

}
