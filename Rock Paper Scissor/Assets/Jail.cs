using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jail : MonoBehaviour
{
    public Image jailedPawn;

    private void OnEnable()
    {
        //StartCoroutine(WaitingToOff());
    }
    IEnumerator  WaitingToOff()
    {
        yield return new WaitForSecondsRealtime(5f);
        this.gameObject.SetActive(false);
    }
}
