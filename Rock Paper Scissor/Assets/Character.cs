using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Avatar
{
    public int faceIndex;
    public int lipsIndex;
    public int glassesIndex;
    public Avatar(int faceIndex, int lipsIndex, int glassesIndex)
    {
        this.faceIndex = faceIndex;
        this.lipsIndex = lipsIndex;
        this.glassesIndex = glassesIndex;
    }
}
public class Character : MonoBehaviour
{

    public List<Sprite> Baseface;
    public List<Sprite> Lips;
    public List<Sprite> Glasses;
    public List<Sprite> Eyes;
    public Image lips;
    public Image glasses;
    public Image eye;
    public int baseIndexFace = 0;
    public int baseIndexLips = 0;
    public int baseIndexGlasses = 0;
    public int baseIndexEye = 0;
    //public int baseIndex=0;
    private void OnEnable()
    {
        //PlayFabManager.instance.GetAvatar();
    }
    public void BaseChange()
    {
        baseIndexFace++;
        if (baseIndexFace == 4)
        {
            baseIndexFace = 0;
        }
        this.GetComponent<Image>().sprite = Baseface[baseIndexFace];

    }
    public void LipsChange()
    {
        baseIndexLips++;
        if (baseIndexLips == 4)
        {
            baseIndexLips = 0;
        }
        lips.sprite = Lips[baseIndexLips];

    }
    public void GlassesChange()
    {
        baseIndexGlasses++;
        if (baseIndexGlasses == 4)
        {
            baseIndexGlasses = 0;
        }
        glasses.sprite = Glasses[baseIndexGlasses];

    }
    public void EyeChange()
    {
        baseIndexEye++;
        if (baseIndexEye == 4)
        {
            baseIndexEye = 0;
        }
        eye.sprite = Eyes[baseIndexEye];

    }
    public Avatar ReturnClass()
    {
        //Debug.LogError(new Avatar(baseIndexFace, baseIndexLips, baseIndexGlasses));
        return new Avatar(baseIndexFace, baseIndexLips, baseIndexGlasses);
    } 
    public void SetAvatar(Avatar avatar)
    {
        this.GetComponent<Image>().sprite = Baseface[avatar.faceIndex];
        lips.sprite = Lips[avatar.lipsIndex];
        glasses.sprite = Glasses[avatar.glassesIndex];

    }
}

