using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameController : MonoBehaviour
{
    private CameraSetup cameraSetup;


    internal void SetupCamera(TeamColor team)
    {
        cameraSetup.SetupCamera(team);
    }
}
